void main(List<String> args) {
  Segitiga segitiga = new Segitiga();
  segitiga.alas = 5;
  segitiga.tinggi = 6;
  print(segitiga.luas);
}

class Bangun {
  late double _panjang, _tinggi, _alas;

  set panjang(double panjang) {
    this._panjang = panjang;
  }

  set tinggi(double tinggi) {
    this._tinggi = tinggi;
  }

  set alas(double alas) {
    this._alas = alas;
  }

  double get panjang => this._panjang;
  double get tinggi => this._tinggi;
  double get alas => this._alas;
}

class Segitiga extends Bangun {
  double get luas => (this.alas * this.tinggi) / 2;
}
