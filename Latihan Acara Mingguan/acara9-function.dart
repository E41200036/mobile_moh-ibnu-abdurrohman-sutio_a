void main(List<String> args) {
  Segitiga segitiga = new Segitiga(alas: 4, tinggi: 3);
  print('Luas segitiga: ' + segitiga.luasSegitiga().toString());
}

class Segitiga {
  late int alas, tinggi;
  Segitiga({required this.alas, required this.tinggi});

  luasSegitiga() {
    return alas * tinggi / 2;
  }
}
