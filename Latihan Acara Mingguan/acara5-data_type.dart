void main(List<String> args) {
  // list of data type in dart
  // int, double, num, String, bool, dynamic, null

  // int
  int a = 10;
  int b = 20;
  int c = a + b;
  print(c);

  // double
  double d = 10.5;
  double e = 20.5;
  double f = d + e;
  print(f);

  // num
  num g = 10;
  num h = 20;
  num i = g + h;
  print(i);

  // String
  String s1 = 'Hello';
  String s2 = 'World';
  String s3 = s1 + s2;
  print(s3);

  // bool
  bool b1 = true;
  bool b2 = false;
  bool b3 = b1 && b2;
  print(b3);

  // dynamic
  dynamic d1 = 10;
  dynamic d2 = 'Hello';
  dynamic d3 = b1 && b2;
  print(d3);

  // null
  Null n = null;
  print(n);
}
