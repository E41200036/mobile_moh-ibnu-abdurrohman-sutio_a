void main() {
  // implementation of operator dart
  var a = 10;
  var b = 20;
  print(a + b);
  print(a - b);
  print(a * b);
  print(a / b);
  print(a ~/ b);
  print(a % b);

  // implementation of operator dart
  var c = 10;
  var d = 20;
  print(c & d);
  print(c | d);
  print(c ^ d);
  print(~c);
  print(c << 2);
  print(c >> 2);

  // implementation of operator dart
  var e = 10;
  var f = 20;
  print(e < f);
  print(e > f);
  print(e <= f);
  print(e >= f);
  print(e == f);
  print(e != f);

  // implementation of operator dart
  var g = 10;
  var h = 20;
  print(g & h);
  print(g | h);
  print(g ^ h);
  print(~g);
  print(g << 2);
  print(g >> 2);

  // implementation of operator dart
  var i = 10;
  var j = 20;
  print(i < j);
  print(i > j);
  print(i <= j);
  print(i >= j);
  print(i == j);
  print(i != j);

  // implementation of operator dart
  var k = 10;
  var l = 20;
  print(k & l);
  print(k | l);
  print(k ^ l);
  print(~k);
  print(k << 2);
  print(k >> 2);

  // implementation of operator dart
  var m = 10;
  var n = 20;
  print(m < n);
  print(m > n);
  print(m <= n);
  print(m >= n);
  print(m == n);
  print(m != n);

  // implementation of operator dart
  var o = 10;
  var p = 20;
  print(o & p);
}
