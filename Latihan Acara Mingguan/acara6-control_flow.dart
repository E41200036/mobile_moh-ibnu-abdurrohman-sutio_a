void main(List<String> args) {
  // implementation of ternary operator
  int a = 10;
  int b = 20;
  int max = a > b ? a : b;
  print(max);

  // implementation of switch case
  int c = 10;
  switch (c) {
    case 1:
      print("one");
      break;
    case 2:
      print("two");
      break;
    case 3:
      print("three");
      break;
    default:
      print("default");
  }

  // implementation of if else if
  int d = 10;
  if (d == 1) {
    print("one");
  } else if (d == 2) {
    print("two");
  } else if (d == 3) {
    print("three");
  } else {
    print("default");
  }

  // implementation of if
  int e = 10;
  if (e == 1) {
    print("one");
  } else if (e == 2) {
    print("two");
  } else if (e == 3) {
    print("three");
  } else {
    print("default");
  }
}
