void main(List<String> args) {
  tagline();
  print('=================================');
  nyanyi();
}

void tagline() {
  print('Life');
  Future.delayed(Duration(seconds: 1), () {
    print('Never Flat');
  });
  print('is');
}

line1() {
  print('Mari menyanyi');
}

line2() {
  print('Semoga ya hari ini lebih baik dari hari kemarin');
}

line3() {
  print('Yang lewat begitu saja tanpa lakukan apa-apa');
}

line4() {
  print('Semoga ya pagi ini lebih cerah dari pagi kemarin');
}

line5() {
  print('Mungkin bisa bangun lebih dini tuk bergegas siapkan diri');
}

void nyanyi() async {
  line1();
  Future.delayed(Duration(seconds: 1), () => line2());
  Future.delayed(Duration(seconds: 2), () => line3());
  Future.delayed(Duration(seconds: 3), () => line4());
  Future.delayed(Duration(seconds: 4), () => line5());
}
