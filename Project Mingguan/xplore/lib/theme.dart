import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color kPrimary = Color(0xffefefef);
Color kBlack = Color(0xff314728);
Color kGrey = Color(0xffA9B0A6);
Color kWhite = Color(0xffFFFFFF);
Color kOrange = Color(0xffF9B650);

FontWeight normal = FontWeight.w400;
FontWeight medium = FontWeight.w500;
FontWeight semibold = FontWeight.w600;

TextStyle blackTextStyle = GoogleFonts.poppins(
  color: kBlack,
);

TextStyle greyTextStyle = GoogleFonts.poppins(
  color: kGrey,
);

TextStyle whiteTextStyle = GoogleFonts.poppins(
  color: kWhite,
);
