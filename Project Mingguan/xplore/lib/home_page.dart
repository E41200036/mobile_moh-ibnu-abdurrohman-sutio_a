import 'package:flutter/material.dart';
import 'package:xplore/theme.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kPrimary,
      body: SafeArea(
        child: Center(
          child: Column(
            children: [
              SizedBox(
                height: 50,
              ),
              Text(
                'Profile Picture',
                textAlign: TextAlign.center,
                style:
                    blackTextStyle.copyWith(fontSize: 20, fontWeight: semibold),
              ),
              SizedBox(
                height: 50,
              ),
              Image.asset(
                'assets/image_primary.png',
                width: 140,
                height: 140,
              ),
              SizedBox(
                height: 47,
              ),
              Text(
                'UX Designer',
                textAlign: TextAlign.center,
                style: greyTextStyle.copyWith(fontSize: 16),
              ),
              SizedBox(
                height: 70,
              ),
              Wrap(
                spacing: 38,
                runSpacing: 40,
                children: [
                  Image.asset(
                    'assets/image_item1.png',
                    width: 80,
                    height: 80,
                  ),
                  Image.asset(
                    'assets/image_item2.png',
                    width: 80,
                    height: 80,
                  ),
                  Image.asset(
                    'assets/image_item3.png',
                    width: 80,
                    height: 80,
                  ),
                  Image.asset(
                    'assets/image_item4.png',
                    width: 80,
                    height: 80,
                  ),
                  Image.asset(
                    'assets/image_item5.png',
                    width: 80,
                    height: 80,
                  ),
                  Image.asset(
                    'assets/image_item6.png',
                    width: 80,
                    height: 80,
                  ),
                ],
              ),
              SizedBox(
                height: 70,
              ),
              Container(
                width: 224,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16),
                  color: kWhite,
                ),
                child: TextButton(
                  onPressed: () {
                    showModalBottomSheet(
                        context: context,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(40),
                        ),
                        builder: (context) {
                          return Container(
                            height: 290,
                            padding: EdgeInsets.symmetric(vertical: 50),
                            width: double.infinity,
                            decoration: BoxDecoration(
                                color: kWhite,
                                borderRadius: BorderRadius.vertical(
                                    top: Radius.circular(50))),
                            child: Column(children: [
                              Text(
                                'Update Photo',
                                style: blackTextStyle.copyWith(
                                    fontSize: 22, fontWeight: medium),
                              ),
                              SizedBox(
                                height: 12,
                              ),
                              SizedBox(
                                width: 252,
                                child: Text(
                                  'You are only able to change the picture profile once',
                                  style: greyTextStyle.copyWith(fontSize: 18),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              SizedBox(
                                height: 30,
                              ),
                              Container(
                                width: 224,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(16),
                                  color: kOrange,
                                ),
                                child: TextButton(
                                  onPressed: () {},
                                  child: Text(
                                    'Continue',
                                    style: whiteTextStyle.copyWith(
                                      fontSize: 16,
                                      fontWeight: medium,
                                    ),
                                  ),
                                ),
                              ),
                            ]),
                          );
                        });
                  },
                  child: Text(
                    'Update Profile',
                    style: blackTextStyle.copyWith(
                      fontSize: 16,
                      fontWeight: medium,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
