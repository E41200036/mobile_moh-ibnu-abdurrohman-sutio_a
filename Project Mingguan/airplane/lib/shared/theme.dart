import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

double default_margin = 24;
double border_radius = 17;

Color kPrimary = Color(0xff5C40CC);
Color kBlack = Color(0xff1F1449);
Color kWhite = Color(0xffFFFFFF);
Color kGrey = Color(0xff9698A9);
Color kGreen = Color(0xff0EC3AE);
Color kRed = Color(0xffEB70A5);
Color kBackground = Color(0xffFAFAFA);
Color kInactive = Color(0xffDBD7EC);
Color kUnavailable = Color(0xffEBECF1);
Color kAvaialable = Color(0xffE0D9FF);

TextStyle blackTextStyle = GoogleFonts.poppins(color: kBlack);
TextStyle whiteTextStyle = GoogleFonts.poppins(color: kWhite);
TextStyle greyTextStyle = GoogleFonts.poppins(color: kGrey);
TextStyle greenTextStyle = GoogleFonts.poppins(color: kGreen);
TextStyle redTextStyle = GoogleFonts.poppins(color: kRed);
TextStyle purpleTextStyle = GoogleFonts.poppins(color: kPrimary);

FontWeight light = FontWeight.w300;
FontWeight regular = FontWeight.w400;
FontWeight medium = FontWeight.w500;
FontWeight semibold = FontWeight.w600;
FontWeight bold = FontWeight.w700;
FontWeight extraBold = FontWeight.w800;
FontWeight black = FontWeight.w900;
