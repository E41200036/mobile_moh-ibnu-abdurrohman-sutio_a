import 'package:equatable/equatable.dart';

class PlaceModel extends Equatable {
  final String id;
  final String imgUrl;
  final String name;
  final String city;
  final int price;
  final double rating;

  PlaceModel({
    required this.id,
    required this.imgUrl,
    required this.name,
    required this.city,
    required this.price,
    required this.rating,
  });

  // mengambil data dari firestore dan mengubahnya menjadi model place
  factory PlaceModel.fromJson(String id, Map<String, dynamic> json) {
    return PlaceModel(
      id: id,
      imgUrl: json['imgUrl'],
      name: json['name'],
      city: json['city'],
      price: json['price'],
      rating: json['rating'].toDouble(),
    );
  }

  // mengubah model place ke dalam map yang dapat dikirim ke firestore
  Map<String, dynamic> toJson() => {
        'id': id,
        'imgUrl': imgUrl,
        'name': name,
        'city': city,
        'price': price,
        'rating': rating
      };
  @override
  List<Object?> get props => [id, imgUrl, name, city, price, rating];
}
