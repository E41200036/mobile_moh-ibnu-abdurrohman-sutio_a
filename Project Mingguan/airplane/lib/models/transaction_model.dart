import 'dart:convert';

import 'package:airplane/models/place_model.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

class TransactionModel extends Equatable {
  final String id;
  final PlaceModel place;
  final int numberOfTraveler;
  final String selectedSeat;
  final bool insurance;
  final bool refundable;
  final double vat;
  final int price;
  final int grandTotal;

  TransactionModel(
      {required this.place,
      this.id = '',
      this.numberOfTraveler = 0,
      this.selectedSeat = '',
      this.insurance = false,
      this.refundable = false,
      this.vat = 0,
      this.price = 0,
      this.grandTotal = 0});

  factory TransactionModel.fromJson(String id, Map<String, dynamic> json) {
    return TransactionModel(
      id: id,
      place: PlaceModel.fromJson(json['place']['id'], json['place']),
      numberOfTraveler: json['numberOfTraveler'],
      selectedSeat: json['selectedSeat'],
      insurance: json['insurance'],
      refundable: json['refundable'],
      vat: json['vat'],
      price: json['price'],
      grandTotal: json['grandTotal'],
    );
  }

  @override
  List<Object?> get props => [
        place,
        numberOfTraveler,
        selectedSeat,
        insurance,
        refundable,
        vat,
        price,
        grandTotal,
      ];
}
