import 'package:bloc/bloc.dart';

class PageCubit extends Cubit<int> {
  // NOTE: Inisialisasi state awal
  PageCubit() : super(0);

  void setPage(int newPage) {
    emit(newPage);
  }
}
