import 'package:bloc/bloc.dart';

class SeatCubit extends Cubit<List<String>> {
  SeatCubit() : super([]);

  // fungsi untuk menambahkan data seat pada list
  void setSelected(String id) {
    // jika data sudah ada, hapus data tersebut
    // jika data belum ada, tambahkan data tersebut
    if (!isSelected(id)) {
      state.add(id);
    } else {
      state.remove(id);
    }
    print('new state $state');
    // emit data yang baru saja diubah
    emit(List.from(state));
  }

  // fungsi untuk cek apakah seat sudah dipilih atau belum
  bool isSelected(String id) {
    int index = state.indexOf(id);
    if (index == -1) {
      return false;
    } else {
      return true;
    }
  }
}
