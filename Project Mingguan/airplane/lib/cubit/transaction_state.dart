part of 'transaction_cubit.dart';

abstract class TransactionState extends Equatable {
  const TransactionState();

  @override
  List<Object> get props => [];
}

class TransactionInitial extends TransactionState {}

class TransactionLoading extends TransactionState {}

class TransactionSuccess extends TransactionState {
  final List<TransactionModel> transactionModel;

  TransactionSuccess({required this.transactionModel});

  @override
  List<Object> get props => [transactionModel];
}

class TransactionFailed extends TransactionState {
  final String message;
  TransactionFailed(this.message);
  @override
  List<Object> get props => [message];
}
