import 'package:airplane/models/user_model.dart';
import 'package:airplane/services/auth_service.dart';
import 'package:airplane/services/user_service.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  AuthCubit() : super(AuthInitial());

  // fungsi untuk sign up user
  void signUp(
      {required String name,
      required String email,
      required String password,
      String hobby = ''}) async {
    try {
      emit(AuthLoading());
      // mengirimkan data ke firebase
      UserModel user = await AuthService().signUp(
        name: name,
        email: email,
        password: password,
        hobby: hobby,
      );
      // jika berhasil maka emit ke AuthSuccess
      emit(AuthSuccess(user));
    } catch (e) {
      // jika gagal maka emit ke AuthFailure
      emit(AuthFailed(e.toString()));
    }
  }

  // fungsi untuk sign out user
  void signOut() async {
    try {
      // menampilkan loading
      emit(AuthLoading());
      // melakukan sign out user
      await AuthService().signOut();
      // jika berhasil maka emit ke AuthInitial dan menghapus data user
      emit(AuthInitial());
    } catch (e) {
      // jika gagal maka emit ke AuthFailure dan menampilkan pesan error
      emit(AuthFailed(e.toString()));
    }
  }

  // fungsi untuk mengambil data user berdasarkan id
  void getCurrentUser(String id) async {
    try {
      // mengambil data user berdasarkan id user yang login
      UserModel user = await UserService().getUserById(id);
      // jika berhasil maka emit ke AuthSuccess dan menampilkan data user
      emit(AuthSuccess(user));
    } catch (e) {
      // jika gagal maka emit ke AuthFailure dan menampilkan pesan error
      emit(AuthFailed(e.toString()));
    }
  }
}
