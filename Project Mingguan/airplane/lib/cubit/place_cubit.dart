import 'package:airplane/services/place_service.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../models/place_model.dart';

part 'place_state.dart';

class PlaceCubit extends Cubit<PlaceState> {
  PlaceCubit() : super(PlaceInitial());

  void getPlaces() async {
    try {
      emit(PlaceLoading());
      // mengubah data dari place service ke dalam model place
      List<PlaceModel> places = await PlaceService().getPlaces();
      emit(PlaceSuccess(places));
    } catch (e) {
      emit(PlaceFailure(e.toString()));
    }
  }
}
