import 'package:airplane/models/transaction_model.dart';
import 'package:airplane/services/transaction_service.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'transaction_state.dart';

class TransactionCubit extends Cubit<TransactionState> {
  TransactionCubit() : super(TransactionInitial());

  void createTransaction(TransactionModel transactionModel) async {
    try {
      emit(TransactionLoading());
      await TransactionService().setTransaction(transactionModel);
      emit(TransactionSuccess(transactionModel: []));
    } catch (e) {
      emit(TransactionFailed(e.toString()));
    }
  }

  void getTransactions() async {
    try {
      emit(TransactionLoading());
      List<TransactionModel> transactions = await TransactionService().getTransactions();
      emit(TransactionSuccess(transactionModel: transactions));
    } catch(e) {
      emit(TransactionFailed(e.toString()));
    }
  }
}
