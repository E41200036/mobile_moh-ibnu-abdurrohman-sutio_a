import 'package:cloud_firestore/cloud_firestore.dart';

import '../models/transaction_model.dart';

class TransactionService {
  CollectionReference transactionCollection =
      FirebaseFirestore.instance.collection('transactions');
  Future<void> setTransaction(TransactionModel transaction) async {
    try {
      transactionCollection.add({
        // data place diisi oleh data place yang sudah di set di place model menjadi tipe json
        'place': transaction.place.toJson(),
        'numberOfTraveler': transaction.numberOfTraveler,
        'selectedSeat': transaction.selectedSeat,
        'insurance': transaction.insurance,
        'refundable': transaction.refundable,
        'vat': transaction.vat,
        'price': transaction.price,
        'grandTotal': transaction.grandTotal,
      });
    } catch (e) {
      throw e;
    }
  }

  Future<List<TransactionModel>> getTransactions() async {
    try {
      // mengambil data dari collection places pada firestore
      QuerySnapshot snapshot = await transactionCollection.get();
      // mengubah data dari firestore ke dalam model place
      List<TransactionModel> transactions = snapshot.docs
          .map((e) =>
              TransactionModel.fromJson(e.id, e.data() as Map<String, dynamic>))
          .toList();
      return transactions;
    } catch (e) {
      throw e;
    }
  }
}
