import 'package:airplane/models/place_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class PlaceService {
  CollectionReference placeCollection =
      FirebaseFirestore.instance.collection('places');

  Future<List<PlaceModel>> getPlaces() async {
    try {
      // mengambil data dari collection places pada firestore
      QuerySnapshot snapshot = await placeCollection.get();
      // mengubah data dari firestore ke dalam model place
      List<PlaceModel> places = snapshot.docs
          .map((e) =>
              PlaceModel.fromJson(e.id, e.data() as Map<String, dynamic>))
          .toList();
      return places;
    } catch (e) {
      throw e;
    }
  }
}
