import 'package:airplane/models/user_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class UserService {
  // membuat collection user pada firestore
  CollectionReference userCollection =

      // menghubungkan ke collection user pada firestore
      FirebaseFirestore.instance.collection('users');

  // membuat method untuk menambahkan data user
  Future<void> setUser(UserModel user) async {
    try {
      // membuat document baru dengan id dari user dan mengisi data dari user
      userCollection.doc(user.id).set({
        'email': user.email,
        'name': user.name,
        'hobby': user.hobby,
        'balance': user.balance,
      });
    } catch (e) {
      throw e;
    }
  }

  // membuat method untuk mengambil data user berdasarkan id user
  Future<UserModel> getUserById(String id) async {
    try {
      // mengambil data user berdasarkan id user yang dikirimkan oleh parameter method getUserById
      DocumentSnapshot snapshot = await userCollection.doc(id).get();
      // mengubah data dari firestore ke dalam model user
      return UserModel(
        id: snapshot.id,
        email: snapshot['email'],
        name: snapshot['name'],
        hobby: snapshot['hobby'],
        balance: snapshot['balance'],
      );
    } catch (e) {
      throw e;
    }
  }
}
