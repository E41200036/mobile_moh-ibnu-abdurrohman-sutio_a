import 'package:airplane/models/user_model.dart';
import 'package:airplane/services/user_service.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AuthService {
  // mengambil data dari firebase auth
  FirebaseAuth _auth = FirebaseAuth.instance;

  // membuat method signup dengan parameter sesuai field pada form
  Future<UserModel> signUp(
      {required String email,
      required String password,
      required String name,
      String hobby = ''}) async {
    try {
      // menambahkan data user pada firebase
      UserCredential userCredential = await _auth
          .createUserWithEmailAndPassword(email: email, password: password);

      // mengisi data user model berdasarkan data firebase
      UserModel user = UserModel(
        id: userCredential.user!.uid,
        email: email,
        name: name,
        hobby: hobby,
        balance: 280000000,
      );

      // menambahkan data user ke database pada firestore
      await UserService().setUser(user);

      // mengembalikan data user
      return user;
    } catch (e) {
      throw e;
    }
  }

  // fungsi signout user
  Future<void> signOut() async {
    try {
      // menghapus data user dari firebase
      await _auth.signOut();
    } catch (e) {
      throw e;
    }
  }
}
