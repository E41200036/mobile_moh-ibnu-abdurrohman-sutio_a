import 'package:airplane/cubit/auth_cubit.dart';
import 'package:airplane/shared/theme.dart';
import 'package:airplane/ui/widgets/custom_button.dart';
import 'package:airplane/ui/widgets/custom_textfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class SignUpPage extends StatelessWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextEditingController fullname = TextEditingController(text: '');
    TextEditingController email = TextEditingController(text: '');
    TextEditingController password = TextEditingController(text: '');
    TextEditingController hobby = TextEditingController(text: '');

    Widget title() {
      return Container(
        margin: EdgeInsets.only(top: 30, bottom: 30),
        child: Text(
          'Join us and get\nyour next journey',
          style: blackTextStyle.copyWith(
            fontSize: 24,
            fontWeight: semibold,
          ),
        ),
      );
    }

    Widget inputSection() {
      return Container(
        width: double.infinity,
        padding: EdgeInsets.symmetric(horizontal: default_margin, vertical: 30),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(border_radius),
          color: kWhite,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomTextField(
              label: 'Fullname',
              hint: '',
              secure: false,
              controller: fullname,
            ),
            CustomTextField(
              label: 'Email',
              hint: '',
              secure: false,
              margin: EdgeInsets.only(top: 20),
              controller: email,
            ),
            CustomTextField(
              label: 'Password',
              hint: '',
              secure: true,
              margin: EdgeInsets.only(top: 20),
              controller: password,
            ),
            CustomTextField(
              label: 'Hobby',
              hint: '',
              secure: false,
              margin: EdgeInsets.only(top: 20),
              controller: hobby,
            ),
            BlocConsumer<AuthCubit, AuthState>(
              listener: (context, state) {
                // mengecek apakah state berhasil atau tidak
                if (state is AuthSuccess) {
                  Navigator.pushNamedAndRemoveUntil(
                      context, '/bonus-saldo', (route) => false);
                } else if (state is AuthFailed) {
                  // menampilkan snackbar error message
                  Alert(
                          context: context,
                          title: 'Error',
                          desc: state.errorMessage)
                      .show();
                }
              },
              builder: (context, state) {
                // menampilkan loading
                if (state is AuthLoading) {
                  return Center(child: CircularProgressIndicator());
                }
                return CustomButton(
                  name: 'Get Started',
                  onPressed: () {
                    // mengirimkan data ke auth cubit untuk di proses oleh bloc auth
                    context.read<AuthCubit>().signUp(
                        name: fullname.text,
                        email: email.text,
                        password: password.text,
                        hobby: hobby.text);
                  },
                  height: 55,
                  margin: EdgeInsets.only(top: 30),
                );
              },
            ),
          ],
        ),
      );
    }

    Widget tocButton() {
      return Container(
        alignment: Alignment.center,
        margin: EdgeInsets.only(top: 50),
        child: Text(
          'Terms and Conditions',
          style: greyTextStyle.copyWith(
            fontSize: 16,
            fontWeight: light,
            decoration: TextDecoration.underline,
          ),
        ),
      );
    }

    return Scaffold(
      backgroundColor: kBackground,
      body: ListView(
        padding: EdgeInsets.all(default_margin),
        children: [
          title(),
          inputSection(),
          tocButton(),
        ],
      ),
    );
  }
}
