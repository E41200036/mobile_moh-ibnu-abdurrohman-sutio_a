import 'package:airplane/models/place_model.dart';
import 'package:airplane/shared/theme.dart';
import 'package:airplane/ui/pages/choose_seat_page.dart';
import 'package:airplane/ui/widgets/custom_button.dart';
import 'package:airplane/ui/widgets/interest_item.dart';
import 'package:airplane/ui/widgets/photo_item.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DetailPage extends StatefulWidget {
  final PlaceModel place;
  const DetailPage(this.place, {Key? key}) : super(key: key);

  @override
  State<DetailPage> createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  @override
  Widget build(BuildContext context) {
    Widget backgroundImage() {
      return Container(
        width: double.infinity,
        height: 450,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(widget.place.imgUrl),
            fit: BoxFit.cover,
          ),
        ),
        child: Container(
          width: double.infinity,
          height: 214,
          decoration: BoxDecoration(
            gradient: LinearGradient(colors: [
              kWhite.withOpacity(0),
              kBlack.withOpacity(.95),
            ], begin: Alignment.topCenter, end: Alignment.bottomCenter),
          ),
        ),
      );
    }

    Widget content() {
      return Container(
        width: double.infinity,
        margin: EdgeInsets.symmetric(horizontal: default_margin),
        child: SingleChildScrollView(
          child: Column(
            children: [
              // NOTE: emblem
              Container(
                width: 108,
                height: 24,
                margin: EdgeInsets.only(top: 30, bottom: 256),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/icon_emblem.png'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),

              // NOTE: title
              Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.place.name,
                          style: whiteTextStyle.copyWith(
                            fontSize: 24,
                            fontWeight: semibold,
                          ),
                        ),
                        Text(
                          widget.place.city,
                          style: whiteTextStyle.copyWith(
                            fontSize: 16,
                            fontWeight: light,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                    Container(
                      width: 16,
                      height: 16,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage('assets/icon_star.png'),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 2,
                    ),
                    Text(
                      widget.place.rating.toString(),
                      style: whiteTextStyle.copyWith(
                        fontWeight: medium,
                      ),
                    ),
                  ]),
                ],
              ),

              // Note: description
              Container(
                width: double.infinity,
                margin: EdgeInsets.only(top: 30),
                padding: EdgeInsets.only(left: 20, right: 20, bottom: 30),
                decoration: BoxDecoration(
                  color: kWhite,
                  borderRadius: BorderRadius.circular(border_radius),
                ),
                child: Container(
                  margin: EdgeInsets.only(top: 30),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // NOTE: about
                      Text(
                        'About',
                        style: blackTextStyle.copyWith(
                            fontSize: 16, fontWeight: semibold),
                      ),
                      SizedBox(
                        height: 6,
                      ),
                      Text(
                        'Berada di jalur jalan provinsi yang menghubungkan Denpasar Singaraja serta letaknya yang dekat dengan Kebun Raya Eka Karya menjadikan tempat Bali.',
                        style: blackTextStyle.copyWith(
                          height: 2,
                        ),
                      ),

                      // NOTE: photos
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        'Photos',
                        style: blackTextStyle.copyWith(
                            fontSize: 16, fontWeight: semibold),
                      ),
                      SizedBox(
                        height: 6,
                      ),
                      Row(
                        children: [
                          PhotoItem(imgUrl: 'assets/image_photo1.png'),
                          PhotoItem(imgUrl: 'assets/image_photo2.png'),
                          PhotoItem(imgUrl: 'assets/image_photo3.png'),
                        ],
                      ),

                      // NOTE: Interests
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        'Interests',
                        style: blackTextStyle.copyWith(
                            fontSize: 16, fontWeight: semibold),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          InterestItem(name: 'Kids Park'),
                          InterestItem(name: 'Honor Bridge'),
                        ],
                      ),
                      SizedBox(height: 10),
                      Row(
                        children: [
                          InterestItem(name: 'City Museum'),
                          InterestItem(name: 'Central Mall'),
                        ],
                      ),
                    ],
                  ),
                ),
              ),

              // NOTE: button transaction
              Container(
                height: 55,
                margin: EdgeInsets.only(bottom: 30, top: 30),
                child: Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            NumberFormat.currency(
                              locale: 'id ',
                              symbol: 'IDR ',
                            ).format(widget.place.price),
                            style: blackTextStyle.copyWith(
                              fontSize: 18,
                              fontWeight: medium,
                            ),
                          ),
                          SizedBox(height: 5),
                          Text(
                            'per orang',
                            style: greyTextStyle.copyWith(
                              fontWeight: light,
                            ),
                          ),
                        ],
                      ),
                    ),
                    CustomButton(
                      width: 135,
                      name: 'Book Now',
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    ChooseSeatPage(this.widget.place)));
                      },
                      height: 55,
                      margin: EdgeInsets.only(
                        left: 35,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    }

    return Scaffold(
      backgroundColor: kBackground,
      body: Stack(children: [
        // NOTE: This is the background image
        backgroundImage(),
        // NOTE: This is the content of the page
        content(),
      ]),
    );
  }
}
