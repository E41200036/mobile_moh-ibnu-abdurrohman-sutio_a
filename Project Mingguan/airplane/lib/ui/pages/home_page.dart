import 'package:airplane/cubit/auth_cubit.dart';
import 'package:airplane/cubit/place_cubit.dart';
import 'package:airplane/shared/theme.dart';
import 'package:airplane/ui/widgets/destination_item.dart';
import 'package:airplane/ui/widgets/new_destination_item.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import '../../models/place_model.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    context.read<PlaceCubit>().getPlaces();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget header() {
      return BlocBuilder<AuthCubit, AuthState>(
        builder: (context, state) {
          if (state is AuthSuccess) {
            return Container(
              margin: EdgeInsets.only(top: 30, bottom: 30),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Howdy,\n${state.user.name}',
                          style: blackTextStyle.copyWith(
                            fontSize: 24,
                            fontWeight: semibold,
                          ),
                        ),
                        SizedBox(
                          height: 6,
                        ),
                        Text(
                          'Where to fly today?',
                          style: greyTextStyle.copyWith(
                            fontSize: 16,
                            fontWeight: light,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: 60,
                    height: 60,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        image: AssetImage(
                          'assets/image_profile.png',
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            );
          } else {
            return SizedBox();
          }
        },
      );
    }

    Widget popularDestination(List<PlaceModel> places) {
      return Container(
        margin: EdgeInsets.only(bottom: 30),
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
              children: places
                  .map((PlaceModel place) => DestinationItem(
                        place: place,
                      ))
                  .toList()),
        ),
      );
    }

    Widget newYearDestination(List<PlaceModel> places) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'New This Year',
            style: blackTextStyle.copyWith(
              fontSize: 18,
              fontWeight: semibold,
            ),
          ),
          SizedBox(
            height: 16,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: places
                .map((PlaceModel place) => NewDestinationItem(place))
                .toList(),
          ),
          SizedBox(
            height: 100,
          ),
        ],
      );
    }

    return Scaffold(
        backgroundColor: kBackground,
        body: BlocConsumer<PlaceCubit, PlaceState>(
          listener: (context, state) {
            if (state is PlaceFailure) {
              Alert(context: context, title: 'Error', desc: state.error).show();
            }
          },
          builder: (context, state) {
            if (state is PlaceSuccess) {
              return ListView(
                padding: EdgeInsets.symmetric(horizontal: default_margin),
                children: [
                  header(),
                  popularDestination(state.places),
                  newYearDestination(state.places),
                ],
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ));
  }
}
