import 'package:airplane/cubit/auth_cubit.dart';
import 'package:airplane/shared/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BonusSaldoPage extends StatelessWidget {
  const BonusSaldoPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthCubit, AuthState>(
      builder: (context, state) {
        if (state is AuthSuccess) {
          return Scaffold(
            body: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: 300,
                    height: 211,
                    padding: EdgeInsets.all(default_margin),
                    margin: EdgeInsets.only(bottom: 80),
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: kPrimary.withOpacity(.3),
                          offset: Offset(0, 10),
                          blurRadius: 40,
                        ),
                      ],
                      borderRadius: BorderRadius.circular(34),
                      image: DecorationImage(
                        image: AssetImage(
                          'assets/image_card.png',
                        ),
                        fit: BoxFit.none,
                      ),
                    ),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Name',
                                      style: whiteTextStyle.copyWith(
                                          fontWeight: light),
                                    ),
                                    Text(
                                      state.user.name,
                                      overflow: TextOverflow.ellipsis,
                                      style: whiteTextStyle.copyWith(
                                        fontSize: 20,
                                        fontWeight: medium,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Row(
                                children: [
                                  Image.asset(
                                    'assets/icon_plane.png',
                                    width: 24,
                                    height: 24,
                                  ),
                                  SizedBox(
                                    width: 6,
                                  ),
                                  Text(
                                    'Pay',
                                    style: whiteTextStyle.copyWith(
                                      fontSize: 16,
                                      fontWeight: medium,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Balance',
                                style:
                                    whiteTextStyle.copyWith(fontWeight: light),
                              ),
                              Text(
                                'IDR ${state.user.balance}',
                                style: whiteTextStyle.copyWith(
                                  fontWeight: medium,
                                  fontSize: 26,
                                ),
                              ),
                            ],
                          ),
                        ]),
                  ),
                  Text(
                    'Big Bonus 🎉',
                    style: blackTextStyle.copyWith(
                        fontSize: 32, fontWeight: semibold),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'We give you early credit so that\nyou can buy a flight ticket',
                    textAlign: TextAlign.center,
                    style:
                        greyTextStyle.copyWith(fontSize: 16, fontWeight: light),
                  ),
                  SizedBox(
                    height: 50,
                  ),
                  Container(
                    width: 220,
                    height: 55,
                    decoration: BoxDecoration(
                      color: kPrimary,
                      borderRadius: BorderRadius.circular(border_radius),
                    ),
                    child: TextButton(
                      onPressed: () {
                        Navigator.pushNamedAndRemoveUntil(
                            context, '/main', (route) => false);
                      },
                      child: Text(
                        'Start Fly Now',
                        style: whiteTextStyle.copyWith(
                          fontSize: 18,
                          fontWeight: medium,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        } else {
          return Scaffold(
            body: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'You need to login first',
                    style: blackTextStyle.copyWith(
                        fontSize: 16, fontWeight: medium),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    width: 220,
                    height: 55,
                    decoration: BoxDecoration(
                      color: kPrimary,
                      borderRadius: BorderRadius.circular(border_radius),
                    ),
                    child: TextButton(
                      onPressed: () {},
                      child: Text(
                        'Login',
                        style: whiteTextStyle.copyWith(
                          fontSize: 18,
                          fontWeight: medium,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        }
      },
    );
  }
}
