import 'package:airplane/shared/theme.dart';
import 'package:airplane/ui/widgets/custom_button.dart';
import 'package:flutter/material.dart';

class GetStartedPage extends StatelessWidget {
  const GetStartedPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [
        Image.asset(
          'assets/image_get_started.png',
          fit: BoxFit.cover,
          width: double.infinity,
        ),
        Container(
          margin: EdgeInsets.only(top: 513),
          child: Column(
            children: [
              Text(
                'Fly Like a Bird',
                style:
                    whiteTextStyle.copyWith(fontSize: 32, fontWeight: semibold),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                'Explore new world with us and let\nyourself get an amazing experiences',
                style: whiteTextStyle.copyWith(
                  fontSize: 16,
                  fontWeight: light,
                ),
              ),
              SizedBox(
                height: 50,
              ),
              CustomButton(
                name: 'Get Started',
                onPressed: () {
                  Navigator.pushNamed(context, '/sign-up');
                },
                margin: EdgeInsets.symmetric(horizontal: 77),
              ),
            ],
          ),
        )
      ]),
    );
  }
}
