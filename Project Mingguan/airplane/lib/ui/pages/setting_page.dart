import 'package:airplane/cubit/auth_cubit.dart';
import 'package:airplane/shared/theme.dart';
import 'package:airplane/ui/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class SettingPage extends StatelessWidget {
  const SettingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: BlocConsumer<AuthCubit, AuthState>(
          listener: (context, state) {
            // cek apakah state berubah atau tidak
            if (state is AuthInitial) {
              Navigator.pushNamedAndRemoveUntil(
                  context, '/get-started', (route) => false);
            } else if (state is AuthFailed) {
              // jika gagal maka muncul alert dialog
              Alert(context: context, title: 'Error', desc: state.errorMessage)
                  .show();
            }
          },
          builder: (context, state) {
            return CustomButton(
              name: 'Sign Out',
              onPressed: () {
                // jika state berubah maka akan dijalankan fungsi signOut
                context.read<AuthCubit>().signOut();
              },
              margin: EdgeInsets.symmetric(horizontal: default_margin),
            );
          },
        ),
      ),
    );
  }
}
