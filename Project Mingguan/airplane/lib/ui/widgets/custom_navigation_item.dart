import 'package:airplane/shared/theme.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../cubit/page_cubit.dart';

class CustomNavigationItem extends StatelessWidget {
  final String imgUrl;
  final int index;
  const CustomNavigationItem(
      {Key? key, required this.imgUrl, required this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        context.read<PageCubit>().setPage(index);
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(),
          Image.asset(
            imgUrl,
            width: 24,
            height: 24,
            color: context.read<PageCubit>().state == index ? kPrimary : kGrey,
          ),
          Container(
            width: 30,
            height: 2,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(border_radius),
              color: context.read<PageCubit>().state == index
                  ? kPrimary
                  : Colors.transparent,
            ),
          ),
        ],
      ),
    );
  }
}
