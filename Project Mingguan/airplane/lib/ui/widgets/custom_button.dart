import 'package:flutter/material.dart';

import '../../shared/theme.dart';

class CustomButton extends StatelessWidget {
  final String name;
  final Function() onPressed;
  final double width;
  final double height;
  final EdgeInsets margin;

  const CustomButton(
      {Key? key,
      required this.name,
      required this.onPressed,
      this.width = double.infinity,
      this.height = 55,
      required this.margin})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      margin: margin,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(border_radius), color: kPrimary),
      child: TextButton(
        onPressed: onPressed,
        child: Text(
          name,
          style: whiteTextStyle.copyWith(
            fontSize: 18,
            fontWeight: medium,
          ),
        ),
      ),
    );
  }
}
