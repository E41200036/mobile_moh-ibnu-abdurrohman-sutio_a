import 'package:flutter/material.dart';

import '../../shared/theme.dart';

class StatusItem extends StatelessWidget {
  final String imgUrl, name;
  const StatusItem({
    Key? key,
    required this.name,
    required this.imgUrl,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 10),
      child: Row(
        children: [
          Image.asset(
            imgUrl,
            width: 16,
            height: 16,
          ),
          SizedBox(
            width: 6,
          ),
          Text(
            name,
            style: blackTextStyle,
          ),
        ],
      ),
    );
  }
}
