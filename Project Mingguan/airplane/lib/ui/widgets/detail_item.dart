import 'package:flutter/material.dart';

import '../../shared/theme.dart';

class DetailItem extends StatelessWidget {
  final String title;
  final String value;
  final TextStyle style;
  const DetailItem(
      {Key? key, required this.title, required this.style, required this.value})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 16),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Row(
              children: [
                Container(
                  width: 16,
                  height: 16,
                  margin: EdgeInsets.only(right: 6),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/icon_check.png'),
                    ),
                  ),
                ),
                Text(
                  title,
                  overflow: TextOverflow.ellipsis,
                  style: blackTextStyle,
                ),
              ],
            ),
          ),
          Text(value, style: style),
        ],
      ),
    );
  }
}
