import 'package:flutter/material.dart';

import '../../shared/theme.dart';

class CustomTextField extends StatelessWidget {
  final String label, hint;
  final bool secure;
  final double width;
  final EdgeInsets margin;
  final TextEditingController controller;
  const CustomTextField(
      {Key? key,
      required this.label,
      required this.hint,
      required this.secure,
      this.margin = EdgeInsets.zero,
      this.width = double.infinity,
      required this.controller})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            label,
            style: blackTextStyle,
          ),
          SizedBox(
            height: 6,
          ),
          Container(
            height: 55,
            width: width,
            child: Expanded(
              child: TextFormField(
                obscureText: secure,
                controller: controller,
                decoration: InputDecoration(
                  hintText: hint,
                  hintStyle: greyTextStyle.copyWith(fontSize: 16),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(border_radius),
                    borderSide: BorderSide(color: kInactive, width: 1),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(border_radius),
                    borderSide: BorderSide(color: kPrimary, width: 1),
                  ),
                ),
                cursorColor: kBlack,
                style: blackTextStyle.copyWith(fontSize: 16),
                onChanged: (value) {},
              ),
            ),
          ),
        ],
      ),
    );
  }
}
