import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../cubit/seat_cubit.dart';
import '../../shared/theme.dart';

class SeatItem extends StatelessWidget {
  final String id;
  final bool isAvailable;
  const SeatItem({Key? key, required this.id, this.isAvailable = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // menampilkan seat item yang dipilih
    bool isSelected = context.watch<SeatCubit>().isSelected(id);

    return GestureDetector(
      onTap: () {
        if (isAvailable) {
          // mengubah state untuk seat yang dipilih
          context.read<SeatCubit>().setSelected(id);
        }
      },
      child: Container(
        width: 48,
        height: 48,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: isSelected
              ? kPrimary
              : isAvailable
                  ? kAvaialable
                  : kUnavailable,
          border: Border.all(
            color: isAvailable
                ? kPrimary
                : !isAvailable
                    ? kUnavailable
                    : isSelected
                        ? kPrimary
                        : kAvaialable,
            width: isAvailable
                ? 2
                : !isAvailable
                    ? 0
                    : isSelected
                        ? 2
                        : 0,
          ),
        ),
        child: Center(
          child: Text(
            isSelected ? 'YOU' : '',
            style: whiteTextStyle.copyWith(
              fontWeight: semibold,
            ),
          ),
        ),
      ),
    );
  }
}
