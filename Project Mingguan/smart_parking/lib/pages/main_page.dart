import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';
import 'package:smart_parking/pages/parking_page.dart';
import 'package:smart_parking/theme.dart';
import 'package:smart_parking/widget/custom_button.dart';
import 'package:smart_parking/widget/menu_item.dart';
import 'package:smart_parking/widget/parking_item.dart';

class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget header() {
      return Container(
        margin: EdgeInsets.only(top: 60),
        child: Row(
          children: [
            Container(
              width: 48,
              height: 48,
              margin: EdgeInsets.only(right: 16),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                  image: AssetImage(
                    'assets/image_profile.png',
                  ),
                ),
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Morning, Alexia',
                    style: blackTextStyle.copyWith(
                      fontWeight: bold,
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Text(
                    'Let’s parking in safe',
                    style: greyTextStyle.copyWith(
                      fontSize: 12,
                    ),
                  ),
                ],
              ),
            ),
            Icon(
              IconlyLight.filter,
              color: kBlack,
              size: 24,
            )
          ],
        ),
      );
    }

    Widget card() {
      return Container(
        width: double.infinity,
        margin: EdgeInsets.only(top: 32),
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          image: DecorationImage(
            image: AssetImage(
              'assets/image_card.png',
            ),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text(
            'AD 433 FG',
            style: softGreyTextStyle.copyWith(
              fontSize: 12,
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Expanded(
                child: Text(
                  'IDR 300.000',
                  style: whiteTextStyle.copyWith(
                    fontSize: 24,
                    fontWeight: bold,
                  ),
                ),
              ),
              Container(
                width: 83,
                height: 29,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: kWhite,
                ),
                child: TextButton(
                  onPressed: () {},
                  child: Text(
                    'Add Balance',
                    style:
                        blueTextStyle.copyWith(fontSize: 10, fontWeight: bold),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 6,
          ),
          Text(
            'Smart Parking Card',
            style: softGreyTextStyle.copyWith(
              fontSize: 10,
            ),
          ),
        ]),
      );
    }

    Widget menu() {
      return Container(
        margin: EdgeInsets.only(top: 40),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            MenuItem(icon: 'assets/icon_card.png', title: 'Pay'),
            MenuItem(icon: 'assets/icon_scan.png', title: 'Scan'),
            MenuItem(icon: 'assets/icon_top_up.png', title: 'Top Up'),
            MenuItem(icon: 'assets/icon_category.png', title: 'Other'),
          ],
        ),
      );
    }

    Widget parkingHistory() {
      return Container(
        margin: EdgeInsets.only(top: 40),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Parking History',
                  style: blackTextStyle.copyWith(
                    fontWeight: bold,
                  ),
                ),
                Icon(
                  IconlyLight.swap,
                  color: kBlack,
                  size: 24,
                ),
              ],
            ),
            SizedBox(
              height: 35,
            ),
            ParkingItem(
              imgUrl: 'assets/image_place.png',
              place: 'Hotel Royal Jember',
              location: 'Mangli, Jember',
              startTime: '05.00',
              endTime: '06.00',
              status: true,
            ),
            ParkingItem(
              imgUrl: 'assets/image_place2.png',
              place: 'McD Jember',
              location: 'Karimata, Jember',
              startTime: '05.00',
              endTime: '09.00',
              status: false,
            ),
          ],
        ),
      );
    }

    return Container(
      decoration: BoxDecoration(
        gradient: kBackground,
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: ListView(
          padding: EdgeInsets.symmetric(horizontal: 30),
          children: [
            header(),
            card(),
            menu(),
            parkingHistory(),
            CustomButton(
              title: 'Parking now',
              height: 48,
              margin: EdgeInsets.only(top: 32, left: 69, right: 69),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ParkingPage()));
              },
              color: kBlue,
            ),
          ],
        ),
      ),
    );
  }
}
