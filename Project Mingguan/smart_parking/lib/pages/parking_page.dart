import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';
import 'package:smart_parking/widget/custom_button.dart';
import 'package:smart_parking/widget/description_item.dart';
import 'package:smart_parking/widget/photo_item.dart';

import '../theme.dart';

class ParkingPage extends StatelessWidget {
  const ParkingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget header() {
      return Container(
        margin: EdgeInsets.only(top: 60),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            GestureDetector(
              onTap: () => Navigator.pop(context),
              child: Icon(
                IconlyLight.arrow_left_2,
                color: kBlack,
                size: 24,
              ),
            ),
            Text(
              'Location',
              style: blackTextStyle.copyWith(
                fontWeight: medium,
              ),
            ),
            SizedBox(),
          ],
        ),
      );
    }

    Widget searchInput() {
      return Container(
        margin: EdgeInsets.only(top: 32),
        width: double.infinity,
        height: 40,
        padding: EdgeInsets.symmetric(horizontal: 19),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(32),
          color: kSoftGrey,
        ),
        child: Center(
          child: Expanded(
              child: TextFormField(
            decoration: InputDecoration.collapsed(
                hintText: 'Search your favorite menu',
                hintStyle:
                    greyTextStyle.copyWith(fontSize: 12, fontWeight: medium)),
          )),
        ),
      );
    }

    Widget location() {
      return Container(
        margin: EdgeInsets.only(top: 32),
        child: Column(
          children: [
            Text(
              'Mission St, California 05 R',
              style: blackTextStyle.copyWith(
                fontSize: 18,
                fontWeight: bold,
              ),
            ),
            SizedBox(
              height: 32,
            ),
            Container(
              width: double.infinity,
              height: 217,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16),
                image: DecorationImage(
                  image: AssetImage('assets/image_maps.png'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ],
        ),
      );
    }

    Widget description() {
      return Container(
        margin: EdgeInsets.only(top: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Description',
              style: greyTextStyle.copyWith(
                fontSize: 12,
                fontWeight: medium,
              ),
            ),
            SizedBox(
              height: 8,
            ),
            DescriptionItem(
              title: 'Price / Hour',
              value: 'IDR 10.000',
            ),
            DescriptionItem(
              title: 'Open',
              value: '06.00 AM',
            ),
            DescriptionItem(
              title: 'Close',
              value: '09.00 PM',
            ),
          ],
        ),
      );
    }

    Widget photos() {
      return Container(
        margin: EdgeInsets.only(top: 24),
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              PhotoItem(imgUrl: 'assets/image_photo1.png'),
              PhotoItem(imgUrl: 'assets/image_photo2.png'),
              PhotoItem(imgUrl: 'assets/image_photo3.png'),
              PhotoItem(imgUrl: 'assets/image_photo4.png'),
            ],
          ),
        ),
      );
    }

    return Container(
      decoration: BoxDecoration(
        gradient: kBackground,
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body:
            ListView(padding: EdgeInsets.symmetric(horizontal: 30), children: [
          header(),
          searchInput(),
          location(),
          description(),
          photos(),
          Container(
            margin: EdgeInsets.only(top: 32),
            child: Row(children: [
              Container(
                margin: EdgeInsets.only(right: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Available',
                      style: greyTextStyle.copyWith(
                        fontWeight: medium,
                        fontSize: 12,
                      ),
                    ),
                    Text(
                      '13',
                      style: blueTextStyle.copyWith(
                        fontSize: 18,
                        fontWeight: medium,
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(right: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Filled',
                        style: greyTextStyle.copyWith(
                          fontWeight: medium,
                          fontSize: 12,
                        ),
                      ),
                      Text(
                        '3',
                        style: redTextStyle.copyWith(
                          fontSize: 18,
                          fontWeight: medium,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              CustomButton(
                title: 'Park Here',
                height: 48,
                onPressed: () {
                  showBottomSheet(
                    backgroundColor: kWhite,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(32),
                        topRight: Radius.circular(32),
                      ),
                    ),
                    enableDrag: true,
                    elevation: 0,
                    context: context,
                    builder: (context) => Container(
                      width: double.infinity,
                      height: 427,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [],
                      ),
                    ),
                  );
                },
                color: kBlue,
                width: 134,
              ),
            ]),
          ),
        ]),
      ),
    );
  }
}
