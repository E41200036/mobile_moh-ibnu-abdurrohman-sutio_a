import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

LinearGradient kBackground = LinearGradient(
  begin: Alignment.topCenter,
  end: Alignment.bottomCenter,
  colors: [
    Color(0xFFFEFEFE),
    Color(0xFFFAFAFA),
  ],
);

Color kRed = Color(0xffE14546);
Color kBlack = Color(0xff2A2A2A);
Color kGrey = Color(0xffA3A3A3);
Color kBlue = Color(0xff139BFA);
Color kSoftWhite = Colors.white.withOpacity(.12);
Color kWhite = Colors.white;
Color kSoftGrey = Color(0xffF5F5F5);
Color kStroke = Color(0xffC4C4C4);

FontWeight normal = FontWeight.normal;
FontWeight medium = FontWeight.w500;
FontWeight semibold = FontWeight.w600;
FontWeight bold = FontWeight.bold;

TextStyle blackTextStyle = GoogleFonts.dmSans(color: kBlack);
TextStyle greyTextStyle = GoogleFonts.dmSans(color: kGrey);
TextStyle blueTextStyle = GoogleFonts.dmSans(color: kBlue);
TextStyle whiteTextStyle = GoogleFonts.dmSans(color: kWhite);
TextStyle softWhiteTextStyle = GoogleFonts.dmSans(color: kSoftWhite);
TextStyle softGreyTextStyle = GoogleFonts.dmSans(color: kSoftGrey);
TextStyle strokeTextStyle = GoogleFonts.dmSans(color: kStroke);
TextStyle redTextStyle = GoogleFonts.dmSans(color: kRed);
