import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';
import 'package:smart_parking/theme.dart';

class ParkingItem extends StatelessWidget {
  final String imgUrl, place, location;
  final String startTime, endTime;
  final bool status;

  const ParkingItem(
      {Key? key,
      required this.imgUrl,
      required this.place,
      required this.location,
      required this.startTime,
      required this.endTime,
      required this.status})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(8),
      margin: EdgeInsets.only(bottom: 16),
      decoration: BoxDecoration(
        color: kSoftGrey,
        borderRadius: BorderRadius.circular(16),
      ),
      child: Row(
        children: [
          Container(
            width: 58,
            height: 58,
            margin: EdgeInsets.only(right: 14),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(16),
              image: DecorationImage(
                image: AssetImage(imgUrl),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  place,
                  style: blackTextStyle.copyWith(
                    fontSize: 12,
                    fontWeight: bold,
                  ),
                ),
                SizedBox(
                  height: 2,
                ),
                Text(
                  '$startTime - $endTime',
                  style: blueTextStyle.copyWith(
                    fontSize: 10,
                    fontWeight: bold,
                  ),
                ),
                SizedBox(
                  height: 2,
                ),
                Row(
                  children: [
                    Icon(
                      IconlyLight.location,
                      color: kGrey,
                      size: 13,
                    ),
                    SizedBox(
                      width: 4,
                    ),
                    Text(
                      location,
                      style: greyTextStyle.copyWith(
                        fontSize: 10,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
            width: 79,
            height: 29,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: status ? kBlue : kRed,
            ),
            child: TextButton(
                onPressed: () {},
                child: Text(
                  status ? 'Success' : 'Waiting',
                  style: whiteTextStyle.copyWith(fontSize: 10),
                )),
          ),
        ],
      ),
    );
  }
}
