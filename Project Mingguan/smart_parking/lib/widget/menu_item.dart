import 'package:flutter/material.dart';

import '../theme.dart';

class MenuItem extends StatelessWidget {
  final String icon, title;
  const MenuItem({Key? key, required this.icon, required this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: 48,
          height: 48,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: kSoftGrey,
          ),
          padding: EdgeInsets.all(14),
          child: Image.asset(icon),
          margin: EdgeInsets.only(bottom: 8),
        ),
        Text(
          title,
          style: blackTextStyle.copyWith(fontSize: 10, fontWeight: bold),
        )
      ],
    );
  }
}
