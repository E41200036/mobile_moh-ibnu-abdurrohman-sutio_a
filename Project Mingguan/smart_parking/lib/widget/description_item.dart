import 'package:flutter/material.dart';

import '../theme.dart';

class DescriptionItem extends StatelessWidget {
  final String title;
  final String value;
  const DescriptionItem({Key? key, required this.title, required this.value})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 4),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: blackTextStyle.copyWith(
              fontSize: 12,
            ),
          ),
          Text(
            value,
            style: blackTextStyle.copyWith(fontSize: 12, fontWeight: bold),
          ),
        ],
      ),
    );
  }
}
