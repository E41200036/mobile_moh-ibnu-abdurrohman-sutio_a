import 'package:flutter/material.dart';
import 'package:smart_parking/theme.dart';

class CustomButton extends StatelessWidget {
  final String title;
  double width, height;
  final Function() onPressed;
  final Color color;
  final EdgeInsets margin;

  CustomButton(
      {Key? key,
      required this.title,
      this.width = double.infinity,
      required this.height,
      required this.onPressed,
      required this.color,
      this.margin = EdgeInsets.zero})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      margin: margin,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: color,
      ),
      child: TextButton(
        onPressed: onPressed,
        child: Text(
          title,
          style: whiteTextStyle.copyWith(
            fontSize: 16,
            fontWeight: bold,
          ),
        ),
      ),
    );
  }
}
