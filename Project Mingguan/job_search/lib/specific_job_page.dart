import 'package:flutter/material.dart';
import 'package:job_search/theme.dart';

class SpecificJobPage extends StatelessWidget {
  final String companyImage, companyName, category, location, jobTime;
  final double salary;

  SpecificJobPage(
      {required this.companyImage,
      required this.companyName,
      required this.category,
      required this.location,
      required this.jobTime,
      required this.salary});

  @override
  Widget build(BuildContext context) {
    Widget titleHeader() {
      return Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
        Text(
          category,
          style: blackTextStyle.copyWith(
              fontSize: 24, fontWeight: FontWeight.bold),
        ),
        SizedBox(
          height: 4,
        ),
        Text(
          companyName + ' • ' + location,
          style: greyTextStyle.copyWith(fontSize: 18, fontWeight: light),
        ),
        SizedBox(
          height: 4,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Rp.' +
                  salary.toStringAsPrecision(salary.toString().length + 1) +
                  '/mo',
              style: blackTextStyle.copyWith(
                fontWeight: light,
              ),
            ),
            SizedBox(width: 10),
            Container(
              height: 18,
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: (jobTime == 'Full-time')
                    ? Color(0xff717171)
                    : Color(0xffFFCDA64),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(jobTime,
                      style: jobTime == 'Full-time'
                          ? whiteTextStyle.copyWith(fontSize: 10)
                          : blackTextStyle.copyWith(
                              fontSize: 10, fontWeight: FontWeight.bold)),
                ],
              ),
            ),
          ],
        ),
        SizedBox(
          height: 17,
        ),
        Container(
          height: 1,
          margin: EdgeInsets.symmetric(horizontal: 40),
          width: double.infinity,
          color: Color(0xffF6F6F6),
        ),
      ]);
    }

    Widget header() {
      return Column(
        children: [
          Stack(children: [
            Image.asset(
              'assets/image_bg_lainhati.png',
              fit: BoxFit.cover,
              height: 206,
              width: double.infinity,
            ),
            Column(children: [
              SizedBox(
                height: 34,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Icon(
                      Icons.arrow_back,
                      color: Colors.white,
                      size: 30,
                    ),
                    Icon(
                      Icons.share,
                      color: Colors.white,
                      size: 30,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 90,
              ),
              Container(
                width: 100,
                height: 100,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    image: DecorationImage(
                      image: AssetImage(companyImage),
                      fit: BoxFit.cover,
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.1),
                        blurRadius: 10,
                        offset: Offset(0, 10),
                      ),
                    ]),
              ),
            ]),
          ]),
          SizedBox(
            height: 10,
          ),
        ],
      );
    }

    Widget subTitle(String text) {
      return Container(
        margin: EdgeInsets.only(bottom: 12),
        child: Text(
          text,
          style: blackTextStyle.copyWith(
              fontSize: 18, fontWeight: FontWeight.bold),
        ),
      );
    }

    Widget description() {
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 35),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            subTitle('Description'),
            Text(
              'A cup of coffee can make everyone smile through their sadness. Barista doesn’t only make a cup of coffee. They greet, smile, they do their job with all their heart. \nNo need to have an experience, all you have to need are just intention and good attitude. You will work with other baristas as well. If you have no experience with barista, we will give you 3-day training. This job is a part-time job.',
              style: blackTextStyle.copyWith(fontWeight: light),
            ),
            SizedBox(
              height: 16,
            ),
          ],
        ),
      );
    }

    Widget qualificationItems(String text) {
      return Container(
        margin: EdgeInsets.only(bottom: 4, left: 29),
        child: Row(
          children: [
            Icon(Icons.adjust, color: kSoftBlue, size: 18),
            SizedBox(
              width: 15,
            ),
            Text(
              text,
              style: blackTextStyle.copyWith(fontWeight: light),
            ),
          ],
        ),
      );
    }

    Widget qualifications() {
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 35),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            subTitle('Qualifications'),
            qualificationItems('Physically and mentally healthy'),
            qualificationItems('Woman or man'),
            qualificationItems('Minimum has senior high school sertificate'),
            qualificationItems('Have an identity card'),
          ],
        ),
      );
    }

    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            header(),
            titleHeader(),
            description(),
            qualifications(),
          ],
        ),
      ),
      bottomNavigationBar: Container(
        width: double.infinity,
        padding: EdgeInsets.symmetric(vertical: 11, horizontal: 35),
        child: Row(children: [
          Container(
            width: 59,
            height: 48,
            margin: EdgeInsets.only(right: 18),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Color(0xfff6f6f6),
            ),
            child: Icon(
              Icons.bookmark,
              color: kGrey,
            ),
          ),
          Expanded(
            child: Container(
              height: 48,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: kSoftBlue,
              ),
              child: TextButton(
                child: Text('Apply Job',
                    style: whiteTextStyle.copyWith(
                        fontSize: 18, fontWeight: FontWeight.bold)),
                onPressed: () {},
              ),
            ),
          ),
        ]),
      ),
    );
  }
}
