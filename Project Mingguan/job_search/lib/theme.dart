import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color kBlack = Color(0xff333333);
Color kBlue = Color(0xff426FB4);
Color kSoftBlue = Color(0xff5C9DFF);
Color kGrey = Color(0xffA7A5A5);
Color kWhite = Color(0xffFFFFFF);

TextStyle blackTextStyle = GoogleFonts.nunito(
  color: kBlack,
);
TextStyle blueTextStyle = GoogleFonts.nunito(
  color: kBlue,
);
TextStyle greyTextStyle = GoogleFonts.nunito(
  color: kGrey,
);
TextStyle whiteTextStyle = GoogleFonts.nunito(
  color: kWhite,
);

FontWeight normal = FontWeight.w400;
FontWeight light = FontWeight.w300;
FontWeight semibold = FontWeight.w500;
