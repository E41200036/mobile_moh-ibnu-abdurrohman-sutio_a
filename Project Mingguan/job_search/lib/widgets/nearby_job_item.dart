import 'package:flutter/material.dart';
import 'package:job_search/specific_job_page.dart';
import 'package:job_search/theme.dart';

class NearbyJobItem extends StatelessWidget {
  final String companyLogo, companyName, category, jobTime, location;
  final double salary, distance;
  const NearbyJobItem(
      {Key? key,
      required this.companyLogo,
      required this.companyName,
      required this.category,
      required this.jobTime,
      required this.salary,
      required this.distance,
      required this.location})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (_) => SpecificJobPage(
                    companyImage: companyLogo,
                    companyName: companyName,
                    category: category,
                    location: location,
                    jobTime: jobTime,
                    salary: salary)));
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.all(16),
        margin: EdgeInsets.only(bottom: 24),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Color(0xffF6F6F6),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Container(
                  width: 68,
                  height: 68,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    image: DecorationImage(
                        image: AssetImage(companyLogo), fit: BoxFit.cover),
                  ),
                ),
                SizedBox(width: 12),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      companyName,
                      style: blackTextStyle.copyWith(fontSize: 16),
                    ),
                    Text(
                      category,
                      style: blackTextStyle.copyWith(
                          fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    Text(
                      'Rp.' +
                          salary.toStringAsPrecision(
                              salary.toString().length + 1) +
                          '/mo',
                      style: blackTextStyle.copyWith(
                        fontWeight: light,
                      ),
                    )
                  ],
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '~$distance m',
                  style: blackTextStyle.copyWith(fontSize: 16),
                ),
                SizedBox(
                  height: 28,
                ),
                Container(
                  height: 18,
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: (jobTime == 'Full-time')
                        ? Color(0xff717171)
                        : Color(0xffFFCDA64),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(jobTime,
                          style: jobTime == 'Full-time'
                              ? whiteTextStyle.copyWith(fontSize: 10)
                              : blackTextStyle.copyWith(
                                  fontSize: 10, fontWeight: FontWeight.bold)),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
