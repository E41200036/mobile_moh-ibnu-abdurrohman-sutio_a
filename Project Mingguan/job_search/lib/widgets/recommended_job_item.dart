import 'package:flutter/material.dart';
import 'package:job_search/theme.dart';

class JobItem extends StatefulWidget {
  final String companyLogo, companyName, category, location, jobTime;
  final double salary;

  const JobItem(
      {Key? key,
      required this.companyLogo,
      required this.companyName,
      required this.category,
      required this.location,
      required this.jobTime,
      required this.salary})
      : super(key: key);

  @override
  State<JobItem> createState() => _JobItemState();
}

bool isFavorite = false;

class _JobItemState extends State<JobItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 257,
      margin: EdgeInsets.only(right: 24),
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Color(0xffF6F6F6),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // company logo
          Container(
            width: 46,
            height: 46,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                image: AssetImage(widget.companyLogo),
                fit: BoxFit.cover,
              ),
            ),
          ),
          // company name
          SizedBox(
            height: 6,
          ),
          Text(widget.companyName,
              style: blackTextStyle.copyWith(fontSize: 16)),
          Text(
            widget.category,
            style: blackTextStyle.copyWith(
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
          // salary & location
          Row(
            children: [
              Text(
                'Rp.' +
                    widget.salary.toStringAsPrecision(
                        widget.salary.toString().length + 1) +
                    '/mo • ' +
                    widget.location,
                style: blackTextStyle.copyWith(
                  fontWeight: light,
                ),
              )
            ],
          ),
          SizedBox(
            height: 6,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                height: 18,
                padding: EdgeInsets.symmetric(horizontal: 10),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: (widget.jobTime == 'Full-time')
                      ? Color(0xff717171)
                      : Color(0xffFFCDA64),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(widget.jobTime,
                        style: widget.jobTime == 'Full-time'
                            ? whiteTextStyle.copyWith(fontSize: 10)
                            : blackTextStyle.copyWith(
                                fontSize: 10, fontWeight: FontWeight.bold)),
                  ],
                ),
              ),
              GestureDetector(
                onTap: () {
                  setState(() {
                    isFavorite = !isFavorite;
                  });
                },
                child: Icon(
                  isFavorite ? Icons.bookmark : Icons.bookmark_border_outlined,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
