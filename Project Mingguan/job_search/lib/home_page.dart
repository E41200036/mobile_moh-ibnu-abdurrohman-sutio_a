import 'package:flutter/material.dart';
import 'package:job_search/theme.dart';
import 'package:job_search/widgets/nearby_job_item.dart';
import 'package:job_search/widgets/recommended_job_item.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget header() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 53,
          ),
          Text('Hello,',
              style:
                  greyTextStyle.copyWith(fontSize: 20, fontWeight: semibold)),
          Text('Jeremy!',
              style: blackTextStyle.copyWith(
                  fontSize: 30, fontWeight: FontWeight.w800)),
          SizedBox(
            height: 12,
          ),
        ],
      );
    }

    Widget searchField() {
      return Row(
        children: [
          SizedBox(
            height: 12,
          ),
          // search box
          Expanded(
            child: TextFormField(
              style: greyTextStyle.copyWith(fontSize: 14, fontWeight: semibold),
              decoration: InputDecoration(
                hintText: 'What are you looking for?',
                hintStyle:
                    greyTextStyle.copyWith(fontSize: 14, fontWeight: semibold),
                suffixIcon: Icon(Icons.search),
                filled: true,
                fillColor: Color(0xffF6F6F6),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12),
                  borderSide: BorderSide.none,
                ),
                contentPadding: EdgeInsets.symmetric(
                  horizontal: 20,
                  vertical: 13,
                ),
              ),
            ),
          ),
          SizedBox(
            width: 14,
          ),
          Container(
            width: 43,
            height: 40,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: kSoftBlue,
            ),
            child: Center(
              child: Image.asset('assets/icon_filter.png', width: 20),
            ),
          ),
        ],
      );
    }

    Widget recommendedJob() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 28,
          ),
          Text(
            'Recommended for you',
            style: blackTextStyle.copyWith(fontSize: 20, fontWeight: semibold),
          ),
          SizedBox(
            height: 18,
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                JobItem(
                    companyLogo: 'assets/image_google.png',
                    companyName: 'Google',
                    category: 'Web Developer',
                    location: 'Jakarta, Indonesia',
                    jobTime: 'Full-time',
                    salary: 6.500),
                JobItem(
                    companyLogo: 'assets/image_bukalapak.png',
                    companyName: 'Bukalapak',
                    category: 'Social Media Marketing',
                    location: 'Jakarta, Indonesia',
                    jobTime: 'Full-time',
                    salary: 4.300),
              ],
            ),
          ),
        ],
      );
    }

    Widget nearbyJob() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 24,
          ),
          Text(
            'Nearby Job',
            style: blackTextStyle.copyWith(fontSize: 20, fontWeight: semibold),
          ),
          SizedBox(
            height: 18,
          ),
          NearbyJobItem(
            companyLogo: 'assets/image_lainhati.png',
            companyName: 'Kopi Lain Hati',
            category: 'Coffee Barista',
            jobTime: 'Part-time',
            salary: 1.750,
            distance: 120,
            location: 'Jakarta, Indonesia',
          ),
          NearbyJobItem(
            companyLogo: 'assets/image_kitchener.png',
            companyName: 'Oti Fried Chicken',
            category: 'Kitchener',
            jobTime: 'Full-time',
            salary: 3.300,
            distance: 120,
            location: 'Jakarta, Indonesia',
          ),
          NearbyJobItem(
            companyLogo: 'assets/image_bukalapak.png',
            companyName: 'BukaLapak',
            category: 'System Analyst',
            jobTime: 'Full-time',
            salary: 5.300,
            distance: 120,
            location: 'Jakarta, Indonesia',
          ),
        ],
      );
    }

    return Scaffold(
      backgroundColor: Colors.white,
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 35),
        children: [
          header(),
          searchField(),
          recommendedJob(),
          nearbyJob(),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.white,
        elevation: 10,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        iconSize: 26,
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
          BottomNavigationBarItem(
              icon: Icon(Icons.bookmark_border), label: 'Home'),
          BottomNavigationBarItem(
              icon: Icon(Icons.person_outline), label: 'Home'),
        ],
      ),
    );
  }
}
