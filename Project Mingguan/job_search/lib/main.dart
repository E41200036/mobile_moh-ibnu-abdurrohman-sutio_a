import 'package:flutter/material.dart';
import 'package:job_search/home_page.dart';

import 'get_started_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: {
        '/': (context) => GetStartedPage(),
        '/home': (context) => HomePage(),
      },
    );
  }
}
