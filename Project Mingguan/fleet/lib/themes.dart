import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color kBlack = Colors.black;
Color kGrey = Color(0xff868686);
Color kBlue = Color(0xff3366FF);
Color kWhite = Color(0xffffffff);
Color kBackground = Color(0xffF7F8FA);

TextStyle blackTextStyle = GoogleFonts.poppins(
  color: kBlack,
);
TextStyle greyTextStyle = GoogleFonts.poppins(
  color: kGrey,
);
TextStyle blueTextStyle = GoogleFonts.poppins(
  color: kBlue,
);
TextStyle whiteTextStyle = GoogleFonts.poppins(
  color: kWhite,
);

// set of font weight
FontWeight bold = FontWeight.bold;
FontWeight normal = FontWeight.normal;
FontWeight semibold = FontWeight.w600;
FontWeight medium = FontWeight.w500;
