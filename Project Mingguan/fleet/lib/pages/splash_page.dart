import 'package:flutter/material.dart';

import '../themes.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset('assets/image_splash.png',
            width: double.infinity, fit: BoxFit.cover),
        Container(
          width: MediaQuery.of(context).size.width - 60,
          height: 301,
          margin: EdgeInsets.only(top: 43),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: kWhite,
          ),
          padding: EdgeInsets.all(27),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Welcome!',
                style:
                    blackTextStyle.copyWith(fontSize: 24, fontWeight: semibold),
              ),
              SizedBox(
                height: 15,
              ),
              Text(
                'welcome to Fleet Finance, the easy way to improve your finances and help you control expenses and income',
                textAlign: TextAlign.center,
                style: greyTextStyle.copyWith(fontSize: 13),
              ),
              SizedBox(
                height: 70,
              ),
              Container(
                width: 153,
                height: 50,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10), color: kBlue),
                child: TextButton(
                  onPressed: () {
                    Navigator.pushNamedAndRemoveUntil(
                        context, '/home', (route) => false);
                  },
                  child: Text(
                    'Get Started',
                    style: whiteTextStyle.copyWith(fontWeight: semibold),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    ));
  }
}
