import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../themes.dart';
import '../widgets/transaction_item.dart';

class DashboardPage extends StatefulWidget {
  const DashboardPage({Key? key}) : super(key: key);

  @override
  State<DashboardPage> createState() => _DashboardPageState();
}

DraggableScrollableController _controller = DraggableScrollableController();

class _DashboardPageState extends State<DashboardPage> {
  @override
  Widget build(BuildContext context) {
    Widget header() {
      return Container(
        margin: EdgeInsets.only(top: 35),
        child: ListView(
          padding: EdgeInsets.symmetric(horizontal: 30, vertical: 35),
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Welcome,',
                      style: greyTextStyle.copyWith(
                        fontSize: 13,
                        fontWeight: medium,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      'Enna Santana 👋',
                      style: blackTextStyle.copyWith(
                        fontSize: 18,
                        fontWeight: semibold,
                      ),
                    ),
                  ],
                ),
                Container(
                  width: 40,
                  height: 40,
                  padding:
                      EdgeInsets.symmetric(horizontal: 4.33, vertical: 7.5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15), color: kWhite),
                  child: Image.asset('assets/image_user.png'),
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.only(top: 28),
              width: MediaQuery.of(context).size.width - 60,
              height: 170,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25),
                image: DecorationImage(
                  image: AssetImage('assets/image_card.png'),
                  fit: BoxFit.cover,
                ),
              ),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'My savings',
                      style: whiteTextStyle.copyWith(fontSize: 13),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Text(
                      'Rp. 10.430.000',
                      style: whiteTextStyle.copyWith(
                        fontSize: 24,
                        fontWeight: semibold,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      width: 222,
                      height: 3,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: LinearProgressIndicator(
                        value: 0.5,
                        backgroundColor: kWhite,
                        valueColor:
                            AlwaysStoppedAnimation<Color>(Color(0xff102693)),
                      ),
                    ),
                    SizedBox(
                      height: 17,
                    ),
                    Text(
                      'Rp. 10.430.000 / Rp. 40.000.000',
                      style: whiteTextStyle.copyWith(
                        fontSize: 10,
                        fontWeight: medium,
                      ),
                    ),
                  ]),
            ),
            SizedBox(
              height: 30,
            ),
            Row(
              children: [
                Expanded(
                  child: Container(
                    height: 60,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: Color(0xff2C2C2C),
                    ),
                    child: TextButton(
                        onPressed: () {},
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SvgPicture.asset('assets/icon_top.svg'),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              'Save Money',
                              style: whiteTextStyle.copyWith(
                                fontSize: 13,
                                fontWeight: medium,
                              ),
                            ),
                          ],
                        )),
                  ),
                ),
                SizedBox(
                  width: 25,
                ),
                Expanded(
                  child: Container(
                    height: 60,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: Color(0xff2C2C2C),
                    ),
                    child: TextButton(
                      onPressed: () {},
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SvgPicture.asset('assets/icon_scan.svg'),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            'Pay',
                            style: whiteTextStyle.copyWith(
                              fontSize: 13,
                              fontWeight: medium,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      );
    }

    Widget contentContainer() {
      return SizedBox.expand(
        child: DraggableScrollableSheet(
            minChildSize: 0.45,
            controller: _controller,
            maxChildSize: 1,
            initialChildSize: 0.45,
            builder: (buildContext, controller) {
              return Container(
                margin: EdgeInsets.only(top: 34),
                padding: EdgeInsets.symmetric(horizontal: 35),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.vertical(top: Radius.circular(40)),
                  color: kWhite,
                ),
                child: SingleChildScrollView(
                  controller: controller,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Align(
                        alignment: Alignment.center,
                        child: Container(
                          width: 49,
                          height: 4,
                          margin: EdgeInsets.only(top: 26, bottom: 14.5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: kBlue.withOpacity(.10),
                          ),
                        ),
                      ),
                      Center(
                        child: Text(
                          'Transactions History',
                          style: blackTextStyle.copyWith(
                            fontSize: 18,
                            fontWeight: semibold,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 31,
                      ),
                      TransactionItem(
                        status: 'Success',
                        date: 'February 19, 03:25 PM',
                        isUp: true,
                        amount: 100000,
                      ),
                      TransactionItem(
                        status: 'Success',
                        date: 'February 19, 03:25 PM',
                        isUp: true,
                        amount: 100000,
                      ),
                      TransactionItem(
                        status: 'Starbucks Drinks',
                        date: 'February 19, 03:25 PM',
                        isUp: false,
                        amount: 100000,
                      ),
                      TransactionItem(
                        status: 'Payment Invest',
                        date: 'February 19, 03:25 PM',
                        isUp: false,
                        amount: 100000,
                      ),
                    ],
                  ),
                ),
              );
            }),
      );
    }

    return Stack(children: [
      header(),
      contentContainer(),
    ]);
  }
}
