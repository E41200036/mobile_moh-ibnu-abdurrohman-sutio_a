import 'package:fleet/themes.dart';
import 'package:fleet/widgets/portofolio_item.dart';
import 'package:flutter/material.dart';

class PortofolioPage extends StatelessWidget {
  const PortofolioPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget header() {
      return Container(
        width: double.infinity,
        height: 216,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.vertical(bottom: Radius.circular(40)),
          image: DecorationImage(
              image: AssetImage('assets/image_card2.png'), fit: BoxFit.cover),
        ),
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Text(
            'My Portofolio',
            style: whiteTextStyle.copyWith(fontSize: 18, fontWeight: semibold),
          ),
          SizedBox(
            height: 30,
          ),
          Text(
            'Savings Value',
            style: whiteTextStyle.copyWith(fontSize: 13),
          ),
          SizedBox(
            height: 12,
          ),
          Text(
            'Rp 12.480.000',
            style: whiteTextStyle.copyWith(fontSize: 24, fontWeight: semibold),
          ),
        ]),
      );
    }

    return ListView(children: [
      header(),
      Padding(
        padding: EdgeInsets.only(top: 20, left: 30, right: 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            PortofolioItem(
              title: 'pension savings funds',
              savings: 10430000,
              goal: 40000000,
              date: 'February 19',
              imageUrl: 'assets/icon_work.svg',
            ),
            PortofolioItem(
              title: 'Camera',
              savings: 40000000,
              goal: 50000000,
              date: 'February 20',
              imageUrl: 'assets/icon_camera.svg',
            ),
            PortofolioItem(
              title: 'pension savings funds',
              savings: 10430000,
              goal: 40000000,
              date: 'February 19',
              imageUrl: 'assets/icon_work.svg',
            ),
            PortofolioItem(
              title: 'Camera',
              savings: 34430000,
              goal: 50000000,
              date: 'February 20',
              imageUrl: 'assets/icon_camera.svg',
            ),
            Container(
              width: double.infinity,
              height: 50,
              margin: EdgeInsets.only(bottom: 16),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: Colors.white,
              ),
              child: TextButton(
                onPressed: () {},
                child: Text(
                  '+ add portofolio',
                  style: blackTextStyle.copyWith(
                    fontSize: 13,
                    fontWeight: semibold,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    ]);
  }
}
