import 'package:fleet/pages/dashboard_page.dart';
import 'package:fleet/pages/portofolio_page.dart';
import 'package:fleet/themes.dart';
import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

int currentIndex = 0;
List pages = [
  DashboardPage(),
  PortofolioPage(),
];

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackground,
      body: pages[currentIndex],
      bottomNavigationBar: Container(
        width: MediaQuery.of(context).size.width,
        height: 84.5,
        padding: EdgeInsets.only(top: 20, bottom: 18),
        decoration: BoxDecoration(
          color: kWhite,
          boxShadow: [
            BoxShadow(
              color: Color(0xff000000).withOpacity(.05),
              offset: Offset(0, 0),
              blurRadius: 14,
              spreadRadius: 10,
            ),
          ],
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(30),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            GestureDetector(
              onTap: () => setState(() {
                currentIndex = 0;
              }),
              child: Column(
                children: [
                  Icon(
                    IconlyLight.home,
                    color: currentIndex == 0 ? kBlue : Color(0xffABABAB),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    'Home',
                    style: blackTextStyle.copyWith(
                      fontSize: 10,
                      fontWeight: medium,
                      color: currentIndex == 0 ? kBlue : Color(0xffABABAB),
                    ),
                  ),
                ],
              ),
            ),
            GestureDetector(
              onTap: () => setState(() {
                currentIndex = 1;
              }),
              child: Column(
                children: [
                  Icon(
                    IconlyLight.wallet,
                    color: currentIndex == 1 ? kBlue : Color(0xffABABAB),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    'Portofolio',
                    style: blackTextStyle.copyWith(
                      fontSize: 10,
                      fontWeight: medium,
                      color: currentIndex == 1 ? kBlue : Color(0xffABABAB),
                    ),
                  ),
                ],
              ),
            ),
            Column(
              children: [
                Icon(
                  IconlyLight.message,
                  color: currentIndex == 2 ? kBlue : Color(0xffABABAB),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  'Support',
                  style: blackTextStyle.copyWith(
                    fontSize: 10,
                    fontWeight: medium,
                    color: currentIndex == 2 ? kBlue : Color(0xffABABAB),
                  ),
                ),
              ],
            ),
            Column(
              children: [
                Icon(
                  IconlyLight.user,
                  color: currentIndex == 3 ? kBlue : Color(0xffABABAB),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  'Profile',
                  style: blackTextStyle.copyWith(
                    fontSize: 10,
                    fontWeight: medium,
                    color: currentIndex == 3 ? kBlue : Color(0xffABABAB),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
