import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../themes.dart';

class PortofolioItem extends StatelessWidget {
  final String title, imageUrl, date;
  final double savings, goal;
  const PortofolioItem(
      {Key? key,
      required this.title,
      required this.savings,
      required this.goal,
      required this.imageUrl,
      required this.date})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        showBottomSheet(
            enableDrag: true,
            elevation: 0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(
                top: Radius.circular(30),
              ),
            ),
            context: context,
            builder: (context) {
              return Container(
                width: double.infinity,
                height: 640,
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/image_card_background2.png'),
                      fit: BoxFit.cover),
                ),
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: Column(
                  children: [
                    SizedBox(
                      height: 88,
                    ),
                    Text(
                      'Congratulations!',
                      style: blackTextStyle.copyWith(
                        fontSize: 18,
                        fontWeight: semibold,
                      ),
                    ),
                    SizedBox(
                      height: 13,
                    ),
                    Text(
                      'You’ve reached your smart saving goal for\n‘Pension Savings Funds’ and ‘Camera’',
                      textAlign: TextAlign.center,
                      style: greyTextStyle.copyWith(
                        fontSize: 11,
                        fontWeight: medium,
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Container(
                      height: 296,
                      width: 220,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: kWhite,
                      ),
                      padding: EdgeInsets.all(20),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Pension savings Funds',
                              style: blackTextStyle.copyWith(
                                  fontSize: 12, fontWeight: semibold),
                            ),
                            SizedBox(
                              height: 6,
                            ),
                            Container(
                              width: double.infinity,
                              height: 1,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Color(0xff888888).withOpacity(.10),
                              ),
                            ),
                            SizedBox(
                              height: 6,
                            ),
                            Text(
                              '19 February 2021 - 19 June 2021',
                              style: greyTextStyle.copyWith(
                                  fontSize: 10, fontWeight: medium),
                            ),
                            SizedBox(
                              height: 13,
                            ),
                            Align(
                              alignment: Alignment.topRight,
                              child: Text(
                                'Rp. 40.000.000',
                                style: greyTextStyle.copyWith(
                                    fontSize: 11, fontWeight: medium),
                              ),
                            ),
                            SizedBox(
                              height: 18,
                            ),
                            Text(
                              'Camera',
                              style: blackTextStyle.copyWith(
                                  fontSize: 12, fontWeight: semibold),
                            ),
                            SizedBox(
                              height: 6,
                            ),
                            Container(
                              width: double.infinity,
                              height: 1,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Color(0xff888888).withOpacity(.10),
                              ),
                            ),
                            SizedBox(
                              height: 6,
                            ),
                            Text(
                              '19 February 2021 - 19 June 2021',
                              style: greyTextStyle.copyWith(
                                  fontSize: 10, fontWeight: medium),
                            ),
                            SizedBox(
                              height: 13,
                            ),
                            Align(
                              alignment: Alignment.topRight,
                              child: Text(
                                'Rp. 40.000.000',
                                style: greyTextStyle.copyWith(
                                    fontSize: 11, fontWeight: medium),
                              ),
                            ),
                            SizedBox(
                              height: 27,
                            ),
                            Container(
                              width: double.infinity,
                              child: DottedLine(
                                direction: Axis.horizontal,
                                dashColor: kGrey.withOpacity(.10),
                                dashGapLength: 5,
                                dashLength: 8,
                                lineLength: double.infinity,
                                dashRadius: 10,
                              ),
                            ),
                            SizedBox(height: 17),
                            Center(
                              child: Column(
                                children: [
                                  Text(
                                    'Total Savings',
                                    style: greyTextStyle.copyWith(fontSize: 10),
                                  ),
                                  SizedBox(
                                    height: 4,
                                  ),
                                  Text(
                                    'Rp 44.000.000',
                                    style: blackTextStyle.copyWith(
                                      fontSize: 16,
                                      fontWeight: semibold,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ]),
                    ),
                    SizedBox(
                      height: 43,
                    ),
                    Container(
                      width: double.infinity,
                      height: 50,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: kBlue,
                      ),
                      child: TextButton(
                        onPressed: () {},
                        child: Text(
                          'Set another goal savings',
                          style: whiteTextStyle.copyWith(
                            fontSize: 13,
                            fontWeight: semibold,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              );
            });
      },
      child: Container(
        height: 130,
        width: double.infinity,
        margin: EdgeInsets.only(bottom: 20),
        padding: EdgeInsets.symmetric(horizontal: 15),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: kWhite,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(children: [
              Container(
                width: 55,
                height: 55,
                margin: EdgeInsets.only(right: 15, top: 37),
                decoration: BoxDecoration(
                  color: Color(0xffADC8FF),
                  shape: BoxShape.circle,
                ),
                padding: EdgeInsets.all(15),
                child: SvgPicture.asset(imageUrl),
              ),
              Container(
                margin: EdgeInsets.only(top: 19.5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      title,
                      style: blackTextStyle.copyWith(fontWeight: semibold),
                    ),
                    SizedBox(
                      height: 12.5,
                    ),
                    Container(
                      width: 200,
                      height: 3,
                      child: LinearProgressIndicator(
                        value: savings / goal,
                        backgroundColor: Color(0xffE5E5E5),
                        valueColor: AlwaysStoppedAnimation(kBlue),
                      ),
                    ),
                    SizedBox(
                      height: 9,
                    ),
                    Text(
                      "Rp. " +
                          savings.toInt().toString() +
                          " / " +
                          "Rp. " +
                          goal.toInt().toString(),
                      style: greyTextStyle.copyWith(
                        fontSize: 11,
                        fontWeight: medium,
                      ),
                    ),
                  ],
                ),
              ),
            ]),
            SizedBox(
              height: 6,
            ),
            Align(
              alignment: Alignment.bottomRight,
              child: Text('Last saving ' + date,
                  style:
                      greyTextStyle.copyWith(fontSize: 10, fontWeight: medium)),
            ),
          ],
        ),
      ),
    );
  }
}
