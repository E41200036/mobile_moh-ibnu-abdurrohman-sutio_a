import 'package:fleet/themes.dart';
import 'package:flutter/material.dart';

class TransactionItem extends StatelessWidget {
  final String status, date;
  final bool isUp;
  final double amount;
  const TransactionItem(
      {Key? key,
      required this.status,
      required this.date,
      required this.isUp,
      required this.amount})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.only(bottom: 30),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Image.asset(
                isUp ? 'assets/icon_up.png' : 'assets/icon_down.png',
                width: 30,
                height: 30,
              ),
              SizedBox(width: 10),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    status,
                    style: blackTextStyle.copyWith(
                        fontSize: 13, fontWeight: medium),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    date,
                    style: greyTextStyle.copyWith(
                        fontSize: 10, fontWeight: medium),
                  ),
                ],
              ),
            ],
          ),
          Text(
            isUp
                ? '+ ' + amount.toInt().toString()
                : '- ' + amount.toInt().toString(),
            style: blackTextStyle.copyWith(fontSize: 13, fontWeight: semibold),
          )
        ],
      ),
    );
  }
}
