import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color kWhite = Colors.white;
Color kBlack = Color(0xff081D43);
Color kBlue = Color(0xff5077DF);
Color kGrey = Color.fromARGB(5, 254, 254, 254).withOpacity(0.7);
Color kSoftBlack = Color(0xff959dae);
Color kStroke = Color(0xff000000).withOpacity(0.025);
Color kBackground = Color(0xffF8FAFD);

TextStyle whiteTextStyle = GoogleFonts.dmSans(
  color: kWhite,
);
TextStyle blackTextStyle = GoogleFonts.dmSans(
  color: kBlack,
);
TextStyle blueTextStyle = GoogleFonts.dmSans(
  color: kBlue,
);
TextStyle greyTextStyle = GoogleFonts.dmSans(
  color: kGrey,
);
TextStyle softBlackTextStyle = GoogleFonts.dmSans(
  color: kSoftBlack,
);

FontWeight normal = FontWeight.normal;
FontWeight medium = FontWeight.w500;
FontWeight semibold = FontWeight.w600;
FontWeight bold = FontWeight.w700;
