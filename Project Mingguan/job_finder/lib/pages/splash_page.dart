import 'package:flutter/material.dart';
import 'package:job_finder/theme.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Image.asset(
            'assets/image_background.png',
            fit: BoxFit.cover,
            width: double.infinity,
            height: 435,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 24, right: 24, bottom: 32),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Find the Job You\'ve\nAlways Dreamed of',
                  style: blackTextStyle.copyWith(
                    fontSize: 24,
                    fontWeight: medium,
                  ),
                ),
                SizedBox(
                  height: 8,
                ),
                Text(
                  'One of the places where you will find the right job with your field of interest, and you just have to wait for the manager to call you to hire',
                  style: softBlackTextStyle.copyWith(fontSize: 16),
                ),
                SizedBox(
                  height: 73,
                ),
                Container(
                  width: double.infinity,
                  height: 64,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: kBlue,
                  ),
                  child: TextButton(
                    onPressed: () {
                      Navigator.pushNamedAndRemoveUntil(
                          context, '/home', (route) => false);
                    },
                    child: Text(
                      'Get Started',
                      style: whiteTextStyle.copyWith(
                        fontSize: 16,
                        fontWeight: medium,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
