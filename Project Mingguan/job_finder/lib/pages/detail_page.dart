import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';
import 'package:job_finder/theme.dart';
import 'package:tab_indicator_styler/tab_indicator_styler.dart';

class DetailPage extends StatefulWidget {
  final String jobLogo, title, company, location;
  final int minSalary, maxSalary;
  final List workingType;
  final bool isAplied;
  const DetailPage(
      {Key? key,
      required this.jobLogo,
      required this.title,
      required this.company,
      required this.location,
      required this.minSalary,
      required this.maxSalary,
      required this.workingType,
      required this.isAplied})
      : super(key: key);

  @override
  State<DetailPage> createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    TabController _tabController = TabController(length: 2, vsync: this);

    Widget topBar() {
      return Container(
        margin: EdgeInsets.only(top: 24, bottom: 56),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              width: 32,
              height: 32,
              padding: EdgeInsets.all(4),
              decoration: BoxDecoration(
                color: kWhite,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Icon(
                IconlyLight.arrow_left,
              ),
            ),
            Text(
              'Job Detail',
              style: blackTextStyle.copyWith(fontSize: 16, fontWeight: medium),
            ),
            Container(
              width: 32,
              height: 32,
              padding: EdgeInsets.all(4),
              decoration: BoxDecoration(
                color: kWhite,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Icon(
                IconlyLight.bookmark,
              ),
            ),
          ],
        ),
      );
    }

    Widget profile() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: 100,
            height: 100,
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(14),
              color: kWhite,
            ),
            child: Image.asset(widget.jobLogo),
          ),
          SizedBox(
            height: 16,
          ),
          Text(
            widget.company,
            style: blackTextStyle.copyWith(fontSize: 18, fontWeight: medium),
          ),
          SizedBox(
            height: 4,
          ),
          Text(
            widget.location,
            style: blackTextStyle.copyWith(
              fontSize: 14,
              color: kBlack.withOpacity(.70),
            ),
          ),
          SizedBox(
            height: 16,
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
                children: widget.workingType
                    .map((e) => Container(
                          height: 22,
                          margin: EdgeInsets.symmetric(horizontal: 8),
                          padding: EdgeInsets.symmetric(horizontal: 12),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(6),
                            color: kWhite,
                          ),
                          child: Center(
                            child: Text(
                              e,
                              style: blackTextStyle.copyWith(
                                  fontSize: 12, color: kBlack.withOpacity(.50)),
                            ),
                          ),
                        ))
                    .toList()),
          ),
        ],
      );
    }

    Widget responsibility_item(String text) {
      return Row(
        children: [
          Icon(
            Icons.adjust,
            color: kSoftBlack,
            size: 12,
          ),
          SizedBox(
            width: 11,
          ),
          Text(
            text,
            style: softBlackTextStyle.copyWith(fontSize: 12),
          ),
          SizedBox(
            height: 8,
          ),
        ],
      );
    }

    Widget description() {
      return SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'About this Job',
              style: blackTextStyle.copyWith(fontSize: 16, fontWeight: medium),
            ),
            SizedBox(
              height: 8,
            ),
            Text(
              'Currently we are in need of several UI Designers to complete our designer shortage, we hope that with this we can improve the quality of our user interface to customers, because it is very important for',
              style: softBlackTextStyle.copyWith(fontSize: 12),
            ),
            SizedBox(
              height: 16,
            ),
            Text(
              'Job Responsibilities',
              style: blackTextStyle.copyWith(fontSize: 16, fontWeight: medium),
            ),
            SizedBox(
              height: 8,
            ),
            responsibility_item(
                'At least have 3 years of experience as a UI Designer'),
            responsibility_item('Able to work in a team or individually'),
            responsibility_item('Have a good passion in UI Design'),
            SizedBox(
              height: 35,
            ),
            Container(
              width: double.infinity,
              height: 64,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16), color: kBlue),
              child: TextButton(
                onPressed: () {},
                child: Text(
                  'Apply Now',
                  style: whiteTextStyle.copyWith(
                    fontSize: 16,
                    fontWeight: medium,
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    }

    Widget detail() {
      return Container(
        width: double.infinity,
        height: MediaQuery.of(context).size.height - 363,
        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 32),
        margin: EdgeInsets.only(top: 56),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.vertical(top: Radius.circular(28)),
          color: kWhite,
        ),
        child: Column(children: [
          Container(
            width: double.infinity,
            height: 48,
            child: TabBar(
              unselectedLabelColor: kBlack.withOpacity(.70),
              labelStyle: blackTextStyle.copyWith(fontSize: 14),
              controller: _tabController,
              indicator: RectangularIndicator(
                bottomLeftRadius: 12,
                bottomRightRadius: 12,
                color: kBlue,
                topLeftRadius: 12,
                topRightRadius: 12,
                strokeWidth: 0.25,
                paintingStyle: PaintingStyle.fill,
              ),
              tabs: [
                Tab(
                  text: 'Description',
                ),
                Tab(
                  text: 'Company',
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 24),
            width: double.infinity,
            height: 300,
            child: TabBarView(controller: _tabController, children: [
              description(),
              Text('o2'),
            ]),
          ),
        ]),
      );
    }

    return Scaffold(
      backgroundColor: kBackground,
      body: ListView(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 24),
            child: Column(children: [
              topBar(),
              profile(),
            ]),
          ),
          detail(),
        ],
      ),
    );
  }
}
