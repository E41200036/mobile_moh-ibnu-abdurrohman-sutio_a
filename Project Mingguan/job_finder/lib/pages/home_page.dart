import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';
import 'package:job_finder/widgets/job_title.dart';
import 'package:job_finder/widgets/recommendation_job.dart';

import '../theme.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget topBar() {
      return Container(
        margin: EdgeInsets.only(top: 24, bottom: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              width: 32,
              height: 32,
              padding: EdgeInsets.all(4),
              decoration: BoxDecoration(
                color: kWhite,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Icon(
                IconlyLight.category,
              ),
            ),
            Container(
              width: 32,
              height: 32,
              padding: EdgeInsets.all(4),
              decoration: BoxDecoration(
                color: kWhite,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Icon(
                IconlyLight.notification,
              ),
            ),
          ],
        ),
      );
    }

    Widget header() {
      return Container(
        margin: EdgeInsets.only(bottom: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Hello Yeeds!',
              style: blackTextStyle.copyWith(fontSize: 16),
            ),
            SizedBox(
              height: 4,
            ),
            Text(
              'Find Your Dream Job',
              style: blackTextStyle.copyWith(fontSize: 24, fontWeight: bold),
            ),
          ],
        ),
      );
    }

    Widget searchField() {
      return Container(
        margin: EdgeInsets.only(bottom: 24),
        child: Row(
          children: [
            Expanded(
                child: TextFormField(
              style: blackTextStyle,
              decoration: InputDecoration(
                filled: true,
                fillColor: kWhite,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(14),
                  borderSide: BorderSide.none,
                ),
                contentPadding: EdgeInsets.all(12),
                prefixIcon: Icon(IconlyLight.search),
                hintText: 'Search your dream job',
                hintStyle: softBlackTextStyle,
              ),
            )),
            Container(
              margin: EdgeInsets.only(left: 16),
              width: 48,
              height: 48,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(14), color: kBlue),
              child: Icon(
                IconlyLight.filter,
                color: kWhite,
              ),
            ),
          ],
        ),
      );
    }

    Widget popularJob() {
      return Container(
        margin: EdgeInsets.only(bottom: 32),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Popular Job',
              style: blackTextStyle.copyWith(fontSize: 16, fontWeight: medium),
            ),
            SizedBox(
              height: 16,
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(children: [
                JobTitle(
                  jobLogo: 'assets/image_designer.png',
                  title: 'Senior Graphic Designer',
                  company: 'Dsgn Agency',
                  location: 'Jakarta, ID',
                  minSalary: 50,
                  maxSalary: 60,
                  isAplied: true,
                  workingType: [
                    'Full Time',
                    'Remote',
                    'Anywhere',
                  ],
                ),
                JobTitle(
                  jobLogo: 'assets/image_ux.png',
                  title: 'Senior Graphic Designer',
                  company: 'Dsgn Agency',
                  location: 'Jakarta, ID',
                  minSalary: 50,
                  maxSalary: 60,
                  isAplied: false,
                  workingType: [
                    'Full Time',
                    'Remote',
                    'Anywhere',
                  ],
                ),
              ]),
            )
          ],
        ),
      );
    }

    Widget recommendationJob() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Recommendation Job',
            style: blackTextStyle.copyWith(
              fontSize: 16,
              fontWeight: medium,
            ),
          ),
          SizedBox(
            height: 16,
          ),
          Wrap(
              runSpacing: 16,
              spacing: 15,
              crossAxisAlignment: WrapCrossAlignment.center,
              children: [
                RecommendationJob(
                  jobLogo: 'assets/image_tokopedia.png',
                  company: 'Tokopedia',
                  jobType: 'Onsite',
                  category: 'Sr. UI Designer',
                  city: 'Jakarta',
                  country: 'Indonesia',
                ),
                RecommendationJob(
                  jobLogo: 'assets/image_gojek.png',
                  company: 'Gojek',
                  jobType: 'Onsite',
                  category: 'Software Engineer',
                  city: 'Jakarta',
                  country: 'Indonesia',
                ),
                RecommendationJob(
                  jobLogo: 'assets/image_youtube.png',
                  company: 'Youtube',
                  jobType: 'Onsite',
                  category: 'Project Manager',
                  city: 'California',
                  country: 'America',
                ),
                RecommendationJob(
                  jobLogo: 'assets/image_shopee.png',
                  company: 'Shopee',
                  jobType: 'Remote',
                  category: 'UI UX Designer',
                  city: 'Bandung',
                  country: 'Singapore',
                ),
                RecommendationJob(
                  jobLogo: 'assets/image_tokopedia.png',
                  company: 'Tokopedia',
                  jobType: 'Onsite',
                  category: 'Sr. UI Designer',
                  city: 'Jakarta',
                  country: 'Indonesia',
                ),
                RecommendationJob(
                  jobLogo: 'assets/image_gojek.png',
                  company: 'Gojek',
                  jobType: 'Onsite',
                  category: 'Software Engineer',
                  city: 'Jakarta',
                  country: 'Indonesia',
                ),
              ]),
        ],
      );
    }

    return Scaffold(
      backgroundColor: kBackground,
      body: ListView(
        padding: EdgeInsets.all(24),
        children: [
          topBar(),
          header(),
          searchField(),
          popularJob(),
          recommendationJob(),
        ],
      ),
      bottomNavigationBar: Container(
        height: 72,
        child: BottomNavigationBar(
            showSelectedLabels: false,
            showUnselectedLabels: false,
            elevation: 0,
            type: BottomNavigationBarType.fixed,
            unselectedIconTheme: IconThemeData(color: Color(0xff081D43)),
            backgroundColor: Colors.white,
            selectedIconTheme: IconThemeData(
              color: kBlue,
            ),
            items: [
              BottomNavigationBarItem(
                  icon: Icon(
                    IconlyLight.home,
                  ),
                  label: 'Home'),
              BottomNavigationBarItem(
                  icon: Icon(
                    IconlyLight.bookmark,
                  ),
                  label: 'Bookmark'),
              BottomNavigationBarItem(
                  icon: Icon(
                    IconlyLight.message,
                  ),
                  label: 'Mail'),
              BottomNavigationBarItem(
                  icon: Icon(
                    IconlyLight.user_1,
                  ),
                  label: 'User'),
            ]),
      ),
    );
  }
}
