import 'package:flutter/material.dart';
import 'package:job_finder/theme.dart';

import '../pages/detail_page.dart';

class JobTitle extends StatelessWidget {
  final String jobLogo, title, company, location;
  final int minSalary, maxSalary;
  final List workingType;
  final bool isAplied;
  const JobTitle(
      {Key? key,
      required this.jobLogo,
      required this.title,
      required this.company,
      required this.location,
      required this.minSalary,
      required this.maxSalary,
      required this.isAplied,
      required this.workingType})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: ((context) => DetailPage(
                      jobLogo: jobLogo,
                      title: title,
                      company: company,
                      location: location,
                      minSalary: minSalary,
                      maxSalary: maxSalary,
                      workingType: workingType,
                      isAplied: isAplied,
                    ))));
      },
      child: Container(
        width: 252,
        height: 148,
        padding: EdgeInsets.all(16),
        margin: EdgeInsets.only(right: 16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(14),
          color: isAplied ? kBlue : kWhite,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: 48,
                  height: 48,
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: isAplied ? kWhite : Color(0xffF4F6F9),
                  ),
                  child: Image.asset(jobLogo),
                ),
                Text(
                  '\$$minSalary\K - \$$maxSalary\K',
                  style: isAplied
                      ? whiteTextStyle.copyWith(
                          fontWeight: medium,
                        )
                      : blackTextStyle.copyWith(
                          fontWeight: medium,
                        ),
                ),
              ],
            ),
            SizedBox(
              height: 4,
            ),
            Text(
              title,
              style: isAplied
                  ? whiteTextStyle.copyWith(
                      fontWeight: medium,
                    )
                  : blackTextStyle.copyWith(
                      fontWeight: medium,
                    ),
            ),
            SizedBox(
              height: 4,
            ),
            Text(
              '$company · $location',
              style: isAplied
                  ? greyTextStyle.copyWith(
                      fontWeight: medium,
                      fontSize: 12,
                    )
                  : blackTextStyle.copyWith(
                      fontWeight: medium,
                      fontSize: 12,
                    ),
            ),
            SizedBox(
              height: 4,
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: workingType
                    .map((e) => Container(
                          height: 22,
                          padding: EdgeInsets.symmetric(horizontal: 12),
                          margin: EdgeInsets.only(right: 11),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(6),
                              border: Border.all(
                                color: isAplied
                                    ? Colors.transparent
                                    : Color(0xff000033).withOpacity(.20),
                                width: .25,
                              ),
                              color: isAplied ? kWhite : Colors.transparent),
                          child: Center(
                            child: Text(
                              e,
                              style: isAplied
                                  ? blueTextStyle.copyWith(fontSize: 12)
                                  : blackTextStyle.copyWith(
                                      fontSize: 12,
                                      color: kBlack.withOpacity(0.50)),
                            ),
                          ),
                        ))
                    .toList(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
