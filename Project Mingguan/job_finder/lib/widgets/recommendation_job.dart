import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';
import 'package:job_finder/pages/detail_page.dart';
import 'package:job_finder/theme.dart';

class RecommendationJob extends StatelessWidget {
  final String jobLogo, company, jobType, category, city, country;
  const RecommendationJob(
      {Key? key,
      required this.jobLogo,
      required this.company,
      required this.jobType,
      required this.category,
      required this.city,
      required this.country})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: (MediaQuery.of(context).size.width - 63) / 2,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: kWhite,
      ),
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Container(
                width: 32,
                height: 32,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(6),
                  color: Color(0xffF4F6F9),
                ),
                padding: EdgeInsets.all(6),
                child: Image.asset(jobLogo),
              ),
              SizedBox(width: 8),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    company,
                    overflow: TextOverflow.ellipsis,
                    style: blackTextStyle.copyWith(
                        fontSize: 16, fontWeight: medium),
                  ),
                  Text(
                    jobType,
                    style: softBlackTextStyle.copyWith(fontSize: 11),
                  ),
                ],
              )
            ],
          ),
          SizedBox(
            height: 19,
          ),
          Text(
            category,
            style: blackTextStyle.copyWith(fontSize: 12, fontWeight: medium),
          ),
          SizedBox(height: 4),
          SizedBox(
            width: double.infinity,
            child: Row(
              children: [
                Icon(
                  IconlyLight.location,
                  size: 12,
                  color: Color(0xff081D43).withOpacity(0.60),
                ),
                SizedBox(
                  width: 4,
                ),
                SizedBox(
                  width: 100,
                  child: Text(
                    "$city, $country",
                    overflow: TextOverflow.ellipsis,
                    style: softBlackTextStyle.copyWith(
                      fontSize: 12,
                      color: Color(0xff081D43).withOpacity(0.60),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
