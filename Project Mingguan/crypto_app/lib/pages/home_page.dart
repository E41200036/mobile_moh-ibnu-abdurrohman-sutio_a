import 'package:crypto_app/pages/exchange_page.dart';
import 'package:crypto_app/theme.dart';
import 'package:crypto_app/widget/menu_item.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhite,
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 24),
        children: [
          SizedBox(
            height: 50,
          ),
          Center(
            child: Column(children: [
              Container(
                width: 80,
                height: 80,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xffEAEAEA).withOpacity(.45),
                      blurRadius: 19,
                      offset: Offset(0, 23),
                      spreadRadius: -4,
                    ),
                  ],
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage('assets/image_user.png'),
                  ),
                ),
              ),
              SizedBox(
                height: 14,
              ),
              Text(
                'Willa Madelaine',
                style: blackTextStyle.copyWith(
                  fontSize: 18,
                  fontWeight: medium,
                ),
              ),
              SizedBox(height: 2),
              Text(
                '@willamaddy',
                style: softPurpleTextStyle,
              ),
            ]),
          ),
          SizedBox(
            height: 30,
          ),
          Container(
            width: double.infinity,
            height: 210,
            padding: EdgeInsets.all(30),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(35),
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage('assets/card.png'),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Color(0xffEAEAEA).withOpacity(.45),
                    blurRadius: 19,
                    offset: Offset(0, 23),
                    spreadRadius: -4,
                  ),
                ]),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Total Balance',
                  style: softPurpleTextStyle,
                ),
                SizedBox(
                  height: 2,
                ),
                Text(
                  '\$80,450,000',
                  style: whiteTextStyle.copyWith(
                    fontSize: 24,
                    fontWeight: medium,
                  ),
                ),
                SizedBox(
                  height: 44,
                ),
                Text(
                  'This Month Profit',
                  style: softPurpleTextStyle,
                ),
                SizedBox(
                  height: 2,
                ),
                Row(
                  children: [
                    Text(
                      '\$41,000',
                      style: whiteTextStyle.copyWith(fontSize: 16),
                    ),
                    SizedBox(
                      width: 4,
                    ),
                    Container(
                      width: 20,
                      height: 20,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: kGreen,
                      ),
                      child: Center(
                        child: Text(
                          '8%',
                          style: whiteTextStyle.copyWith(
                            fontSize: 8,
                            fontWeight: medium,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(
            height: 30,
          ),
          Text(
            'My Assets',
            style: blackTextStyle.copyWith(fontSize: 16, fontWeight: medium),
          ),
          SizedBox(
            height: 12,
          ),
          SingleChildScrollView(
            clipBehavior: Clip.none,
            scrollDirection: Axis.horizontal,
            child: Row(children: [
              Container(
                width: 140,
                height: 135,
                margin: EdgeInsets.only(right: 16),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(22),
                    border: Border.all(color: kStroke, width: 1),
                    color: kWhite,
                    boxShadow: [
                      BoxShadow(
                        color: Color(0xffEAEAEA).withOpacity(.45),
                        blurRadius: 19,
                        offset: Offset(0, 23),
                        spreadRadius: -4,
                      ),
                    ]),
                padding: EdgeInsets.all(14),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: 40,
                      height: 40,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: kYellow,
                      ),
                      padding: EdgeInsets.all(10),
                      child: Image.asset('assets/icon_bitcoin.png'),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      'BTC',
                      style: greyTextStyle.copyWith(fontSize: 12),
                    ),
                    Text(
                      '\$90,500',
                      style: blackTextStyle.copyWith(
                        fontSize: 18,
                        fontWeight: medium,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                width: 140,
                height: 135,
                margin: EdgeInsets.only(right: 16),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(22),
                    border: Border.all(color: kStroke, width: 1),
                    color: kWhite,
                    boxShadow: [
                      BoxShadow(
                        color: Color(0xffEAEAEA).withOpacity(.45),
                        blurRadius: 19,
                        offset: Offset(0, 23),
                        spreadRadius: -4,
                      ),
                    ]),
                padding: EdgeInsets.all(14),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: 40,
                      height: 40,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: kBlue,
                      ),
                      padding: EdgeInsets.all(10),
                      child: Image.asset('assets/icon_etherium.png'),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      'ETH',
                      style: greyTextStyle.copyWith(fontSize: 12),
                    ),
                    Text(
                      '\$12,100',
                      style: blackTextStyle.copyWith(
                        fontSize: 18,
                        fontWeight: medium,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                width: 140,
                height: 135,
                margin: EdgeInsets.only(right: 16),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(22),
                    border: Border.all(color: kStroke, width: 1),
                    color: kWhite,
                    boxShadow: [
                      BoxShadow(
                        color: Color(0xffEAEAEA).withOpacity(.45),
                        blurRadius: 19,
                        offset: Offset(0, 23),
                        spreadRadius: -4,
                      ),
                    ]),
                padding: EdgeInsets.all(14),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: 40,
                      height: 40,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: kBlue,
                      ),
                      padding: EdgeInsets.all(10),
                      child: Image.asset('assets/icon_doge.png'),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      'DODGE',
                      style: greyTextStyle.copyWith(fontSize: 12),
                    ),
                    Text(
                      '\$50,100',
                      style: blackTextStyle.copyWith(
                        fontSize: 18,
                        fontWeight: medium,
                      ),
                    ),
                  ],
                ),
              ),
            ]),
          ),
          SizedBox(
            height: 30,
          ),
          Text(
            'Main Menu',
            style: blackTextStyle.copyWith(fontSize: 16, fontWeight: medium),
          ),
          SizedBox(
            height: 12,
          ),
          Center(
            child: Wrap(
              crossAxisAlignment: WrapCrossAlignment.center,
              spacing: 22,
              runAlignment: WrapAlignment.center,
              runSpacing: 24,
              clipBehavior: Clip.none,
              children: [
                GestureDetector(
                    onTap: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ExchangePage(),
                          ),
                        ),
                    child: MenuItem(
                        name: 'Analytics', image: 'assets/cy_analystics.png')),
                MenuItem(name: 'Withdraw', image: 'assets/cy_withdraw.png'),
                MenuItem(name: 'Transfer', image: 'assets/cy_deposit.png'),
                MenuItem(name: 'Wallet', image: 'assets/cy_wallet.png'),
                MenuItem(name: 'Message', image: 'assets/cy_message.png'),
                MenuItem(name: 'Verify', image: 'assets/cy_verify.png'),
              ],
            ),
          ),
          SizedBox(
            height: 57,
          ),
        ],
      ),
    );
  }
}
