import 'package:crypto_app/pages/price_page.dart';
import 'package:crypto_app/theme.dart';
import 'package:flutter/material.dart';

class ExchangePage extends StatelessWidget {
  const ExchangePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhite,
      body: ListView(
        padding: EdgeInsets.all(28),
        children: [
          SizedBox(
            height: 70,
          ),
          Text(
            'Exchange Coin',
            textAlign: TextAlign.center,
            style: blackTextStyle.copyWith(fontSize: 20, fontWeight: medium),
          ),
          SizedBox(
            height: 113,
          ),
          Center(
            child: Image.asset(
              'assets/icon_arrow.png',
              width: 280,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  Container(
                    width: 94,
                    height: 94,
                    padding: EdgeInsets.all(18),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(35),
                        color: kYellow),
                    child: Image.asset(
                      'assets/icon_bitcoin.png',
                    ),
                  ),
                  SizedBox(
                    height: 18,
                  ),
                  Text(
                    'BTC',
                    style: greyTextStyle.copyWith(fontSize: 16),
                  ),
                  SizedBox(
                    height: 2,
                  ),
                  Text(
                    '1.390',
                    style: blackTextStyle.copyWith(
                      fontSize: 22,
                      fontWeight: medium,
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  Container(
                    width: 94,
                    height: 94,
                    padding: EdgeInsets.all(18),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(35),
                      color: kBlue,
                    ),
                    child: Image.asset(
                      'assets/icon_etherium.png',
                    ),
                  ),
                  SizedBox(
                    height: 18,
                  ),
                  Text(
                    'ETH',
                    style: greyTextStyle.copyWith(fontSize: 16),
                  ),
                  SizedBox(
                    height: 2,
                  ),
                  Text(
                    '22.890',
                    style: blackTextStyle.copyWith(
                      fontSize: 22,
                      fontWeight: medium,
                    ),
                  ),
                ],
              ),
            ],
          ),
          SizedBox(
            height: 160,
          ),
          Container(
            width: 245,
            height: 45,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(49),
              color: Color(0xff5735D2),
            ),
            child: TextButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => PricePage()));
              },
              child: Text(
                'Procced Now',
                style: whiteTextStyle.copyWith(
                  fontSize: 16,
                  fontWeight: medium,
                ),
              ),
            ),
          ),
          SizedBox(
            height: 30,
          ),
          Text(
            'Terms & Conditions',
            textAlign: TextAlign.center,
            style: greyTextStyle.copyWith(
              fontSize: 16,
              decoration: TextDecoration.underline,
            ),
          )
        ],
      ),
    );
  }
}
