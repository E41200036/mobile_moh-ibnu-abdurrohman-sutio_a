import 'package:crypto_app/theme.dart';
import 'package:crypto_app/widget/price_item.dart';
import 'package:flutter/material.dart';

class PricePage extends StatelessWidget {
  const PricePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhite,
      body: ListView(
        padding: EdgeInsets.all(24),
        children: [
          SizedBox(
            height: 70,
          ),
          Text(
            'Price Alert',
            textAlign: TextAlign.center,
            style: blackTextStyle.copyWith(
              fontSize: 20,
              fontWeight: medium,
            ),
          ),
          SizedBox(
            height: 50,
          ),
          Row(
            children: [
              Container(
                width: 50,
                height: 50,
                margin: EdgeInsets.only(right: 14),
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(18),
                  color: Color(0xff01ABEE),
                ),
                child: Image.asset('assets/icon_doge.png'),
              ),
              Text(
                'DOGE',
                style: blackTextStyle.copyWith(
                    fontSize: 18, fontWeight: bold, color: Color(0xff01ABEE)),
              ),
            ],
          ),
          SizedBox(
            height: 24,
          ),
          PriceItem(price: 90500, status: true, time: '12 mins'),
          PriceItem(price: 80000, status: false, time: '2 days'),
          PriceItem(price: 65000, status: false, time: '1 hour'),
          SizedBox(
            height: 30,
          ),
          Row(
            children: [
              Container(
                width: 50,
                height: 50,
                margin: EdgeInsets.only(right: 14),
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(18),
                  color: kYellow,
                ),
                child: Image.asset('assets/icon_bitcoin.png'),
              ),
              Text(
                'BITCOIN',
                style: yellowTextStyle.copyWith(fontSize: 18, fontWeight: bold),
              ),
            ],
          ),
          SizedBox(
            height: 24,
          ),
          PriceItem(price: 90500, status: true, time: '12 mins'),
          PriceItem(price: 80000, status: false, time: '2 days'),
          PriceItem(price: 65000, status: false, time: '1 hour'),
        ],
      ),
    );
  }
}
