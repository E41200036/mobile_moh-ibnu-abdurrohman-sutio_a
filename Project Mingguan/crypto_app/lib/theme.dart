import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color kBlack = Color(0xff282B6D);
Color kGrey = Color(0xff9BA6B6);
Color kSoftPurple = Color(0xffBFADFF);
Color kWhite = Colors.white;
Color kGreen = Color(0xff58BD7D);
Color kYellow = Color(0xffF7931A);
Color kBlue = Color(0xff2B5BD8);
Color kCyan = Color(0xff01ABEE);
Color kStroke = Color(0xffF5F5F5);

TextStyle blackTextStyle = GoogleFonts.poppins(color: kBlack);
TextStyle greyTextStyle = GoogleFonts.poppins(color: kGrey);
TextStyle softPurpleTextStyle = GoogleFonts.poppins(color: kSoftPurple);
TextStyle whiteTextStyle = GoogleFonts.poppins(color: kWhite);
TextStyle greenTextStyle = GoogleFonts.poppins(color: kGreen);
TextStyle yellowTextStyle = GoogleFonts.poppins(color: kYellow);
TextStyle blueTextStyle = GoogleFonts.poppins(color: kBlue);
TextStyle cyanTextStyle = GoogleFonts.poppins(color: kCyan);
TextStyle strokeTextStyle = GoogleFonts.poppins(color: kStroke);

FontWeight light = FontWeight.w300;
FontWeight normal = FontWeight.normal;
FontWeight medium = FontWeight.w500;
FontWeight semibold = FontWeight.w600;
FontWeight bold = FontWeight.bold;
