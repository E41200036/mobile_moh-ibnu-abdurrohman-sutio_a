import 'package:crypto_app/theme.dart';
import 'package:flutter/material.dart';

class PriceItem extends StatelessWidget {
  final double price;
  final String time;
  final bool status;
  const PriceItem(
      {Key? key, required this.price, required this.status, required this.time})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '$time ago',
                style: greyTextStyle.copyWith(fontSize: 12),
              ),
              SizedBox(
                height: 6,
              ),
              Text(
                status
                    ? 'Above \$${price.toInt()}'
                    : 'Below \$${price.toInt()}',
                style: blackTextStyle.copyWith(
                  fontSize: 18,
                  fontWeight: medium,
                ),
              ),
            ],
          ),
          Image.asset(
            status ? 'assets/ic_up.png' : 'assets/ic_down.png',
            width: 24,
            height: 24,
          ),
        ],
      ),
    );
  }
}
