import 'package:crypto_app/theme.dart';
import 'package:flutter/material.dart';

class MenuItem extends StatelessWidget {
  final String name, image;
  const MenuItem({Key? key, required this.name, required this.image})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 110,
      width: (MediaQuery.of(context).size.width - 100) / 3,
      decoration: BoxDecoration(
        border: Border.all(width: 1, color: kStroke),
        borderRadius: BorderRadius.circular(22),
        color: kWhite,
        boxShadow: [
          BoxShadow(
            color: Color(0xffEAEAEA).withOpacity(.45),
            blurRadius: 19,
            offset: Offset(0, 23),
            spreadRadius: -4,
          ),
        ],
      ),
      child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Image.asset(
          image,
          width: 46,
          height: 46,
        ),
        SizedBox(
          height: 11,
        ),
        Text(
          name,
          style: blackTextStyle.copyWith(
            fontSize: 12,
            fontWeight: medium,
          ),
        ),
      ]),
    );
  }
}
