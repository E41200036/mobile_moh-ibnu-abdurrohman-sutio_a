import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';
import 'package:travelapp/theme.dart';
import 'package:travelapp/ui/pages/detail_page.dart';

class DestinationItem extends StatelessWidget {
  final String imgUrl, name, location;
  final double rating, price;
  const DestinationItem(
      {Key? key,
      required this.imgUrl,
      required this.name,
      required this.location,
      required this.rating,
      required this.price})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => DetailPage(
            imgUrl: imgUrl,
            name: name,
            location: location,
            price: price,
            rating: rating,
          ),
        ),
      ),
      child: Container(
        width: 244,
        margin: EdgeInsets.only(right: 16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(22),
          color: kWhite,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: double.infinity,
              height: 180,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.vertical(
                  top: Radius.circular(22),
                ),
                image: DecorationImage(
                    image: AssetImage(imgUrl), fit: BoxFit.cover),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 12, vertical: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        name,
                        style: blackTextStyle.copyWith(fontWeight: semibold),
                      ),
                      SizedBox(
                        height: 4,
                      ),
                      Row(
                        children: [
                          Icon(
                            IconlyBroken.location,
                            color: kGrey,
                            size: 16,
                          ),
                          Text(
                            " " + location,
                            style: greyTextStyle.copyWith(
                                fontSize: 11, fontWeight: medium),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Icon(
                        IconlyBroken.star,
                        color: kGrey,
                        size: 16,
                      ),
                      Text(
                        " " + rating.toString(),
                        style: greyTextStyle.copyWith(
                          fontSize: 11,
                          fontWeight: semibold,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
