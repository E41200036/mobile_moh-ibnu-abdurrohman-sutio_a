import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';
import 'package:travelapp/theme.dart';

class PackageItem extends StatelessWidget {
  final String imgUrl, name, location;
  final double price;

  const PackageItem(
      {Key? key,
      required this.imgUrl,
      required this.name,
      required this.price,
      required this.location})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 75,
      margin: EdgeInsets.only(bottom: 16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(13),
        color: kWhite,
      ),
      padding: EdgeInsets.all(11),
      child: Row(children: [
        Container(
          width: 55,
          height: 53,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            image:
                DecorationImage(image: AssetImage(imgUrl), fit: BoxFit.cover),
          ),
        ),
        SizedBox(
          width: 12,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              name,
              style: blackTextStyle.copyWith(
                fontSize: 13,
                fontWeight: semibold,
              ),
            ),
            SizedBox(
              height: 4,
            ),
            Row(
              children: [
                Icon(
                  IconlyBroken.location,
                  size: 16,
                  color: kGrey,
                ),
                Text(
                  location,
                  style: greyTextStyle.copyWith(
                    fontSize: 12,
                    fontWeight: medium,
                  ),
                ),
              ],
            )
          ],
        )
      ]),
    );
  }
}
