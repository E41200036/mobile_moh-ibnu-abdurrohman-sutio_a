import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';
import 'package:travelapp/theme.dart';

class DetailPage extends StatefulWidget {
  final String imgUrl, name, location;
  final double rating, price;
  const DetailPage(
      {Key? key,
      required this.imgUrl,
      required this.name,
      required this.location,
      required this.rating,
      required this.price})
      : super(key: key);

  @override
  State<DetailPage> createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> with TickerProviderStateMixin {
  late TabController _tabController;
  @override
  void initState() {
    _tabController = TabController(length: 4, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget header() {
      return Container(
          width: double.infinity,
          height: 390 + 50,
          child: Stack(
            children: [
              Container(
                width: double.infinity,
                height: 390,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(widget.imgUrl),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 41, left: 22, right: 22),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () => Navigator.pop(context),
                      child: Container(
                        width: 39,
                        height: 40,
                        decoration: BoxDecoration(
                          color: kBlack.withOpacity(.60),
                          borderRadius: BorderRadius.circular(11),
                        ),
                        child: Center(
                          child: Icon(
                            IconlyBroken.arrow_left_2,
                            color: kWhite,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      width: 39,
                      height: 40,
                      decoration: BoxDecoration(
                        color: kBlack.withOpacity(.60),
                        borderRadius: BorderRadius.circular(11),
                      ),
                      child: Center(
                        child: Icon(
                          IconlyBold.heart,
                          color: kWhite,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  width: double.infinity,
                  margin: EdgeInsets.symmetric(horizontal: 30),
                  height: 100,
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 22),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(26),
                      color: kWhite,
                      boxShadow: [
                        BoxShadow(
                          offset: Offset(0, 4),
                          blurRadius: 15,
                          color: Color(0xffd0d0d0).withOpacity(.25),
                        ),
                      ]),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.name,
                        style: blackTextStyle.copyWith(
                            fontSize: 18, fontWeight: semibold),
                      ),
                      SizedBox(height: 2),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            '\$${widget.price}',
                            style: orangeTextStyle.copyWith(
                                fontSize: 18, fontWeight: semibold),
                          ),
                          Row(
                            children: [
                              Icon(
                                IconlyBroken.location,
                                size: 16,
                                color: kBlack,
                              ),
                              SizedBox(
                                width: 4,
                              ),
                              Text(
                                widget.location,
                                style: blackTextStyle.copyWith(
                                  fontSize: 12,
                                  fontWeight: medium,
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ));
    }

    Widget activities_item(String imgUrl) {
      return Container(
        width: 120,
        margin: EdgeInsets.only(right: 16),
        height: 80,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(26),
            image:
                DecorationImage(image: AssetImage(imgUrl), fit: BoxFit.cover)),
      );
    }

    Widget details() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Wilson island appeals through its sheer natural beauty of loom volcanoes and lush terraced rice fields that exude peace and  serenity. Wilson enchancts with its dramatic and colourful of a ceremonies",
            textAlign: TextAlign.justify,
            style: softGreyTextStyle.copyWith(
              fontSize: 12,
            ),
          ),
          SizedBox(
            height: 25,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Activities",
                style: blackTextStyle.copyWith(
                  fontSize: 15,
                  fontWeight: semibold,
                ),
              ),
              Text(
                "View all",
                style: orangeTextStyle.copyWith(
                  fontSize: 11,
                  fontWeight: semibold,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 15,
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(children: [
              activities_item('assets/image_place3.png'),
              activities_item('assets/image_place4.png'),
              activities_item('assets/image_place1.png'),
            ]),
          )
        ],
      );
    }

    return Scaffold(
      backgroundColor: kWhite,
      bottomNavigationBar: Container(
        width: double.infinity,
        height: 100,
        padding: EdgeInsets.symmetric(horizontal: 30, vertical: 25),
        decoration: BoxDecoration(
          color: kWhite,
          boxShadow: [
            BoxShadow(
              offset: Offset(0, -4),
              blurRadius: 15,
              color: Color(0xffd6d6d6).withOpacity(.25),
            ),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Total price',
                  style: greyTextStyle.copyWith(
                    fontSize: 10,
                    fontWeight: medium,
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  '\$${widget.price.toString()}',
                  style: orangeTextStyle.copyWith(
                    fontSize: 20,
                    fontWeight: semibold,
                  ),
                ),
              ],
            ),
            Container(
              width: 154,
              height: 50,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(14),
                color: kOrange,
              ),
              child: Center(
                child: TextButton(
                    onPressed: () {},
                    child: Text(
                      'Book Now',
                      style: whiteTextStyle.copyWith(
                        fontSize: 16,
                        fontWeight: medium,
                      ),
                    )),
              ),
            )
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            header(),
            Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(left: 22, right: 22, top: 20),
              height: 28,
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    TabBar(
                        controller: _tabController,
                        isScrollable: true,
                        unselectedLabelColor: kGrey,
                        indicatorColor: kOrange,
                        indicator: BoxDecoration(
                          borderRadius: BorderRadius.circular(25),
                          color: kOrange,
                        ),
                        labelStyle: greyTextStyle.copyWith(
                          fontSize: 13,
                          fontWeight: medium,
                        ),
                        tabs: [
                          Tab(
                            child: Container(
                              width: 64,
                              child: Center(
                                child: Text(
                                  'Details',
                                ),
                              ),
                            ),
                          ),
                          Tab(
                            child: Container(
                              width: 64,
                              child: Center(
                                child: Text(
                                  'Reviews',
                                ),
                              ),
                            ),
                          ),
                          Tab(
                            child: Container(
                              width: 64,
                              child: Center(
                                child: Text(
                                  'Budget',
                                ),
                              ),
                            ),
                          ),
                          Tab(
                            child: Container(
                              width: 64,
                              child: Center(
                                child: Text(
                                  'Highlights',
                                ),
                              ),
                            ),
                          ),
                        ]),
                  ],
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(left: 22, right: 22, top: 12),
              height: MediaQuery.of(context).size.height - 390 - 50,
              child: TabBarView(controller: _tabController, children: [
                details(),
                details(),
                details(),
                details(),
              ]),
            ),
          ],
        ),
      ),
    );
  }
}
