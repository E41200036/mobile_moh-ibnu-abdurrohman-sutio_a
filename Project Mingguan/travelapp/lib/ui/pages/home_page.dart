import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';
import 'package:travelapp/theme.dart';
import 'package:travelapp/ui/widgets/destination_item.dart';
import 'package:travelapp/ui/widgets/package_item.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget header() {
      return Container(
        margin: EdgeInsets.only(top: 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Icon(
                  IconlyBroken.category,
                  size: 20,
                  color: kBlack,
                ),
                Container(
                  width: 36,
                  height: 36,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(6),
                    image: DecorationImage(
                        image: AssetImage('assets/image_user.png'),
                        fit: BoxFit.cover),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 30,
            ),
            Text(
              'Hello Jessica ',
              style:
                  blackTextStyle.copyWith(fontSize: 18, fontWeight: semibold),
            ),
            SizedBox(
              height: 4,
            ),
            Text(
              'Let’s discover best package for you.',
              style: greyTextStyle.copyWith(
                fontSize: 12,
                fontWeight: medium,
              ),
            ),
          ],
        ),
      );
    }

    Widget searchInput() {
      return Container(
        margin: EdgeInsets.only(top: 25, bottom: 25),
        child: Row(
          children: [
            Expanded(
              child: Container(
                height: 45,
                padding: EdgeInsets.symmetric(horizontal: 12, vertical: 10),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  color: Color(0xffF9F9F9),
                ),
                child: Row(
                  children: [
                    Icon(IconlyBroken.search, color: kSoftGrey, size: 20),
                    SizedBox(
                      width: 12,
                    ),
                    Expanded(
                        child: Center(
                      child: TextFormField(
                        decoration: InputDecoration.collapsed(
                          hintText: 'Search your favorite place here.',
                          hintStyle: softGreyTextStyle.copyWith(
                              fontSize: 12, fontWeight: medium),
                        ),
                      ),
                    )),
                  ],
                ),
              ),
            ),
            Container(
              width: 45,
              height: 45,
              margin: EdgeInsets.only(left: 9),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: kSoftWhite,
              ),
              child: Icon(
                IconlyBroken.filter,
                size: 20,
                color: kSoftGrey,
              ),
            ),
          ],
        ),
      );
    }

    Widget recommendedDestination() {
      return Container(
        margin: EdgeInsets.only(bottom: 25),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Recommended',
                    style: blackTextStyle.copyWith(
                      fontSize: 15,
                      fontWeight: semibold,
                    )),
                Text(
                  'View all',
                  style: orangeTextStyle.copyWith(
                      fontSize: 11, fontWeight: semibold),
                ),
              ],
            ),
            SizedBox(
              height: 12,
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  DestinationItem(
                    imgUrl: 'assets/image_place1.png',
                    name: 'Wilson Island Tour',
                    price: 499,
                    location: 'Maldives, Asia ',
                    rating: 4.9,
                  ),
                  DestinationItem(
                    imgUrl: 'assets/image_place2.png',
                    name: 'St. Lucia island',
                    price: 500,
                    location: 'Lucia island ',
                    rating: 4.9,
                  ),
                ],
              ),
            )
          ],
        ),
      );
    }

    Widget popularPackage() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Popular package',
                style: blackTextStyle.copyWith(
                  fontSize: 15,
                  fontWeight: semibold,
                ),
              ),
              Text(
                'View all',
                style: orangeTextStyle.copyWith(
                  fontSize: 11,
                  fontWeight: semibold,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 12,
          ),
          PackageItem(
              imgUrl: 'assets/image_place1.png',
              name: 'Alesund viewpoint package',
              price: 499,
              location: 'Norway'),
          PackageItem(
              imgUrl: 'assets/image_place2.png',
              name: 'Manarola package',
              price: 500,
              location: 'La Spezia, Italy'),
          PackageItem(
              imgUrl: 'assets/image_place3.png',
              price: 800,
              name: 'Heceta head viewpoint package',
              location: 'Florence, USA'),
        ],
      );
    }

    return Scaffold(
      backgroundColor: Color(0xffF8F7FD),
      body: ListView(
        padding: EdgeInsets.all(30),
        children: [
          header(),
          searchInput(),
          recommendedDestination(),
          popularPackage(),
        ],
      ),
    );
  }
}
