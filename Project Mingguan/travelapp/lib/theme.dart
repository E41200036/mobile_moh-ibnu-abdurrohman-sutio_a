import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color kBlack = Colors.black;
Color kGrey = Color(0xff9E9E9E);
Color kSoftGrey = Color(0xffC2C2CA);
Color kSoftWhite = Color(0xffF9F9F9);
Color kOrange = Color(0xffF08A52);
Color kWhite = Color(0xffFFFFFF);

TextStyle blackTextStyle = GoogleFonts.poppins(color: kBlack);
TextStyle greyTextStyle = GoogleFonts.poppins(color: kGrey);
TextStyle softGreyTextStyle = GoogleFonts.poppins(color: kSoftGrey);
TextStyle softWhiteTextStyle = GoogleFonts.poppins(color: kSoftWhite);
TextStyle orangeTextStyle = GoogleFonts.poppins(color: kOrange);
TextStyle whiteTextStyle = GoogleFonts.poppins(color: kWhite);

FontWeight light = FontWeight.w300;
FontWeight normal = FontWeight.w400;
FontWeight medium = FontWeight.w500;
FontWeight semibold = FontWeight.w600;
FontWeight bold = FontWeight.w700;
