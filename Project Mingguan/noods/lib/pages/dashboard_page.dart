import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';
import 'package:noods/pages/home_page.dart';
import 'package:noods/pages/setting_page.dart';

import '../theme.dart';

class DashboardPage extends StatefulWidget {
  const DashboardPage({Key? key}) : super(key: key);

  @override
  State<DashboardPage> createState() => _DashboardPageState();
}

List page = [
  HomePage(),
  SettingPage(),
];
int index = 0;

class _DashboardPageState extends State<DashboardPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBlack,
      body: page[index],
      floatingActionButton: Container(
        width: double.infinity,
        height: 50,
        margin: EdgeInsets.symmetric(horizontal: 30),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(40),
          color: kSoftBlack,
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 40),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              GestureDetector(
                onTap: () => setState(() {
                  index = 0;
                }),
                child: Icon(
                  index == 0 ? IconlyBold.home : IconlyLight.home,
                  color: kWhite,
                ),
              ),
              Icon(
                IconlyLight.calendar,
                color: kWhite,
              ),
              GestureDetector(
                onTap: () => setState(() {
                  index = 1;
                }),
                child: Icon(
                  index == 1 ? IconlyBold.setting : IconlyLight.setting,
                  color: kWhite,
                ),
              ),
            ],
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
