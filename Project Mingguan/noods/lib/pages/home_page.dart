import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';

import '../theme.dart';
import '../widgets/category_item.dart';
import 'detail_page.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.symmetric(horizontal: 30),
      children: [
        SizedBox(
          height: 53,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Getting started\nis super easy!',
              style:
                  whiteTextStyle.copyWith(fontSize: 24, fontWeight: semibold),
            ),
            Container(
              width: 50,
              height: 50,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                    image: AssetImage('assets/image_user.png'),
                    fit: BoxFit.cover),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 39,
        ),
        Container(
          width: double.infinity,
          height: 50,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(40),
            color: kSoftBlack,
          ),
          padding: EdgeInsets.symmetric(horizontal: 29),
          child: Expanded(
              child: Row(
            children: [
              Icon(
                IconlyBroken.search,
                color: kWhite,
                size: 22,
              ),
              SizedBox(
                width: 5,
              ),
              Expanded(
                  child: TextFormField(
                decoration: InputDecoration.collapsed(
                    hintText: 'Search', hintStyle: whiteTextStyle),
              )),
            ],
          )),
        ),
        SizedBox(
          height: 20,
        ),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              CategoryItem(name: 'Design'),
              CategoryItem(name: 'Music'),
              CategoryItem(name: 'Art'),
            ],
          ),
        ),
        SizedBox(
          height: 18,
        ),
        Text(
          'Popular',
          style: whiteTextStyle.copyWith(fontSize: 18, fontWeight: semibold),
        ),
        SizedBox(
          height: 21,
        ),
        Container(
          width: double.infinity,
          height: 368,
          decoration: BoxDecoration(
            border: Border.all(color: kStroke, width: 1),
            color: kSoftBlack,
            borderRadius: BorderRadius.circular(20),
          ),
          padding: EdgeInsets.all(15),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Container(
              width: double.infinity,
              height: 197,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                image: DecorationImage(
                  image: AssetImage('assets/image_course.png'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text.rich(TextSpan(children: [
              TextSpan(
                text: 'IDR 100.000',
                style: softPurpleTextStyle.copyWith(
                  fontWeight: semibold,
                ),
              ),
              TextSpan(
                text: ' • ',
                style: whiteTextStyle,
              ),
              TextSpan(
                text: 'Team',
                style: whiteTextStyle.copyWith(
                  fontSize: 12,
                ),
              ),
            ])),
            SizedBox(
              height: 4,
            ),
            Text(
              'Hack A Design',
              style: whiteTextStyle.copyWith(
                fontSize: 24,
                fontWeight: bold,
              ),
            ),
            SizedBox(
              height: 17,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Icon(
                          IconlyLight.user,
                          color: kWhite,
                          size: 20,
                        ),
                        SizedBox(
                          width: 11,
                        ),
                        Text(
                          'Deviant art',
                          style: whiteTextStyle,
                        )
                      ],
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    Row(
                      children: [
                        Icon(
                          IconlyLight.calendar,
                          color: kWhite,
                          size: 20,
                        ),
                        SizedBox(
                          width: 11,
                        ),
                        Text(
                          '25 - 30 Dec 2021',
                          style: whiteTextStyle,
                        )
                      ],
                    ),
                  ],
                ),
                Container(
                  width: 92,
                  height: 39,
                  decoration: BoxDecoration(
                    gradient: kPurple,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: TextButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => DetailPage()));
                    },
                    child: Text(
                      'Join',
                      style: whiteTextStyle.copyWith(
                        fontWeight: semibold,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ]),
        ),
      ],
    );
  }
}
