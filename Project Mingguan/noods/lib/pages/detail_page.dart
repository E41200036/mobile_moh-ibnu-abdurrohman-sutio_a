import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';
import 'package:noods/theme.dart';

class DetailPage extends StatelessWidget {
  const DetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(
      children: [
        Stack(
          children: [
            Container(
              width: double.infinity,
              height: 448,
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/image_course.png'),
                    fit: BoxFit.cover),
              ),
            ),
            Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height,
              child: DraggableScrollableSheet(
                  minChildSize: 0.5,
                  initialChildSize: 0.5,
                  maxChildSize: 1,
                  builder: (context, scrollController) {
                    return Container(
                      width: double.infinity,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.vertical(
                          top: Radius.circular(30),
                        ),
                        color: kBlack,
                      ),
                      padding: EdgeInsets.all(30),
                      child: SingleChildScrollView(
                        controller: scrollController,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Hack A Design',
                                style: whiteTextStyle.copyWith(
                                  fontSize: 24,
                                  fontWeight: bold,
                                ),
                              ),
                              SizedBox(
                                height: 12,
                              ),
                              Text(
                                'Owner',
                                style: darkGreyTextStyle,
                              ),
                              SizedBox(
                                height: 6,
                              ),
                              Text(
                                'Deviant art',
                                style: whiteTextStyle.copyWith(
                                    fontWeight: semibold),
                              ),
                              SizedBox(
                                height: 12,
                              ),
                              Text(
                                'Price',
                                style: darkGreyTextStyle,
                              ),
                              SizedBox(
                                height: 3,
                              ),
                              Text.rich(TextSpan(children: [
                                TextSpan(
                                  text: 'IDR 100.000',
                                  style: softPurpleTextStyle.copyWith(
                                    fontWeight: semibold,
                                  ),
                                ),
                                TextSpan(
                                  text: ' • ',
                                  style: whiteTextStyle,
                                ),
                                TextSpan(
                                  text: 'Team',
                                  style: whiteTextStyle.copyWith(
                                    fontSize: 12,
                                  ),
                                ),
                              ])),
                              SizedBox(
                                height: 12,
                              ),
                              Text(
                                'Date',
                                style: darkGreyTextStyle,
                              ),
                              SizedBox(
                                height: 6,
                              ),
                              Text(
                                '25 - 30 Dec 2021',
                                style: whiteTextStyle,
                              ),
                              SizedBox(
                                height: 54,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.pop(context);
                                    },
                                    child: Row(
                                      children: [
                                        Icon(
                                          IconlyLight.arrow_left_2,
                                          color: kWhite,
                                          size: 18,
                                        ),
                                        SizedBox(
                                          width: 4,
                                        ),
                                        Text(
                                          'Back',
                                          style: whiteTextStyle,
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    width: 92,
                                    height: 39,
                                    decoration: BoxDecoration(
                                      gradient: kPurple,
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: TextButton(
                                      onPressed: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    DetailPage()));
                                      },
                                      child: Text(
                                        'Join',
                                        style: whiteTextStyle.copyWith(
                                          fontWeight: semibold,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ]),
                      ),
                    );
                  }),
            ),
          ],
        ),
      ],
    ));
  }
}
