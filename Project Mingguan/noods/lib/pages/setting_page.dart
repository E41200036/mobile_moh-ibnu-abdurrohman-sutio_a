import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';
import 'package:noods/theme.dart';
import 'package:noods/widgets/setting_item.dart';

class SettingPage extends StatelessWidget {
  const SettingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget header() {
      return Container(
        margin: EdgeInsets.only(top: 120),
        child: Column(children: [
          Container(
            width: 84,
            height: 84,
            margin: EdgeInsets.only(bottom: 20),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                image: AssetImage(
                  'assets/image_user.png',
                ),
              ),
            ),
          ),
          Text(
            'Alex Horrison',
            style: whiteTextStyle.copyWith(
              fontSize: 16,
              fontWeight: bold,
            ),
          ),
          Text(
            '@alexhorison22',
            style: whiteTextStyle.copyWith(fontSize: 12),
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            width: 101,
            height: 40,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              gradient: kPurple,
            ),
            child: TextButton(
              onPressed: () {},
              child: Text(
                'Edit Profile',
                style: whiteTextStyle.copyWith(
                  fontSize: 12,
                  fontWeight: semibold,
                ),
              ),
            ),
          ),
        ]),
      );
    }

    Widget content() {
      return Container(
        margin: EdgeInsets.only(top: 100, bottom: 60),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Content',
              style: darkGreyTextStyle.copyWith(fontSize: 12),
            ),
            SizedBox(
              height: 30,
            ),
            SettingItem(
              icon: Icon(
                IconlyLight.notification,
                color: kWhite,
                size: 20,
              ),
              name: 'Notification',
            ),
            SettingItem(
              icon: Icon(
                IconlyLight.download,
                color: kWhite,
                size: 20,
              ),
              name: 'Download',
            ),
            SizedBox(
              height: 6,
            ),
            Text(
              'CONFIGURATION',
              style: darkGreyTextStyle.copyWith(fontSize: 12),
            ),
            SizedBox(
              height: 30,
            ),
            SettingItem(
              icon: Icon(
                IconlyLight.user,
                color: kWhite,
                size: 20,
              ),
              name: 'Account',
            ),
            SettingItem(
              icon: Icon(
                IconlyLight.calendar,
                color: kWhite,
                size: 20,
              ),
              name: 'Events',
            ),
            SettingItem(
              icon: Icon(
                IconlyLight.home,
                color: kWhite,
                size: 20,
              ),
              name: 'Store',
            ),
          ],
        ),
      );
    }

    return Scaffold(
      backgroundColor: kBlack,
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 30),
        children: [
          header(),
          content(),
        ],
      ),
    );
  }
}
