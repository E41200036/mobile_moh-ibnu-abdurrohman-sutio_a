import 'package:flutter/material.dart';
import 'package:noods/theme.dart';

class CategoryItem extends StatefulWidget {
  final String name;
  const CategoryItem({Key? key, required this.name}) : super(key: key);

  @override
  State<CategoryItem> createState() => _CategoryItemState();
}

bool isActive = false;

class _CategoryItemState extends State<CategoryItem> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          isActive = !isActive;
        });
      },
      child: Container(
        height: 39,
        margin: EdgeInsets.only(right: 16),
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        decoration: BoxDecoration(
            gradient: isActive ? kPurple : null,
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: isActive ? Colors.transparent : kStroke)),
        child: Center(
          child: Text(
            widget.name,
            style: whiteTextStyle,
          ),
        ),
      ),
    );
  }
}
