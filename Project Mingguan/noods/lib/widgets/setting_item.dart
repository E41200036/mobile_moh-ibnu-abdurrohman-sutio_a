import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';
import 'package:noods/theme.dart';

class SettingItem extends StatelessWidget {
  final Icon icon;
  final String name;
  const SettingItem({Key? key, required this.icon, required this.name})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 24),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              icon,
              SizedBox(
                width: 12,
              ),
              Text(
                name,
                style: whiteTextStyle,
              ),
            ],
          ),
          Icon(
            IconlyLight.arrow_right_2,
            size: 20,
            color: kWhite,
          ),
        ],
      ),
    );
  }
}
