import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color kBlack = Color(0xff2A2A2A);
Color kWhite = Colors.white;
LinearGradient kPurple = LinearGradient(colors: [
  Color(0xff6335E7),
  Color(0xff582FCB),
]);
Color kStroke = Color(0xff434343);
Color kSoftBlack = Color(0xff313030);
Color kSoftPurple = Color(0xff9773D3);
Color kDarkGrey = Color(0xff656565);

TextStyle blackTextStyle = GoogleFonts.manrope(color: kBlack);
TextStyle whiteTextStyle = GoogleFonts.manrope(color: kWhite);
TextStyle softPurpleTextStyle = GoogleFonts.manrope(color: kSoftPurple);
TextStyle darkGreyTextStyle = GoogleFonts.manrope(color: kDarkGrey);

FontWeight semibold = FontWeight.w600;
FontWeight bold = FontWeight.w700;
FontWeight normal = FontWeight.w400;
