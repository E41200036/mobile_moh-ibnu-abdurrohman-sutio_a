import 'package:flutter/material.dart';
import 'package:noods/pages/dashboard_page.dart';
import 'package:noods/pages/splash_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: {
        '/': (context) => SplashPage(),
        '/dashboard': (context) => DashboardPage(),
      },
    );
  }
}
