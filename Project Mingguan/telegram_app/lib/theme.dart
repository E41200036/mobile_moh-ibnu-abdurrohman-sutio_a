import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color kWhite = Color(0xffFFFFFF);
Color kBlack = Color(0xff252836);
Color kSoftBlack = Color(0xff2F3142);
Color kGrey = Color(0xff959CA4);
Color kGreen = Color(0xff33BA00);
Color kBlue = Color(0xff3988FC);
Color kPurple = Color(0xff6A6AE3);

FontWeight normal = FontWeight.normal;
FontWeight medium = FontWeight.w500;
FontWeight semibold = FontWeight.w600;

TextStyle whiteTextStyle = GoogleFonts.poppins(color: kWhite);
TextStyle blackTextStyle = GoogleFonts.poppins(color: kBlack);
TextStyle greyTextStyle = GoogleFonts.poppins(color: kGrey);
TextStyle purpleTextStyle = GoogleFonts.poppins(color: kPurple);
