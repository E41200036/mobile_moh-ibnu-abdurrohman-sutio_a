import 'package:flutter/material.dart';
import 'package:telegram_app/theme.dart';
import 'package:telegram_app/widgets/chat_item.dart';
import 'package:telegram_app/widgets/sidebar_menu_item.dart';
import 'package:telegram_app/widgets/status_item.dart';

class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget header() {
      return Container(
        margin: EdgeInsets.only(top: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              width: 40,
              height: 40,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                    image: AssetImage('assets/image_profile.png'),
                    fit: BoxFit.cover),
              ),
            ),
            Image.asset(
              'assets/icon_menu.png',
              width: 18,
              height: 14,
            ),
          ],
        ),
      );
    }

    Widget status() {
      return Container(
        margin: EdgeInsets.only(top: 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Online',
                  style: whiteTextStyle.copyWith(
                      fontSize: 20, fontWeight: semibold),
                ),
                Container(
                  width: 20,
                  height: 20,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(2),
                    color: kSoftBlack,
                  ),
                  child: Center(
                      child: Text(
                    '2',
                    style: whiteTextStyle.copyWith(
                      fontSize: 12,
                      fontWeight: semibold,
                    ),
                  )),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(children: [
                Container(
                  width: 50,
                  height: 58,
                  margin: EdgeInsets.only(right: 20),
                  child: Stack(
                    children: [
                      Container(
                        width: 50,
                        height: 50,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            image: AssetImage(
                              'assets/image_profile.png',
                            ),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(
                          width: 22,
                          height: 22,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: kBlue,
                            border: Border.all(color: kSoftBlack, width: 2),
                          ),
                          child: Center(
                            child: Icon(
                              Icons.add,
                              color: kWhite,
                              size: 18,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                StatusItem(img: 'assets/image_photo1.png'),
                StatusItem(img: 'assets/image_photo2.png'),
                StatusItem(img: 'assets/image_photo3.png'),
                StatusItem(img: 'assets/image_photo4.png'),
              ]),
            )
          ],
        ),
      );
    }

    Widget chat() {
      return Container(
        margin: EdgeInsets.only(top: 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Messages',
                  style: whiteTextStyle.copyWith(
                      fontSize: 20, fontWeight: semibold),
                ),
                Icon(
                  Icons.search,
                  color: kGrey,
                  size: 22,
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            ChatItem(
              img: 'assets/image_profile.png',
              name: 'Ibnnu',
              message: 'Kirim file nya bro!',
              time: 'Now',
            ),
            ChatItem(
              img: 'assets/image_photo1.png',
              name: 'Fikri',
              message: 'Kamu dimana?',
              time: 'Now',
            ),
            ChatItem(
              img: 'assets/image_photo2.png',
              name: 'Haris',
              message: 'Kita ketemu dirumah ku',
              time: 'Now',
            ),
            ChatItem(
              img: 'assets/image_photo3.png',
              name: 'Amalia',
              message: 'Aku sudah selesai',
              time: 'Now',
            ),
            ChatItem(
              img: 'assets/image_photo4.png',
              name: 'Nomad',
              message: 'Hari ini dimana?',
              time: 'Now',
            ),
            ChatItem(
              img: 'assets/image_photo5.png',
              name: 'Hari',
              message: 'Kamu masuk kelompoku ya?',
              time: 'Now',
            ),
          ],
        ),
      );
    }

    return Scaffold(
      drawer: Drawer(
        backgroundColor: kBlack,
        child: Container(
          padding: EdgeInsets.only(top: 60, left: 24, right: 24, bottom: 40),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: 80,
                height: 80,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                      image: AssetImage('assets/image_photo4.png'),
                      fit: BoxFit.cover),
                ),
              ),
              SizedBox(
                height: 16,
              ),
              Text(
                'Ibnnu A.S',
                style: whiteTextStyle.copyWith(
                  fontSize: 16,
                  fontWeight: semibold,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    '+62 812-345-6789',
                    style: greyTextStyle.copyWith(
                      fontSize: 14,
                      fontWeight: normal,
                    ),
                  ),
                  Icon(
                    Icons.arrow_drop_down_outlined,
                    color: kGrey,
                    size: 22,
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 16),
                width: double.infinity,
                height: 1.5,
                decoration: BoxDecoration(
                  color: kSoftBlack,
                ),
              ),
              SizedBox(
                height: 16,
              ),
              SidebarMenuItem(
                title: 'New Group',
                icon: Icons.account_circle,
                color: kBlue,
              ),
              SidebarMenuItem(
                title: 'New Channel',
                icon: Icons.videocam_rounded,
                color: Color.fromARGB(255, 231, 156, 7),
              ),
              SidebarMenuItem(
                title: 'Contact',
                icon: Icons.contact_page,
                color: Color.fromARGB(255, 231, 50, 50),
              ),
              SidebarMenuItem(
                title: 'Calls',
                icon: Icons.call,
                color: Color.fromARGB(255, 18, 213, 60),
              ),
              Spacer(),
              Text(
                'Telegramnya Ibnu',
                style: greyTextStyle.copyWith(
                  fontSize: 16,
                  fontWeight: semibold,
                ),
              ),
              SizedBox(
                height: 4,
              ),
              Text(
                'Version 1.0.0',
                style: greyTextStyle.copyWith(
                  fontSize: 12,
                  fontWeight: normal,
                ),
              ),
            ],
          ),
        ),
      ),
      backgroundColor: Color(0xff252836),
      body: ListView(
        padding: EdgeInsets.all(30),
        children: [
          header(),
          status(),
          chat(),
        ],
      ),
      bottomNavigationBar: Container(
        width: double.infinity,
        height: 80,
        padding: EdgeInsets.symmetric(vertical: 16),
        decoration: BoxDecoration(
          color: kSoftBlack,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Column(
              children: [
                Image.asset(
                  'assets/icon_chat.png',
                  width: 24,
                  height: 24,
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  'Chats',
                  style: purpleTextStyle.copyWith(
                    fontSize: 12,
                    fontWeight: semibold,
                  ),
                ),
              ],
            ),
            Column(
              children: [
                Image.asset(
                  'assets/icon_call.png',
                  width: 24,
                  height: 24,
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  'Calls',
                  style: greyTextStyle.copyWith(
                    fontSize: 12,
                    fontWeight: semibold,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
