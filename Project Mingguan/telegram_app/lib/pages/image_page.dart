import 'package:flutter/material.dart';
import 'package:telegram_app/theme.dart';

class ImagePage extends StatelessWidget {
  final String img;
  final String name;
  const ImagePage({Key? key, required this.img, required this.name})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBlack,
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: 100,
            height: 100,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                image: AssetImage(img),
                fit: BoxFit.cover,
              ),
            ),
          ),
          SizedBox(
            height: 16,
          ),
          Text(
            name,
            style: whiteTextStyle.copyWith(
              fontSize: 16,
              fontWeight: semibold,
            ),
          ),
        ],
      )),
    );
  }
}
