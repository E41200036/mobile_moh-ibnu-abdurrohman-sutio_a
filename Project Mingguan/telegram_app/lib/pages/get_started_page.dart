import 'package:flutter/material.dart';
import 'package:telegram_app/theme.dart';

class GetStartedPage extends StatelessWidget {
  const GetStartedPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBlack,
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 24),
        children: [
          SizedBox(height: 130),
          Image.asset(
            'assets/image_get_started.png',
            width: double.infinity,
            height: 285,
          ),
          SizedBox(
            height: 129,
          ),
          Text(
            'Instant Messaging, Simple \nAnd Personal',
            style: whiteTextStyle.copyWith(
              fontSize: 24,
              fontWeight: semibold,
            ),
          ),
          SizedBox(height: 10),
          Text(
            'Start your new journey in the Chat Us',
            style: greyTextStyle,
          ),
          SizedBox(
            height: 25,
          ),
          Align(
            alignment: Alignment.bottomLeft,
            child: Container(
              width: 180,
              height: 55,
              decoration: BoxDecoration(
                color: kPurple,
                borderRadius: BorderRadius.circular(12),
              ),
              child: TextButton(
                onPressed: () {
                  Navigator.pushNamedAndRemoveUntil(
                      context, '/main', (route) => false);
                },
                child: Text(
                  'Let’s Begin',
                  style: whiteTextStyle.copyWith(
                    fontSize: 20,
                    fontWeight: semibold,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
