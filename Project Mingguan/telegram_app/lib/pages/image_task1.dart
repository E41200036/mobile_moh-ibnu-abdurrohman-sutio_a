import 'package:flutter/material.dart';
import 'package:telegram_app/theme.dart';

class ImageTask1 extends StatelessWidget {
  const ImageTask1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kPurple,
        title: Text(
          'Soup Makanan Enak Sekali',
          style: whiteTextStyle.copyWith(
            fontWeight: semibold,
          ),
        ),
      ),
      body: ListView(
        children: [
          Container(
            width: double.infinity,
            height: 325,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(
                  'assets/image_food.png',
                ),
                fit: BoxFit.cover,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
