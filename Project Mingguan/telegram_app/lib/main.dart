import 'package:flutter/material.dart';
import 'package:telegram_app/pages/get_started_page.dart';
import 'package:telegram_app/pages/image_task1.dart';
import 'package:telegram_app/pages/main_page.dart';

void main(List<String> args) {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      // NOTE: TUGAS 1
      // home: ImageTask1(),
      // NOTE: TUGAS 2
      routes: {
        '/': (context) => GetStartedPage(),
        '/main': (context) => MainPage(),
      },
    );
  }
}
