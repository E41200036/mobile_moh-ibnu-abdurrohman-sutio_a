import 'package:flutter/material.dart';

import '../theme.dart';

class SidebarMenuItem extends StatelessWidget {
  final String title;
  final IconData icon;
  final Color color;
  const SidebarMenuItem(
      {Key? key, required this.title, required this.icon, required this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 16),
      child: Row(
        children: [
          Container(
            width: 40,
            height: 40,
            margin: EdgeInsets.only(right: 16),
            padding: EdgeInsets.all(8),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(14),
              color: color,
            ),
            child: Icon(
              icon,
              color: kWhite,
            ),
          ),
          Text(
            title,
            style: whiteTextStyle.copyWith(fontSize: 14, fontWeight: medium),
          ),
        ],
      ),
    );
  }
}
