import 'package:flutter/material.dart';

import '../theme.dart';

class StatusItem extends StatelessWidget {
  final String img;
  const StatusItem({Key? key, required this.img}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 50,
      height: 58,
      margin: EdgeInsets.only(right: 20),
      child: Stack(
        children: [
          Container(
            width: 50,
            height: 50,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                image: AssetImage(
                  img,
                ),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: Container(
              width: 15,
              height: 15,
              margin: EdgeInsets.only(bottom: 5),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: kGreen,
                border: Border.all(color: kSoftBlack, width: 2),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
