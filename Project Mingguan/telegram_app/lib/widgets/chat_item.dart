import 'package:flutter/material.dart';
import 'package:telegram_app/pages/image_page.dart';

import '../theme.dart';

class ChatItem extends StatelessWidget {
  final String img;
  final String name;
  final String message;
  final String time;
  const ChatItem(
      {Key? key,
      required this.img,
      required this.name,
      required this.message,
      required this.time})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ImagePage(img: img, name: name)));
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 16,
          ),
          Row(
            children: [
              Container(
                width: 50,
                height: 50,
                margin: EdgeInsets.only(right: 15),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image: AssetImage(
                      img,
                    ),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      name,
                      style: whiteTextStyle.copyWith(
                        fontWeight: semibold,
                      ),
                    ),
                    Text(
                      message,
                      style: greyTextStyle.copyWith(
                        fontSize: 12,
                        fontWeight: medium,
                      ),
                    ),
                  ],
                ),
              ),
              Text(
                time,
                style: greyTextStyle.copyWith(fontSize: 10),
              ),
            ],
          ),
          SizedBox(
            height: 16,
          ),
          Container(
            width: double.infinity,
            height: 1,
            decoration: BoxDecoration(
              color: kSoftBlack,
            ),
          ),
        ],
      ),
    );
  }
}
