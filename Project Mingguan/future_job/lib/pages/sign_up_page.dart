import 'package:cool_alert/cool_alert.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:future_job/models/user_model.dart';
import 'package:future_job/providers/auth_provider.dart';
import 'package:future_job/providers/user_provider.dart';
import 'package:provider/provider.dart';

import '../theme.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  TextEditingController _fullnameController = TextEditingController(text: '');
  TextEditingController _emailAddressController =
      TextEditingController(text: '');
  TextEditingController _passwordController = TextEditingController(text: '');
  TextEditingController _goalController = TextEditingController(text: '');

  @override
  Widget build(BuildContext context) {
    var userProvider = Provider.of<UserProvider>(context);
    var authProvider = Provider.of<AuthProvider>(context);

    void resetField() {
      _fullnameController.clear();
      _emailAddressController.clear();
      _passwordController.clear();
      _goalController.clear();
    }

    Widget backLink() {
      return Align(
        alignment: Alignment.center,
        child: Container(
          margin: EdgeInsets.only(top: 20),
          child: GestureDetector(
            onTap: () {
              Navigator.pushNamed(context, '/sign-in');
              resetField();
            },
            child: Text(
              'Back to Sign In',
              style: greyTextStyle.copyWith(
                fontWeight: FontWeight.w300,
              ),
            ),
          ),
        ),
      );
    }

    Widget buttonSignUp() {
      return Container(
        margin: EdgeInsets.only(top: 40),
        width: double.infinity,
        height: 45,
        decoration: BoxDecoration(
          color: kPurple,
          borderRadius: BorderRadius.circular(99),
        ),
        child: TextButton(
          onPressed: () async {
            String email, password, fullname, goal;
            email = _emailAddressController.text;
            password = _passwordController.text;
            fullname = _fullnameController.text;
            goal = _goalController.text;
            if (email.length > 0 &&
                password.length > 0 &&
                fullname.length > 0 &&
                goal.length > 0) {
              UserModel? userModel = await authProvider.register(
                  _emailAddressController.text,
                  _passwordController.text,
                  _fullnameController.text,
                  _goalController.text);
              if (userModel != null) {
                userProvider.user = userModel;
                Navigator.pushNamed(context, '/home');
                resetField();
              } else {
                print('email telah terdaftar');
              }
            } else {
              CoolAlert.show(
                context: context,
                type: CoolAlertType.error,
                title: 'Error',
                text: 'Please fill all field',
              );
            }
          },
          child: Text(
            'Sign Up',
            style: whiteTextStyle.copyWith(
              fontWeight: medium,
            ),
          ),
        ),
      );
    }

    Widget inputGoal() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 20),
          Text(
            'Your Goal',
            style: greyTextStyle.copyWith(fontSize: 16),
          ),
          SizedBox(
            height: 8,
          ),
          TextFormField(
            controller: _goalController,
            onChanged: (val) => setState(() {}),
            cursorColor: kPurple,
            style: purpleTextStyle.copyWith(fontSize: 16),
            decoration: InputDecoration(
              contentPadding: EdgeInsets.only(left: 20, bottom: 12, top: 9),
              fillColor: kSoftGrey,
              filled: true,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(100),
                borderSide: BorderSide.none,
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(100),
                borderSide: BorderSide(
                    color: _goalController.text.length > 0 ? kPurple : kRed,
                    width: 1),
              ),
            ),
          ),
        ],
      );
    }

    Widget inputPassword() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 20,
          ),
          Text(
            'Password',
            style: greyTextStyle.copyWith(fontSize: 16),
          ),
          SizedBox(
            height: 8,
          ),
          TextFormField(
            obscureText: true,
            controller: _passwordController,
            onChanged: (value) {
              setState(() {});
            },
            cursorColor: kPurple,
            style: purpleTextStyle.copyWith(fontSize: 16),
            decoration: InputDecoration(
              contentPadding: EdgeInsets.only(left: 20, bottom: 12, top: 9),
              fillColor: kSoftGrey,
              filled: true,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(100),
                borderSide: BorderSide.none,
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(100),
                borderSide: BorderSide(
                    color: _passwordController.text.length > 0 ? kPurple : kRed,
                    width: 1),
              ),
            ),
          ),
        ],
      );
    }

    Widget inputEmail() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Email address',
            style: greyTextStyle.copyWith(fontSize: 16),
          ),
          SizedBox(
            height: 8,
          ),
          TextFormField(
            controller: _emailAddressController,
            onChanged: (value) {
              setState(() {});
            },
            cursorColor: kPurple,
            style: purpleTextStyle.copyWith(fontSize: 16),
            decoration: InputDecoration(
              contentPadding: EdgeInsets.only(left: 20, bottom: 12, top: 9),
              fillColor: kSoftGrey,
              filled: true,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(100),
                borderSide: BorderSide.none,
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(100),
                borderSide: BorderSide(
                    color: EmailValidator.validate(_emailAddressController.text)
                        ? kPurple
                        : Colors.red,
                    width: 1),
              ),
            ),
          ),
        ],
      );
    }

    Widget inputFullname() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Full Name',
            style: greyTextStyle.copyWith(fontSize: 16),
          ),
          SizedBox(
            height: 8,
          ),
          TextFormField(
            controller: _fullnameController,
            onChanged: (value) {
              setState(() {});
            },
            style: purpleTextStyle.copyWith(fontSize: 16),
            cursorColor: kPurple,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.only(left: 20, top: 9, bottom: 12),
              hintText: '',
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(100),
                borderSide: BorderSide.none,
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(100),
                borderSide: BorderSide(
                    color: _fullnameController.text.length > 0 ? kPurple : kRed,
                    width: 1),
              ),
              filled: true,
              fillColor: kSoftGrey,
            ),
          ),
          SizedBox(
            height: 20,
          ),
        ],
      );
    }

    Widget inputImage() {
      return Column(
        children: [
          Align(
            alignment: Alignment.center,
            child: Container(
              width: 120,
              height: 120,
              padding: EdgeInsets.all(9),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(color: kPurple, width: 1),
              ),
              child: Image.asset(
                'assets/icon_upload.png',
                fit: BoxFit.cover,
              ),
            ),
          ),
          SizedBox(
            height: 50,
          ),
        ],
      );
    }

    Widget header() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 6,
          ),
          Text(
            'Sign Up',
            style: greyTextStyle.copyWith(fontSize: 16),
          ),
          SizedBox(
            height: 2,
          ),
          Text(
            'Begin New Journey',
            style: blackTextStyle.copyWith(fontSize: 24, fontWeight: semibold),
          ),
          SizedBox(
            height: 50,
          )
        ],
      );
    }

    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(default_margin),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              header(),
              inputImage(),
              inputFullname(),
              inputEmail(),
              inputPassword(),
              inputGoal(),
              buttonSignUp(),
              backLink(),
            ],
          ),
        ),
      ),
    );
  }

  void showError(String message) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(message),
      backgroundColor: Colors.red,
    ));
  }
}
