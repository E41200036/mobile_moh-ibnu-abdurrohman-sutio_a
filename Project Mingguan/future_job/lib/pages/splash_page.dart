import 'dart:async';

import 'package:flutter/material.dart';
import 'package:future_job/theme.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Timer(
        Duration(seconds: 3),
        () => Navigator.pushNamedAndRemoveUntil(
            context, '/get-started', (_) => false));
    return Scaffold(
      backgroundColor: kPurple,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/image_splash.png',
              height: 174,
              width: 182,
            ),
          ],
        ),
      ),
    );
  }
}
