import 'package:flutter/material.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:future_job/models/category_model.dart';
import 'package:future_job/models/job_model.dart';
import 'package:future_job/providers/category_provider.dart';
import 'package:future_job/providers/job_provider.dart';
import 'package:future_job/theme.dart';
import 'package:future_job/widgets/hot_category_item.dart';
import 'package:future_job/widgets/job_title.dart';
import 'package:provider/provider.dart';

import '../providers/user_provider.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var userProvider = Provider.of<UserProvider>(context);
    var categoryProvider = Provider.of<CategoryProvider>(context);
    var jobProvider = Provider.of<JobProvider>(context);

    Widget hotCategories() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Hot Categories',
            style: blackTextStyle.copyWith(fontSize: 16),
          ),
          SizedBox(height: 16),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: FutureBuilder<List<CategoryModel>>(
              future: categoryProvider.getCategories(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return Row(
                    children: snapshot.data!
                        .map((category) => Padding(
                              padding: const EdgeInsets.only(right: 16),
                              child: HotCateogryItem(category),
                            ))
                        .toList(),
                  );
                } else {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
              },
            ),
          ),
          SizedBox(
            height: 30,
          ),
        ],
      );
    }

    Widget justPosted() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Just Posted',
            style: blackTextStyle.copyWith(fontSize: 16),
          ),
          SizedBox(height: 26),
          FutureBuilder<List<JobModel>>(
            future: jobProvider.getAllJob(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                return Column(
                  children: snapshot.data!.map((job) => JobTitle(job)).toList(),
                );
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            },
          ),
        ],
      );
    }

    Widget header() {
      return Container(
        margin: EdgeInsets.only(top: 6, bottom: 30),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Howdy',
                  style: greyTextStyle.copyWith(fontSize: 16),
                ),
                SizedBox(height: 2),
                Text(
                  userProvider.user.name,
                  style: blackTextStyle.copyWith(
                    fontSize: 24,
                    fontWeight: semibold,
                  ),
                ),
              ],
            ),
            Container(
              width: 58,
              height: 58,
              padding: EdgeInsets.all(4),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(color: kPurple, width: 1),
              ),
              child: Image.asset('assets/image_profile.png'),
            ),
          ],
        ),
      );
    }

    return Scaffold(
      body: SafeArea(
        child: ListView(
          padding: EdgeInsets.all(default_margin),
          children: [
            header(),
            hotCategories(),
            justPosted(),
          ],
        ),
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.symmetric(vertical: 16),
        child: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            showSelectedLabels: false,
            showUnselectedLabels: false,
            elevation: 0,
            selectedIconTheme: IconThemeData(color: kBlack),
            unselectedIconTheme: IconThemeData(
              color: kGrey,
            ),
            currentIndex: 0,
            items: [
              BottomNavigationBarItem(
                  icon: Icon(
                    IconlyLight.home,
                  ),
                  label: 'Home'),
              BottomNavigationBarItem(
                  icon: Icon(
                    IconlyLight.notification,
                  ),
                  label: 'Notification'),
              BottomNavigationBarItem(
                  icon: Icon(
                    IconlyLight.heart,
                  ),
                  label: 'Favorite'),
              BottomNavigationBarItem(
                  icon: Icon(IconlyLight.user2), label: 'Account'),
            ]),
      ),
    );
  }
}
