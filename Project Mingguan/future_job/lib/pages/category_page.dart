import 'package:flutter/material.dart';
import 'package:future_job/models/category_model.dart';
import 'package:future_job/models/job_model.dart';
import 'package:future_job/providers/job_provider.dart';
import 'package:future_job/theme.dart';
import 'package:future_job/widgets/job_title.dart';
import 'package:provider/provider.dart';

class CategoryPage extends StatelessWidget {
  final CategoryModel category;
  CategoryPage(this.category);

  @override
  Widget build(BuildContext context) {
    var jobProvider = Provider.of<JobProvider>(context);

    Widget newStartups() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 4),
          Text(
            'New Startups',
            style: blackTextStyle.copyWith(fontSize: 16),
          ),
          SizedBox(height: 26),
          FutureBuilder<List<JobModel>>(
            future: jobProvider.getAllJob(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: snapshot.data!.map((job) => JobTitle(job)).toList(),
                );
              } else {
                return CircularProgressIndicator();
              }
            },
          ),
          // JobTitle(
          //   postedImage: 'assets/icon_google.png',
          //   position: 'Front-End Developer',
          //   company: 'Google',
          // ),
          // JobTitle(
          //   postedImage: 'assets/icon_instagram.png',
          //   position: 'UI Designer',
          //   company: 'Instagram',
          // ),
          // JobTitle(
          //   postedImage: 'assets/icon_facebook.png',
          //   position: 'Data Scientist',
          //   company: 'Facebook',
          // ),
        ],
      );
    }

    Widget bigCompanies() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 30),
          Text(
            'Big Companies',
            style: blackTextStyle.copyWith(fontSize: 16),
          ),
          SizedBox(height: 26),
          FutureBuilder<List<JobModel>>(
            future: jobProvider.getJobByCategory(category.name),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: snapshot.data!.map((job) => JobTitle(job)).toList(),
                );
              } else {
                return CircularProgressIndicator();
              }
            },
          ),
        ],
      );
    }

    Widget header() {
      return Container(
        width: double.infinity,
        height: 270,
        padding: EdgeInsets.all(default_margin),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.vertical(bottom: Radius.circular(16)),
          image: DecorationImage(
            image: NetworkImage(category.imageUrl),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              category.name,
              style:
                  whiteTextStyle.copyWith(fontSize: 24, fontWeight: semibold),
            ),
            Text(
              '12,309 available',
              style: whiteTextStyle.copyWith(fontSize: 16),
            ),
            SizedBox(height: 6),
          ],
        ),
      );
    }

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            header(),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: default_margin),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  bigCompanies(),
                  newStartups(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
