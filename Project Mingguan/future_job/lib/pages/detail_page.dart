import 'package:flutter/material.dart';
import 'package:future_job/models/job_model.dart';
import 'package:future_job/theme.dart';

class DetailPage extends StatefulWidget {
  final JobModel job;
  DetailPage(this.job);

  @override
  State<DetailPage> createState() => _DetailPageState();
}

bool isApplied = false;

class _DetailPageState extends State<DetailPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(default_margin),
          child: Column(
            children: [
              isApplied ? appliedAlert() : Container(),
              header(),
              aboutJob(),
              qualification(),
              responsibilities(),
              isApplied ? cancelButton() : applyButton(),
              messageRecruiter(),
            ],
          ),
        ),
      ),
    );
  }

  Widget appliedAlert() {
    return Container(
      margin: EdgeInsets.only(top: 30),
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 9),
      width: 312,
      decoration: BoxDecoration(
        color: kSoftGrey,
        borderRadius: BorderRadius.circular(49),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.025),
            blurRadius: 20,
            offset: Offset(0, 10),
          ),
        ],
      ),
      child: Center(
        child: Text(
            'You have applied this job and the recruiter will contact you',
            textAlign: TextAlign.center,
            style: greyTextStyle.copyWith(color: Color(0xffA2A6B4))),
      ),
    );
  }

  Widget messageRecruiter() {
    return Align(
      alignment: Alignment.center,
      child: Text(
        'Message Recruiter',
        style: greyTextStyle.copyWith(fontWeight: FontWeight.w300),
      ),
    );
  }

  Widget cancelButton() {
    return Container(
      width: 200,
      height: 45,
      margin: EdgeInsets.only(top: 50, bottom: 20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(66),
        color: kRed,
      ),
      child: TextButton(
        onPressed: () {
          setState(() {
            isApplied = false;
          });
        },
        child: Text(
          'Cancel Apply',
          style: whiteTextStyle.copyWith(fontWeight: medium),
        ),
      ),
    );
  }

  Widget applyButton() {
    return Container(
      width: 200,
      height: 45,
      margin: EdgeInsets.only(top: 50, bottom: 20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(66),
        color: kPurple,
      ),
      child: TextButton(
        onPressed: () {
          setState(() {
            isApplied = true;
          });
        },
        child: Text(
          'Apply for Job',
          style: whiteTextStyle.copyWith(fontWeight: medium),
        ),
      ),
    );
  }

  Widget responsibilities() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 16,
        ),
        Text(
          'Responsibilities',
          style: blackTextStyle.copyWith(
            fontWeight: medium,
          ),
        ),
        SizedBox(
          height: 16,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: widget.job.responsibilities
              .map((e) => detail(e.toString()))
              .toList(),
        ),
      ],
    );
  }

  Widget qualification() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 16,
        ),
        Text(
          'Qualification',
          style: blackTextStyle.copyWith(
            fontWeight: medium,
          ),
        ),
        SizedBox(
          height: 16,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: widget.job.qualifications
              .map((e) => detail(e.toString()))
              .toList(),
        ),
      ],
    );
  }

  // detail
  Widget detail(String text) {
    return Container(
      margin: EdgeInsets.only(bottom: 16),
      child: Row(
        children: [
          Icon(
            Icons.adjust,
            color: kPurple,
            size: 18,
          ),
          SizedBox(
            width: 8,
          ),
          Expanded(
            child: Text(
              text,
              style: blackTextStyle.copyWith(fontWeight: FontWeight.w300),
            ),
          ),
        ],
      ),
    );
  }

  Widget aboutJob() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'About the job',
          style: blackTextStyle.copyWith(
            fontWeight: medium,
          ),
        ),
        SizedBox(
          height: 16,
        ),
        // fetch about job
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: widget.job.about.map((e) => detail(e.toString())).toList(),
        ),
      ],
    );
  }

  Widget header() {
    return Column(
      children: [
        SizedBox(
          height: 30,
        ),
        Image.network(
          widget.job.companyLogo,
          width: 58,
          fit: BoxFit.cover,
        ),
        SizedBox(
          height: 20,
        ),
        Text(
          widget.job.category,
          style: blackTextStyle.copyWith(
            fontSize: 20,
            fontWeight: semibold,
          ),
        ),
        SizedBox(
          height: 2,
        ),
        Text(
          widget.job.companyName + " · " + widget.job.location,
          style: greyTextStyle,
        ),
        SizedBox(
          height: 30,
        ),
      ],
    );
  }
}
