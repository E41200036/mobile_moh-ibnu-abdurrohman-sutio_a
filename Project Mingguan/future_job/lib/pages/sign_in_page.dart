import 'package:cool_alert/cool_alert.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:future_job/models/user_model.dart';
import 'package:future_job/theme.dart';
import 'package:provider/provider.dart';

import '../providers/auth_provider.dart';
import '../providers/user_provider.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({Key? key}) : super(key: key);

  @override
  State<SignInPage> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  TextEditingController _emailController = TextEditingController(text: '');
  TextEditingController _passwordController = TextEditingController(text: '');

  @override
  Widget build(BuildContext context) {
    var userProvider = Provider.of<UserProvider>(context);
    var authProvider = Provider.of<AuthProvider>(context);

    // reset all field
    void resetField() {
      _emailController.clear();
      _passwordController.clear();
    }

    Widget linkCreateNewAccount() {
      return Align(
        alignment: Alignment.center,
        child: GestureDetector(
          onTap: () => Navigator.pushNamed(context, '/sign-up'),
          child: Text(
            'Create New Account',
            style: greyTextStyle.copyWith(fontWeight: FontWeight.w300),
          ),
        ),
      );
    }

    Widget buttonSignIn() {
      return Container(
        width: double.infinity,
        margin: EdgeInsets.only(bottom: 20),
        height: 45,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(66),
          color: kPurple,
        ),
        child: TextButton(
            onPressed: () async {
              // cek username & password
              String email = _emailController.text;
              String password = _passwordController.text;

              if (EmailValidator.validate(email) && password.length > 0) {
                UserModel? user = await authProvider.login(
                    _emailController.text, _passwordController.text);
                if (user != null) {
                  userProvider.user = user;
                  Navigator.pushNamedAndRemoveUntil(
                      context, '/home', (route) => false);
                  resetField();
                } else {
                  CoolAlert.show(
                    context: context,
                    type: CoolAlertType.error,
                    title: 'Error',
                  );
                }
              }
            },
            child: Text(
              'Sign In',
              style: whiteTextStyle.copyWith(fontWeight: medium),
            )),
      );
    }

    Widget inputPassword() {
      return Container(
        margin: EdgeInsets.only(bottom: 40),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Password',
              style: greyTextStyle.copyWith(fontSize: 16),
            ),
            SizedBox(
              height: 8,
            ),
            TextFormField(
              controller: _passwordController,
              obscureText: true,
              onChanged: (val) => setState(() {}),
              cursorColor: kPurple,
              style: purpleTextStyle.copyWith(fontSize: 16),
              decoration: InputDecoration(
                contentPadding: EdgeInsets.symmetric(horizontal: 20),
                hintText: '',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(100),
                  borderSide: BorderSide.none,
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(100),
                  borderSide: BorderSide(
                      color:
                          _passwordController.text.length > 0 ? kPurple : kRed,
                      width: 1),
                ),
                filled: true,
                fillColor: kSoftGrey,
              ),
            ),
          ],
        ),
      );
    }

    Widget inputEmail() {
      return Container(
        margin: EdgeInsets.only(bottom: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Email Address',
              style: greyTextStyle.copyWith(fontSize: 16),
            ),
            SizedBox(
              height: 8,
            ),
            TextFormField(
              controller: _emailController,
              onChanged: (val) => setState(() {}),
              cursorColor: kPurple,
              style: purpleTextStyle.copyWith(fontSize: 16),
              decoration: InputDecoration(
                contentPadding: EdgeInsets.symmetric(horizontal: 20),
                hintText: '',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(100),
                  borderSide: BorderSide.none,
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(100),
                  borderSide: BorderSide(
                      color: EmailValidator.validate(_emailController.text)
                          ? kPurple
                          : kRed,
                      width: 1),
                ),
                filled: true,
                fillColor: kSoftGrey,
              ),
            ),
          ],
        ),
      );
    }

    Widget heroImage() {
      return Container(
        margin: EdgeInsets.only(bottom: 40),
        child: Align(
          alignment: Alignment.center,
          child: Image.asset(
            'assets/image_sign_in.png',
            width: 261.49,
            height: 240,
          ),
        ),
      );
    }

    Widget header() {
      return Container(
        margin: EdgeInsets.only(top: 6, bottom: 40),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text(
            'Sign In',
            style: greyTextStyle.copyWith(fontSize: 16),
          ),
          SizedBox(height: 2),
          Text(
            'Build Your Career',
            style: blackTextStyle.copyWith(
              fontSize: 24,
              fontWeight: semibold,
            ),
          ),
        ]),
      );
    }

    return Scaffold(
      body: Padding(
          padding: EdgeInsets.all(default_margin),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                header(),
                heroImage(),
                inputEmail(),
                inputPassword(),
                buttonSignIn(),
                linkCreateNewAccount(),
              ],
            ),
          )),
    );
  }
}
