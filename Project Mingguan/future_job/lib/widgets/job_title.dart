import 'package:flutter/material.dart';
import 'package:future_job/models/job_model.dart';
import 'package:future_job/pages/detail_page.dart';
import 'package:future_job/theme.dart';

class JobTitle extends StatelessWidget {
  final JobModel job;
  JobTitle(this.job);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => DetailPage(job),
          ),
        );
      },
      child: Container(
        width: double.maxFinite,
        margin: EdgeInsets.only(bottom: 26),
        child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Image.network(
            job.companyLogo,
            width: 45,
            height: 45,
            fit: BoxFit.cover,
          ),
          SizedBox(
            width: 27,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                job.category,
                style:
                    blackTextStyle.copyWith(fontSize: 16, fontWeight: medium),
              ),
              SizedBox(
                height: 2,
              ),
              Text(job.companyName, style: greyTextStyle),
              SizedBox(
                height: 18,
              ),
              // border line
              Container(
                width: MediaQuery.of(context).size.width - 120,
                height: 1,
                color: Color(0xFFE4E4E4),
              ),
            ],
          ),
        ]),
      ),
    );
  }
}
