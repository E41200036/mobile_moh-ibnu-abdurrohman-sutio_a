import 'package:flutter/material.dart';
import 'package:future_job/models/category_model.dart';
import 'package:future_job/pages/category_page.dart';
import 'package:future_job/theme.dart';

class HotCateogryItem extends StatelessWidget {
  final CategoryModel category;

  HotCateogryItem(this.category);

  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => CategoryPage(category),
        ),
      ),
      child: Container(
        width: 150,
        height: 200,
        padding: EdgeInsets.all(16),
        margin: EdgeInsets.only(right: 16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          image: DecorationImage(
            image: NetworkImage(category.imageUrl),
            fit: BoxFit.cover,
          ),
        ),
        child: Align(
          alignment: Alignment.bottomLeft,
          child: Text(
            category.name.toString(),
            style: whiteTextStyle.copyWith(fontSize: 18, fontWeight: medium),
          ),
        ),
      ),
    );
  }
}
