import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:future_job/models/category_model.dart';
import 'package:http/http.dart' as http;

class CategoryProvider with ChangeNotifier {
  Future<List<CategoryModel>> getCategories() async {
    try {
      var response = await http
          .get(Uri.parse('https://bwa-jobs.herokuapp.com/categories'));
      print(response.statusCode);
      print(response.body);
      if (response.statusCode == 200) {
        List<CategoryModel> categories = [];
        var data = json.decode(response.body);
        for (var item in data) {
          categories.add(CategoryModel.fromJson(item));
        }
        return categories;
      } else {
        return [];
      }
    } catch (e) {
      print(e);
      return [];
    }
  }
}
