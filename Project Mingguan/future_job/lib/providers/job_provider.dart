import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:future_job/models/job_model.dart';

import 'package:http/http.dart' as http;

class JobProvider with ChangeNotifier {
  Future<List<JobModel>> getAllJob() async {
    try {
      var response =
          await http.get(Uri.parse('https://bwa-jobs.herokuapp.com/jobs'));

      print(response.statusCode);
      print(response.body);

      // check if data was received
      if (response.statusCode == 200) {
        List<JobModel> jobList = [];
        List data = json.decode(response.body);
        for (var item in data) {
          jobList.add(JobModel.fromJson(item));
        }
        return jobList;
      } else {
        return [];
      }
    } catch (e) {
      print(e);
      return [];
    }
  }

  // get job by category
  Future<List<JobModel>> getJobByCategory(String category) async {
    try {
      var response = await http.get(
          Uri.parse('https://bwa-jobs.herokuapp.com/jobs?category=$category'));
      print(response.statusCode);
      print(response.body);
      if (response.statusCode == 200) {
        List<JobModel> jobList = [];
        List data = json.decode(response.body);
        for (var item in data) {
          jobList.add(JobModel.fromJson(item));
        }
        return jobList;
      } else {
        return [];
      }
    } catch (e) {
      print(e);
      return [];
    }
  }
}
