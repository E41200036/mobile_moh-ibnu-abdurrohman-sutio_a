import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color kWhite = Colors.white;
Color kPurple = Color(0xff4141A4);
Color kGrey = Color(0xffB3B5C4);
Color kSoftGrey = Color(0xffF1F0F5);
Color kRed = Color(0xffFD4F56);
Color kBlack = Color(0xff272C2F);

double default_margin = 24.0;

TextStyle whiteTextStyle = GoogleFonts.poppins(color: kWhite);
TextStyle blackTextStyle = GoogleFonts.poppins(color: kBlack);
TextStyle purpleTextStyle = GoogleFonts.poppins(color: kPurple);
TextStyle greyTextStyle = GoogleFonts.poppins(color: kGrey);
TextStyle softGreyTextStyle = GoogleFonts.poppins(color: kSoftGrey);

FontWeight normal = FontWeight.w400;
FontWeight medium = FontWeight.w500;
FontWeight semibold = FontWeight.w600;
