import 'package:flutter/material.dart';
import 'package:future_job/pages/get_started_page.dart';
import 'package:future_job/pages/home_page.dart';
import 'package:future_job/pages/splash_page.dart';
import 'package:future_job/pages/sign_in_page.dart';
import 'package:future_job/pages/sign_up_page.dart';
import 'package:future_job/providers/auth_provider.dart';
import 'package:future_job/providers/category_provider.dart';
import 'package:future_job/providers/job_provider.dart';
import 'package:provider/provider.dart';

import 'providers/user_provider.dart';

void main(List<String> args) => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => AuthProvider()),
        ChangeNotifierProvider(create: (_) => UserProvider()),
        ChangeNotifierProvider(create: (_) => CategoryProvider()),
        ChangeNotifierProvider(create: (_) => JobProvider()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        routes: {
          '/': (context) => SplashPage(),
          '/get-started': (context) => GetStartedPage(),
          '/sign-in': (context) => SignInPage(),
          '/sign-up': (context) => SignUpPage(),
          '/home': (context) => HomePage(),
        },
      ),
    );
  }
}
