import 'package:finansa/theme.dart';
import 'package:finansa/widgets/menu_item.dart';
import 'package:flutter/material.dart';

class DashboardPage extends StatelessWidget {
  const DashboardPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffFAFAFA),
      body: SafeArea(
        child: ListView(
          padding: EdgeInsets.all(24),
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Dashboard',
                  style: blackTextStyle.copyWith(
                    fontSize: 18,
                    fontWeight: medium,
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    showModalBottomSheet(
                        backgroundColor: kWhite,
                        enableDrag: true,
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.vertical(
                          top: Radius.circular(40),
                        )),
                        context: context,
                        builder: (context) => Container(
                              height: 490,
                              width: double.infinity,
                              child: Padding(
                                padding: const EdgeInsets.all(30),
                                child: SingleChildScrollView(
                                  clipBehavior: Clip.none,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      SizedBox(
                                        height: 50,
                                      ),
                                      Align(
                                        alignment: Alignment.center,
                                        child: Column(children: [
                                          Image.asset(
                                            'assets/icon_google.png',
                                            width: 60,
                                            height: 60,
                                          ),
                                          SizedBox(
                                            height: 17,
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Text(
                                                'Google Drive',
                                                textAlign: TextAlign.center,
                                                style: blackTextStyle.copyWith(
                                                    fontSize: 16,
                                                    fontWeight: medium),
                                              ),
                                              SizedBox(
                                                width: 5,
                                              ),
                                              Image.asset(
                                                'assets/icon_check.png',
                                                width: 16,
                                                height: 16,
                                              ),
                                              SizedBox(
                                                height: 6,
                                              ),
                                            ],
                                          ),
                                          Text(
                                            '12.30 PM',
                                            style: darkGreyTextStyle,
                                          ),
                                          SizedBox(
                                            height: 30,
                                          ),
                                          Text(
                                            '- \$20,500',
                                            style: blackTextStyle.copyWith(
                                              fontSize: 28,
                                              fontWeight: semibold,
                                            ),
                                          ),
                                          SizedBox(
                                            height: 30,
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(bottom: 16),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Text('CloudOne 10GB',
                                                    style:
                                                        blackTextStyle.copyWith(
                                                            fontSize: 16)),
                                                Text('\$15,000',
                                                    style:
                                                        blackTextStyle.copyWith(
                                                            fontSize: 16)),
                                              ],
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(bottom: 16),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Text('Workspace',
                                                    style:
                                                        blackTextStyle.copyWith(
                                                            fontSize: 16)),
                                                Text('\$500',
                                                    style:
                                                        blackTextStyle.copyWith(
                                                            fontSize: 16)),
                                              ],
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(bottom: 30),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Text('Stream Maxx',
                                                    style:
                                                        blackTextStyle.copyWith(
                                                            fontSize: 16)),
                                                Text('\$5,000',
                                                    style:
                                                        blackTextStyle.copyWith(
                                                            fontSize: 16)),
                                              ],
                                            ),
                                          ),
                                          Container(
                                            width: double.infinity,
                                            height: 45,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(16),
                                              color: kPurple,
                                            ),
                                            child: TextButton(
                                                onPressed: () {},
                                                child: Text(
                                                  'Ok, Close',
                                                  style:
                                                      whiteTextStyle.copyWith(
                                                    fontSize: 16,
                                                    fontWeight: semibold,
                                                  ),
                                                )),
                                          ),
                                        ]),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ));
                  },
                  child: Image.asset(
                    'assets/icon_notif.png',
                    width: 24,
                    height: 24,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 30,
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              clipBehavior: Clip.none,
              child: Row(
                children: [
                  Container(
                    width: 300,
                    height: 190,
                    margin: EdgeInsets.only(right: 20),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      image: DecorationImage(
                        image: AssetImage('assets/image_card1.png'),
                        fit: BoxFit.cover,
                      ),
                    ),
                    padding: EdgeInsets.symmetric(horizontal: 24, vertical: 30),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Current Balance',
                          style: whiteTextStyle.copyWith(fontSize: 13),
                        ),
                        SizedBox(
                          height: 4,
                        ),
                        Text(
                          '\$12,480,209',
                          style: whiteTextStyle.copyWith(
                            fontSize: 28,
                            fontWeight: semibold,
                          ),
                        ),
                        SizedBox(
                          height: 40,
                        ),
                        Row(
                          children: [
                            Text(
                              '••••  ••••  ••••  3910',
                              style: whiteTextStyle.copyWith(
                                fontSize: 16,
                                letterSpacing: 4,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: 300,
                    height: 190,
                    margin: EdgeInsets.only(right: 20),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      image: DecorationImage(
                        image: AssetImage('assets/image_card2.png'),
                        fit: BoxFit.cover,
                      ),
                    ),
                    padding: EdgeInsets.symmetric(horizontal: 24, vertical: 30),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Current Balance',
                          style: whiteTextStyle.copyWith(fontSize: 13),
                        ),
                        SizedBox(
                          height: 4,
                        ),
                        Text(
                          '\$12,480,209',
                          style: whiteTextStyle.copyWith(
                            fontSize: 28,
                            fontWeight: semibold,
                          ),
                        ),
                        SizedBox(
                          height: 40,
                        ),
                        Row(
                          children: [
                            Text(
                              '••••  ••••  ••••  3901',
                              style: whiteTextStyle.copyWith(
                                fontSize: 16,
                                letterSpacing: 4,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Text(
              'Main Menu',
              style: blackTextStyle.copyWith(
                fontSize: 18,
                fontWeight: medium,
              ),
            ),
            SizedBox(
              height: 18,
            ),
            Center(
              child: Wrap(
                crossAxisAlignment: WrapCrossAlignment.center,
                spacing: 29,
                runSpacing: 18,
                children: [
                  MenuItem(imgUrl: 'assets/icon_top.png', name: 'Top Up'),
                  MenuItem(
                      imgUrl: 'assets/icon_transfer.png', name: 'Transfer'),
                  MenuItem(imgUrl: 'assets/icon_bill.png', name: 'Bills'),
                  MenuItem(
                      imgUrl: 'assets/icon_withdraw.png', name: 'Withdraw'),
                  MenuItem(imgUrl: 'assets/icon_reward.png', name: 'Reward'),
                  MenuItem(imgUrl: 'assets/icon_location.png', name: 'Nearby'),
                ],
              ),
            )
          ],
        ),
      ),
      floatingActionButton: Container(
        width: 70,
        clipBehavior: Clip.none,
        height: 70,
        padding: EdgeInsets.all(18),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(width: 6, color: Color(0xffFAFAFA)),
          color: kPurple,
        ),
        child: Image.asset('assets/icon_scan.png'),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: Container(
        width: double.infinity,
        height: 60,
        color: kWhite,
        child: Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
          Image.asset('assets/icon_home.png', width: 24, height: 24),
          Image.asset('assets/icon_calendar.png', width: 24, height: 24),
          SizedBox(
            width: 50,
          ),
          Image.asset('assets/icon_briefcase.png', width: 24, height: 24),
          Image.asset('assets/icon_setting.png', width: 24, height: 24),
        ]),
      ),
    );
  }
}
