import 'package:finansa/theme.dart';
import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 30),
        children: [
          SizedBox(
            height: 100,
          ),
          Image.asset(
            'assets/image_splash.png',
            width: double.infinity,
          ),
          SizedBox(
            height: 60,
          ),
          Text(
            'Effortless.\nIntegrated app.',
            style: blackTextStyle.copyWith(
              fontSize: 28,
              fontWeight: semibold,
            ),
          ),
          SizedBox(
            height: 50,
          ),
          Row(
            children: [
              Expanded(
                  child: Container(
                height: 45,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16),
                  color: kGrey,
                ),
                child: TextButton(
                  onPressed: () {},
                  child: Text(
                    'Sign In',
                    style: darkGreyTextStyle.copyWith(
                      fontSize: 16,
                      fontWeight: medium,
                    ),
                  ),
                ),
              )),
              SizedBox(
                width: 15,
              ),
              Expanded(
                  child: Container(
                height: 45,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16),
                  color: kPurple,
                ),
                child: TextButton(
                  onPressed: () {
                    Navigator.pushNamed(context, '/dahsboard');
                  },
                  child: Text(
                    'Get Started',
                    style: whiteTextStyle.copyWith(
                      fontSize: 16,
                      fontWeight: medium,
                    ),
                  ),
                ),
              )),
            ],
          ),
        ],
      ),
    );
  }
}
