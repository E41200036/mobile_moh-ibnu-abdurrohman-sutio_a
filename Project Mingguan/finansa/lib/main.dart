import 'package:finansa/splash_page.dart';
import 'package:flutter/material.dart';

import 'dashboard_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(debugShowCheckedModeBanner: false, routes: {
      '/': (context) => SplashPage(),
      '/dahsboard': (context) => DashboardPage(),
    });
  }
}
