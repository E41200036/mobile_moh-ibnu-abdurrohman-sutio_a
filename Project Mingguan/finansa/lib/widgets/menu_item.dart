import 'package:finansa/theme.dart';
import 'package:flutter/material.dart';

class MenuItem extends StatelessWidget {
  final String imgUrl, name;
  const MenuItem({Key? key, required this.imgUrl, required this.name})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: (MediaQuery.of(context).size.width - 110) / 3,
      height: 123,
      child: Column(children: [
        Container(
          width: 90,
          height: 90,
          padding: EdgeInsets.all(28),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(24),
            color: kWhite,
          ),
          child: Image.asset(imgUrl),
        ),
        SizedBox(
          height: 12,
        ),
        Text(
          name,
          style: blackTextStyle,
        ),
      ]),
    );
  }
}
