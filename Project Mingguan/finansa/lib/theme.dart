import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color kBlack = Color(0xff111111);
Color kGrey = Color(0xffEDEDF3);
Color kDarkGrey = Color(0xff6F6F94);
Color kPurple = Color(0xff5F59E1);
Color kWhite = Color(0xffFFFFFF);
Color kGreen = Color(0xff1BA37A);

TextStyle blackTextStyle = GoogleFonts.poppins(color: kBlack);
TextStyle greyTextStyle = GoogleFonts.poppins(color: kGrey);
TextStyle darkGreyTextStyle = GoogleFonts.poppins(color: kDarkGrey);
TextStyle whiteTextStyle = GoogleFonts.poppins(color: kWhite);

FontWeight normal = FontWeight.w400;
FontWeight medium = FontWeight.w500;
FontWeight semibold = FontWeight.w600;
FontWeight bold = FontWeight.w700;
