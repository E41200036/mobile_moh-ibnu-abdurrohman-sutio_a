import 'package:eater/pages/see_all_page.dart';
import 'package:flutter/material.dart';

import '../themes.dart';
import '../widgets/category_item.dart';
import '../widgets/food_item.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget header() {
      return Container(
        margin: EdgeInsets.only(bottom: 28),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Eater',
              style: whiteTextStyle.copyWith(
                  fontSize: 24, fontWeight: FontWeight.w800),
            ),
            Text(
              'Eat together and happy',
              style: greyTextStyle.copyWith(
                fontWeight: semibold,
              ),
            ),
          ],
        ),
      );
    }

    Widget searchInput() {
      return Container(
        width: double.infinity,
        height: 43,
        margin: EdgeInsets.only(bottom: 20),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: kSoftBlack,
        ),
        padding: EdgeInsets.symmetric(horizontal: 30),
        child: Expanded(
            child: Center(
          child: TextFormField(
            style: whiteTextStyle,
            decoration: InputDecoration.collapsed(
                hintText: 'Search', hintStyle: greyTextStyle),
          ),
        )),
      );
    }

    Widget categories() {
      return Container(
        margin: EdgeInsets.only(bottom: 20),
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(children: [
            CategoryItem(
              img: 'assets/image_category1.png',
              name: 'Food',
            ),
            CategoryItem(
              img: 'assets/image_category2.png',
              name: 'Drink',
            ),
            CategoryItem(
              img: 'assets/image_category3.png',
              name: 'Snack',
            ),
            CategoryItem(
              img: 'assets/image_category4.png',
              name: 'Vegetable',
            ),
            CategoryItem(
              img: 'assets/image_category5.png',
              name: 'Cake',
            ),
          ]),
        ),
      );
    }

    Widget popularFood() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Most Popular',
                style: whiteTextStyle.copyWith(
                  fontWeight: semibold,
                  fontSize: 18,
                ),
              ),
              GestureDetector(
                onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => SeeAllPage(),
                  ),
                ),
                child: Text(
                  'See all',
                  style: blueTextStyle.copyWith(
                    fontSize: 14,
                    fontWeight: semibold,
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 30,
          ),
          FoodItem(
            name: 'Massaman Curry',
            img: 'assets/image_food1.png',
            store: 'Kantin Bu Niban',
            price: 24000,
          ),
          FoodItem(
            name: 'Kaliagri Soup',
            img: 'assets/image_food2.png',
            store: 'Kantin Bu Sari',
            price: 10000,
          ),
          FoodItem(
            name: 'Rannarock',
            img: 'assets/image_food3.png',
            store: 'Kantin Pak Ibnu',
            price: 20000,
          ),
        ],
      );
    }

    return ListView(
      padding: EdgeInsets.only(left: 30, right: 30, top: 60),
      children: [
        header(),
        searchInput(),
        categories(),
        popularFood(),
      ],
    );
  }
}
