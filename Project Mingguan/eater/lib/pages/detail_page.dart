import 'package:eater/themes.dart';
import 'package:eater/widgets/photo_item.dart';
import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';

class DetailPage extends StatelessWidget {
  const DetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBlack,
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: 396,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/image_detail.png'),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Container(
            width: double.infinity,
            margin: EdgeInsets.only(top: 332),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(40),
              color: kBlack,
            ),
            padding: EdgeInsets.only(left: 30, right: 30, top: 18),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () => Navigator.pop(context),
                      child: Icon(
                        IconlyLight.arrow_left,
                        color: kWhite,
                      ),
                    ),
                    Text(
                      'Detail',
                      style: whiteTextStyle.copyWith(
                          fontSize: 18, fontWeight: semibold),
                    ),
                    Icon(
                      IconlyLight.bookmark,
                      color: kWhite,
                    ),
                  ],
                ),
                SizedBox(height: 30),
                Text(
                  'Kaliagri Soup',
                  style: whiteTextStyle.copyWith(
                      fontSize: 24, fontWeight: semibold),
                ),
                SizedBox(
                  height: 5,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Kantin Bu Salim',
                      style: greyTextStyle.copyWith(
                        fontSize: 12,
                        fontWeight: semibold,
                      ),
                    ),
                    Text(
                      'Rp. 15.000',
                      style: whiteTextStyle.copyWith(
                        fontSize: 18,
                        fontWeight: semibold,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 30,
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      PhotoItem(img: 'assets/image_photos1.png'),
                      PhotoItem(img: 'assets/image_photos2.png'),
                      PhotoItem(img: 'assets/image_photos3.png'),
                      PhotoItem(img: 'assets/image_photos4.png'),
                      PhotoItem(img: 'assets/image_photos5.png'),
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Text(
                  'Description',
                  style: whiteTextStyle.copyWith(
                    fontSize: 14,
                    fontWeight: semibold,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                    'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable',
                    textAlign: TextAlign.justify,
                    style: whiteTextStyle.copyWith(fontSize: 14, height: 1.5)),
              ],
            ),
          ),
        ],
      ),
      bottomNavigationBar: Container(
        width: double.infinity,
        height: 50,
        margin: EdgeInsets.only(left: 30, right: 30, bottom: 20),
        decoration: BoxDecoration(
            color: kBlue, borderRadius: BorderRadius.circular(20)),
        child: Center(
          child: Text(
            'Order Now',
            style: whiteTextStyle.copyWith(
              fontSize: 18,
              fontWeight: semibold,
            ),
          ),
        ),
      ),
    );
  }
}
