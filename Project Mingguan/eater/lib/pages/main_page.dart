import 'package:eater/cubit/navigation_cubit.dart';
import 'package:eater/pages/account_page.dart';
import 'package:eater/pages/favorite_page.dart';
import 'package:eater/pages/home_page.dart';
import 'package:eater/widgets/custom_bottom_navigation_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:iconly/iconly.dart';

import '../themes.dart';

class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget _buildContent(int index) {
      switch (index) {
        case 0:
          return HomePage();
        case 1:
          return FavoritePage();
        case 2:
          return AccountPage();
        default:
          return HomePage();
      }
    }

    return BlocBuilder<NavigationCubit, int>(
      builder: (context, state) {
        return Scaffold(
          backgroundColor: kBlack,
          body: _buildContent(state),
          bottomNavigationBar: Container(
            width: double.infinity,
            height: 54,
            margin: EdgeInsets.only(left: 30, right: 30, bottom: 21),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Color(0xff404040).withOpacity(.87),
                    Color(0xff4b4b4b),
                  ]),
            ),
            padding: EdgeInsets.symmetric(horizontal: 70),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                CustomBottomNavigationItem(
                    icon: context.read<NavigationCubit>().state == 0
                        ? IconlyBold.home
                        : IconlyLight.home,
                    index: 0),
                CustomBottomNavigationItem(
                    icon: context.read<NavigationCubit>().state == 1
                        ? IconlyBold.bookmark
                        : IconlyLight.bookmark,
                    index: 1),
                CustomBottomNavigationItem(
                    icon: context.read<NavigationCubit>().state == 2
                        ? IconlyBold.profile
                        : IconlyLight.profile,
                    index: 2),
              ],
            ),
          ),
        );
      },
    );
  }
}
