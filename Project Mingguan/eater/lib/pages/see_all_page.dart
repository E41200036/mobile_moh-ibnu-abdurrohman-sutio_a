import 'package:eater/widgets/food_data.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import '../themes.dart';
import '../widgets/category_item.dart';

class SeeAllPage extends StatelessWidget {
  const SeeAllPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List foodList = [
      FoodData(
          img: 'https://picsum.photos/seed/image001/500/500',
          name: 'The Lalapan',
          price: 12000),
      FoodData(
          img: 'https://picsum.photos/seed/image002/500/500',
          name: 'Mc Donalds',
          price: 22000),
      FoodData(
          img: 'https://picsum.photos/seed/image003/500/500',
          name: 'Japanes Shushi',
          price: 40000),
      FoodData(
          img: 'https://picsum.photos/seed/image006/500/500',
          name: 'Hamburger',
          price: 70000),
      FoodData(
          img: 'https://picsum.photos/seed/image005/500/500',
          name: 'Nasi Goreng',
          price: 10000),
    ];

    Widget header() {
      return Container(
        margin: EdgeInsets.only(bottom: 28),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Eater',
              style: whiteTextStyle.copyWith(
                  fontSize: 24, fontWeight: FontWeight.w800),
            ),
            Text(
              'Eat together and happy',
              style: greyTextStyle.copyWith(
                fontWeight: semibold,
              ),
            ),
          ],
        ),
      );
    }

    Widget searchInput() {
      return Container(
        width: double.infinity,
        height: 43,
        margin: EdgeInsets.only(bottom: 20),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: kSoftBlack,
        ),
        padding: EdgeInsets.symmetric(horizontal: 30),
        child: Expanded(
            child: Center(
          child: TextFormField(
            style: whiteTextStyle,
            decoration: InputDecoration.collapsed(
                hintText: 'Search', hintStyle: greyTextStyle),
          ),
        )),
      );
    }

    Widget categories() {
      return Container(
        margin: EdgeInsets.only(bottom: 20),
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(children: [
            CategoryItem(
              img: 'assets/image_category1.png',
              name: 'Food',
            ),
            CategoryItem(
              img: 'assets/image_category2.png',
              name: 'Drink',
            ),
            CategoryItem(
              img: 'assets/image_category3.png',
              name: 'Snack',
            ),
            CategoryItem(
              img: 'assets/image_category4.png',
              name: 'Vegetable',
            ),
            CategoryItem(
              img: 'assets/image_category5.png',
              name: 'Cake',
            ),
          ]),
        ),
      );
    }

    Widget foodGalery() {
      return StaggeredGrid.count(
        crossAxisCount: 2,
        mainAxisSpacing: 20,
        crossAxisSpacing: 20,
        children: foodList
            .map((e) => Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: double.infinity,
                      height: foodList.indexOf(e) % 3 == 0 ? 232 : 134,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        image: DecorationImage(
                            image: NetworkImage(e.img), fit: BoxFit.cover),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      e.name,
                      style:
                          whiteTextStyle.copyWith(fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Text(
                      'IDR ${e.price.toString()}',
                      style: whiteTextStyle.copyWith(fontWeight: semibold),
                    ),
                  ],
                ))
            .toList(),
      );
    }

    return Scaffold(
      backgroundColor: kBlack,
      body: ListView(
        padding: EdgeInsets.only(left: 30, right: 30, top: 60),
        children: [
          header(),
          searchInput(),
          categories(),
          foodGalery(),
        ],
      ),
    );
  }
}
