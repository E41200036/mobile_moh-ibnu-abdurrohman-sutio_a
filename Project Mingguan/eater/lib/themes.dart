import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color kBlack = Color(0xff2A2A2A);
Color kSoftBlack = Color(0xff434343);
Color kWhite = Color(0xffFFFFFF);
Color kGrey = Color(0xffA3A3A3);
Color kBlue = Color(0xff139BFA);

FontWeight normal = FontWeight.normal;
FontWeight semibold = FontWeight.w600;

TextStyle blackTextStyle = GoogleFonts.manrope(color: kBlack);
TextStyle whiteTextStyle = GoogleFonts.manrope(color: kWhite);
TextStyle greyTextStyle = GoogleFonts.manrope(color: kGrey);
TextStyle blueTextStyle = GoogleFonts.manrope(color: kBlue);
TextStyle softBlackTextStyle = GoogleFonts.manrope(color: kSoftBlack);
