import 'package:flutter/material.dart';

class PhotoItem extends StatelessWidget {
  final String img;
  const PhotoItem({Key? key, required this.img}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 44,
      height: 44,
      margin: EdgeInsets.only(right: 20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        image: DecorationImage(
          image: AssetImage(img),
          fit: BoxFit.none,
        ),
      ),
    );
  }
}
