class FoodData {
  final String img;
  final String name;
  final int price;

  FoodData({required this.img, required this.name, required this.price});
}
