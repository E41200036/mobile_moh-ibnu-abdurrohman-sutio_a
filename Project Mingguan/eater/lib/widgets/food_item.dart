import 'package:eater/pages/detail_page.dart';
import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';

import '../themes.dart';

class FoodItem extends StatelessWidget {
  final String name;
  final String img;
  final String store;
  final int price;
  const FoodItem(
      {Key? key,
      required this.name,
      required this.img,
      required this.store,
      required this.price})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
          context, MaterialPageRoute(builder: (context) => DetailPage())),
      child: Container(
        width: double.infinity,
        height: 100,
        margin: EdgeInsets.only(bottom: 20),
        child: Stack(
          children: [
            Align(
              alignment: Alignment.bottomLeft,
              child: Container(
                width: double.infinity,
                height: 78,
                padding: EdgeInsets.symmetric(vertical: 10),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: kSoftBlack,
                ),
                child: Row(children: [
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.only(left: 119),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  name,
                                  overflow: TextOverflow.ellipsis,
                                  style: whiteTextStyle.copyWith(
                                    fontWeight: semibold,
                                  ),
                                ),
                                Text(
                                  store,
                                  style: greyTextStyle.copyWith(
                                      fontWeight: semibold, fontSize: 10),
                                ),
                              ],
                            ),
                          ),
                          Text(
                            'IDR $price',
                            style: whiteTextStyle.copyWith(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 20),
                    child: Icon(
                      IconlyLight.plus,
                      color: kWhite,
                    ),
                  ),
                ]),
              ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: Container(
                width: 90,
                margin: EdgeInsets.only(left: 10),
                height: 90,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                      isAntiAlias: true,
                      image: AssetImage(img),
                      fit: BoxFit.none),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
