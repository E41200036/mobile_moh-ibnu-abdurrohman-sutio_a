import 'package:flutter/material.dart';

import '../themes.dart';

class CategoryItem extends StatelessWidget {
  final String img, name;
  const CategoryItem({Key? key, required this.img, required this.name})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 56,
      margin: EdgeInsets.only(right: 20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: kSoftBlack,
      ),
      padding: EdgeInsets.all(4),
      child: Column(
        children: [
          Container(
            width: double.infinity,
            height: 45,
            margin: EdgeInsets.only(bottom: 5),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              image: DecorationImage(
                image: AssetImage(img),
                fit: BoxFit.none,
              ),
            ),
          ),
          Text(
            name,
            style: whiteTextStyle.copyWith(
              fontSize: 10,
              fontWeight: semibold,
            ),
          ),
        ],
      ),
    );
  }
}
