import 'package:eater/cubit/navigation_cubit.dart';
import 'package:eater/themes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CustomBottomNavigationItem extends StatelessWidget {
  final IconData icon;
  final int index;
  const CustomBottomNavigationItem(
      {Key? key, required this.icon, required this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        context.read<NavigationCubit>().navigateTo(index);
      },
      child: Icon(
        icon,
        color: kWhite,
      ),
    );
  }
}
