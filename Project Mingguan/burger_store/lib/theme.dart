import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color kBlack = Color(0xff0E0943);
Color kGrey = Color(0xffAFAFAF);
Color kWhite = Colors.white;
Color kOrange = Color(0xffFA7854);
Color kPurple = Color(0xff554AB2);
Color kSoftPurple = Color(0xff9B8FFF);
Color kDarkPurple = Color(0xff4A3EAE);
Color kBackground = Color(0xffF6F5FF);

TextStyle blackTextStyle = GoogleFonts.poppins(color: kBlack);
TextStyle greyTextStyle = GoogleFonts.poppins(color: kGrey);
TextStyle whiteTextStyle = GoogleFonts.poppins(color: kWhite);
TextStyle softPurpleTextStyle = GoogleFonts.poppins(color: kSoftPurple);
TextStyle orangeTextStyle = GoogleFonts.poppins(color: kOrange);

FontWeight normal = FontWeight.w400;
FontWeight medium = FontWeight.w500;
FontWeight semibold = FontWeight.w600;
