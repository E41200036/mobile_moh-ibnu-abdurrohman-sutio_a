import 'package:burger_store/theme.dart';
import 'package:burger_store/widgets/pie_item.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget header() {
      return Container(
        margin: EdgeInsets.only(top: 30, bottom: 33),
        child: Row(
          children: [
            Container(
              width: 60,
              margin: EdgeInsets.only(right: 18),
              height: 60,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                  image: AssetImage('assets/image_user.png'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Hello, Shadam',
                  style:
                      whiteTextStyle.copyWith(fontSize: 20, fontWeight: medium),
                ),
                SizedBox(
                  height: 2,
                ),
                Text(
                  'What Pie you want to try today?',
                  style: softPurpleTextStyle,
                ),
              ],
            )
          ],
        ),
      );
    }

    Widget searchInput() {
      return Container(
        padding: EdgeInsets.all(15),
        margin: EdgeInsets.only(bottom: 30),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          color: kDarkPurple,
        ),
        child: Row(
          children: [
            Expanded(
              child: TextFormField(
                decoration: InputDecoration.collapsed(
                    hintText: 'Find your favorite Pie',
                    hintStyle: softPurpleTextStyle),
              ),
            ),
            Image.asset(
              'assets/icon_search.png',
              width: 20,
              height: 20,
            ),
          ],
        ),
      );
    }

    Widget category() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Category',
            style: blackTextStyle.copyWith(fontSize: 20, fontWeight: semibold),
          ),
          SizedBox(
            height: 12,
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                Container(
                  margin: EdgeInsets.only(right: 25),
                  child: Column(
                    children: [
                      Container(
                        width: 60,
                        height: 60,
                        padding: EdgeInsets.all(15),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          color: kWhite,
                        ),
                        child: Image.asset('assets/icon_pie1.png'),
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      Text(
                        'Salty Pie',
                        style: greyTextStyle.copyWith(fontSize: 12),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 25),
                  child: Column(
                    children: [
                      Container(
                        width: 60,
                        height: 60,
                        padding: EdgeInsets.all(15),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          color: kOrange,
                        ),
                        child: Image.asset('assets/icon_pie2_active.png'),
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      Text(
                        'Sweet Pie',
                        style: blackTextStyle.copyWith(
                            fontSize: 12, fontWeight: medium),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 25),
                  child: Column(
                    children: [
                      Container(
                        width: 60,
                        height: 60,
                        padding: EdgeInsets.all(15),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          color: kWhite,
                        ),
                        child: Image.asset('assets/icon_pie3.png'),
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      Text(
                        'Fruit Pie',
                        style: greyTextStyle.copyWith(fontSize: 12),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 25),
                  child: Column(
                    children: [
                      Container(
                        width: 60,
                        height: 60,
                        padding: EdgeInsets.all(15),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          color: kWhite,
                        ),
                        child: Image.asset('assets/icon_drink.png'),
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      Text(
                        'Drinks',
                        style: greyTextStyle.copyWith(fontSize: 12),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 30,
          ),
        ],
      );
    }

    Widget popularPie() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Popular Sweet Pie',
            style: blackTextStyle.copyWith(fontSize: 20, fontWeight: semibold),
          ),
          SizedBox(height: 12),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(children: [
              PieItem(
                pieImage: 'assets/image_pie1.png',
                pieName: 'Cream Sweet Pie',
                minDuration: 20,
                maxDuration: 30,
                rating: 4.5,
              ),
              PieItem(
                pieImage: 'assets/image_pie2.png',
                pieName: 'DoubleJumbo Pie',
                minDuration: 30,
                maxDuration: 45,
                rating: 4.8,
              ),
              PieItem(
                pieImage: 'assets/image_pie3.png',
                pieName: 'Fruit’s Small Pie',
                minDuration: 20,
                maxDuration: 30,
                rating: 4.5,
              ),
            ]),
          ),
        ],
      );
    }

    Widget content() {
      return Container(
        width: double.infinity,
        height: MediaQuery.of(context).size.height,
        padding: EdgeInsets.all(30),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.vertical(top: Radius.circular(30)),
          color: Color(0xffF6F5FF),
        ),
        child: SingleChildScrollView(
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            category(),
            popularPie(),
            SizedBox(
              height: 70,
            ),
          ]),
        ),
      );
    }

    return Scaffold(
      backgroundColor: kPurple,
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 30, left: 30, right: 30),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        header(),
                        searchInput(),
                      ]),
                ),
                content(),
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              width: double.infinity,
              height: 85,
              padding: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
              decoration: BoxDecoration(
                color: kWhite,
                borderRadius: BorderRadius.vertical(top: Radius.circular(30)),
              ),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      children: [
                        Image.asset(
                          'assets/icon_home.png',
                          width: 20,
                          height: 20,
                        ),
                        SizedBox(
                          height: 4,
                        ),
                        Text(
                          'Home',
                          style: orangeTextStyle.copyWith(fontWeight: medium),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        Image.asset(
                          'assets/icon_cart.png',
                          width: 20,
                          height: 20,
                        ),
                        SizedBox(
                          height: 4,
                        ),
                        Text(
                          'Cart',
                          style: greyTextStyle.copyWith(fontWeight: medium),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        Image.asset(
                          'assets/icon_love.png',
                          width: 20,
                          height: 20,
                        ),
                        SizedBox(
                          height: 4,
                        ),
                        Text(
                          'Love',
                          style: greyTextStyle.copyWith(fontWeight: medium),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        Image.asset(
                          'assets/icon_user.png',
                          width: 20,
                          height: 20,
                        ),
                        SizedBox(
                          height: 4,
                        ),
                        Text(
                          'Profile',
                          style: greyTextStyle.copyWith(fontWeight: medium),
                        ),
                      ],
                    ),
                  ]),
            ),
          ),
        ],
      ),
    );
  }
}
