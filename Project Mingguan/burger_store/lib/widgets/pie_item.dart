import 'package:burger_store/theme.dart';
import 'package:flutter/material.dart';

class PieItem extends StatelessWidget {
  final String pieImage, pieName;
  final int minDuration, maxDuration;
  final double rating;

  const PieItem(
      {Key? key,
      required this.pieImage,
      required this.pieName,
      required this.minDuration,
      required this.maxDuration,
      required this.rating})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 150,
      padding: EdgeInsets.all(12),
      height: 223,
      margin: EdgeInsets.only(right: 12),
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: BorderRadius.circular(12),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: 126,
            height: 136,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              image: DecorationImage(
                image: AssetImage(pieImage),
                fit: BoxFit.cover,
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: 86,
                  height: 25,
                  decoration: BoxDecoration(
                    color: kWhite.withOpacity(0.70),
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(12),
                        bottomLeft: Radius.circular(12)),
                  ),
                  child: Center(
                    child: Text(
                      minDuration.toString() +
                          "-" +
                          maxDuration.toString() +
                          " Min",
                      style: blackTextStyle.copyWith(
                        fontSize: 12,
                        fontWeight: medium,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 12,
          ),
          SizedBox(
            width: 126,
            child: Text(
              pieName,
              style: blackTextStyle.copyWith(fontWeight: semibold),
            ),
          ),
          SizedBox(
            height: 12,
          ),
          Row(
            children: [
              Image.asset(
                'assets/icon_star.png',
                width: 14,
                height: 14,
              ),
              SizedBox(
                width: 4,
              ),
              Text(
                rating.toString(),
                style:
                    orangeTextStyle.copyWith(fontSize: 12, fontWeight: medium),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
