import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color kBlack = Colors.black;
Color kGrey = Color(0xffA3A3A3);
Color kSoftGrey = Color(0xffF2F2F2);
Color kGreen = Color(0xff14C044);
Color kDarkGreen = Color(0xff169F3C);
Color kWhite = Colors.white;

FontWeight normal = FontWeight.normal;
FontWeight medium = FontWeight.w500;
FontWeight semibold = FontWeight.w600;

TextStyle blackTextStyle = GoogleFonts.inter(color: kBlack);
TextStyle greyTextStyle = GoogleFonts.inter(color: kGrey);
TextStyle softGreyTextStyle = GoogleFonts.inter(color: kSoftGrey);
TextStyle greenTextStyle = GoogleFonts.inter(color: kGreen);
TextStyle whiteTextStyle = GoogleFonts.inter(color: kWhite);
