import 'package:bloc/bloc.dart';

class CategoryCubit extends Cubit<int> {
  CategoryCubit() : super(0);

  void setCategory(int index) {
    emit(index);
  }
}
