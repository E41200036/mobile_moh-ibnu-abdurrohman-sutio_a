import 'package:flutter/material.dart';

import '../theme.dart';

class ActivityItem extends StatelessWidget {
  final String surah;
  final int date, ayah;
  final String month;
  const ActivityItem(
      {Key? key,
      required this.surah,
      required this.date,
      required this.month,
      required this.ayah})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 70,
      padding: EdgeInsets.all(12),
      margin: EdgeInsets.only(bottom: 16),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16), color: Colors.grey[200]),
      child: Row(
        children: [
          Container(
            width: 43,
            height: 43,
            margin: EdgeInsets.only(right: 28),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(16),
              color: kWhite,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  date.toString(),
                  style: blackTextStyle.copyWith(
                    fontSize: 18,
                    fontWeight: semibold,
                  ),
                ),
                Text(
                  month,
                  style: blackTextStyle.copyWith(
                    fontSize: 10,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'QS: $surah',
                  style: blackTextStyle.copyWith(fontWeight: semibold),
                ),
                SizedBox(
                  height: 2,
                ),
                Text(
                  '$ayah Ayat',
                  style: greyTextStyle.copyWith(fontSize: 12),
                ),
              ],
            ),
          ),
          Container(
            width: 77,
            height: 25,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20), color: kGreen),
            child: Center(
              child: Text(
                'Completed',
                style: whiteTextStyle.copyWith(
                  fontSize: 12,
                  fontWeight: semibold,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
