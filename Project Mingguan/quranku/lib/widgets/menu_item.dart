import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';

import '../theme.dart';

class MenuItem extends StatelessWidget {
  final String name;
  final Icon icon;
  const MenuItem({Key? key, required this.name, required this.icon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: 46,
          height: 46,
          margin: EdgeInsets.only(bottom: 8),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: kSoftGrey,
          ),
          padding: EdgeInsets.all(11),
          child: Center(
            child: icon,
          ),
        ),
        Text(
          name,
          style: blackTextStyle.copyWith(fontSize: 12),
        ),
      ],
    );
  }
}
