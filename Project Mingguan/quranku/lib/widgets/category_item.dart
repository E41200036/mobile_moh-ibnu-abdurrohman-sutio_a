import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:quranku/cubit/category_cubit.dart';

import '../theme.dart';

class CategoryItem extends StatelessWidget {
  final String name;
  final int index;

  const CategoryItem({Key? key, required this.name, required this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        context.read<CategoryCubit>().setCategory(index);
      },
      child: Container(
        height: 25,
        margin: EdgeInsets.only(right: 24),
        padding: EdgeInsets.symmetric(horizontal: 10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color:
              context.read<CategoryCubit>().state == index ? kGreen : kSoftGrey,
        ),
        child: Center(
          child: Text(
            name,
            style: whiteTextStyle.copyWith(
              fontSize: 12,
              fontWeight: semibold,
              color: context.read<CategoryCubit>().state == index
                  ? kWhite
                  : kBlack,
            ),
          ),
        ),
      ),
    );
  }
}
