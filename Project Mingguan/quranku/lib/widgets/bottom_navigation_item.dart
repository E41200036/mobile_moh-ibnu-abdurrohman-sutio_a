import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../cubit/navigation_cubit.dart';
import '../theme.dart';

class BottomNavigationItem extends StatelessWidget {
  final IconData icon;
  final int index;
  const BottomNavigationItem(
      {Key? key, required this.icon, required this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        context.read<NavigationCubit>().setNavigation(index);
      },
      child: Container(
        width: 40,
        height: 40,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          color: context.read<NavigationCubit>().state == index
              ? kGreen
              : Colors.transparent,
        ),
        child: Icon(icon,
            color: context.read<NavigationCubit>().state == index
                ? Colors.white
                : kBlack),
      ),
    );
  }
}
