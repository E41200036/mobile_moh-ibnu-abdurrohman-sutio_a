import 'package:flutter/material.dart';

import '../theme.dart';

class ReadItem extends StatelessWidget {
  final String surahName;
  final int ayat;
  final String arabic;
  final String date;
  const ReadItem(
      {Key? key,
      required this.surahName,
      required this.ayat,
      required this.arabic,
      required this.date})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 24),
      child: Row(
        children: [
          Image.asset(
            'assets/icon_check.png',
            width: 24,
            height: 24,
          ),
          SizedBox(
            width: 16,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  surahName,
                  style: blackTextStyle.copyWith(
                    fontWeight: semibold,
                  ),
                ),
                SizedBox(
                  height: 2,
                ),
                Text(
                  arabic,
                  style: greyTextStyle.copyWith(fontSize: 12),
                ),
              ],
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text(
                '$ayat Ayat',
                style:
                    blackTextStyle.copyWith(fontSize: 12, fontWeight: semibold),
              ),
              SizedBox(
                height: 2,
              ),
              Text(
                date,
                style: greyTextStyle.copyWith(fontSize: 12),
              ),
            ],
          )
        ],
      ),
    );
  }
}
