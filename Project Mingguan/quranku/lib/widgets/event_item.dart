import 'package:flutter/material.dart';

import '../theme.dart';

class EventItem extends StatelessWidget {
  final String img, title, content, date;
  const EventItem(
      {Key? key,
      required this.img,
      required this.title,
      required this.content,
      required this.date})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 24),
      child: Row(
        children: [
          Container(
            width: 80,
            height: 80,
            margin: EdgeInsets.only(right: 18),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(16),
              image: DecorationImage(image: AssetImage(img), fit: BoxFit.cover),
            ),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: blackTextStyle.copyWith(fontWeight: semibold),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  content,
                  style: blackTextStyle.copyWith(
                    fontSize: 12,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  date,
                  style: greyTextStyle.copyWith(fontSize: 10),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
