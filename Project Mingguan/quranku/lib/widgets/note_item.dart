import 'package:flutter/material.dart';
import 'package:quranku/theme.dart';

class NoteItem extends StatelessWidget {
  final String img, title, date;
  const NoteItem(
      {Key? key, required this.img, required this.title, required this.date})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: (MediaQuery.of(context).size.width - 116) / 3,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            height: 111,
            width: double.infinity,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(16),
              image: DecorationImage(image: AssetImage(img), fit: BoxFit.cover),
            ),
            margin: EdgeInsets.only(bottom: 14),
          ),
          SizedBox(
            width: double.infinity,
            child: Text(
              title,
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              style: blackTextStyle.copyWith(
                fontSize: 14,
                fontWeight: semibold,
              ),
            ),
          ),
          SizedBox(
            height: 3,
          ),
          Text(
            date,
            style: greyTextStyle.copyWith(
              fontSize: 10,
            ),
          ),
        ],
      ),
    );
  }
}
