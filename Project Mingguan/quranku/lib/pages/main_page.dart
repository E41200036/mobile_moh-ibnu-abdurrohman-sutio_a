import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:iconly/iconly.dart';
import 'package:quranku/cubit/navigation_cubit.dart';
import 'package:quranku/pages/home_page.dart';
import 'package:quranku/pages/statistic_page.dart';
import 'package:quranku/widgets/bottom_navigation_item.dart';

import '../cubit/category_cubit.dart';

class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget buildContent(int currentPage) {
      switch (currentPage) {
        case 0:
          return HomePage();
        case 1:
          return StatisticPage();
        default:
          return HomePage();
      }
    }

    return BlocBuilder<NavigationCubit, int>(
      builder: (context, currentPage) {
        return BlocBuilder<CategoryCubit, int>(
          builder: (context, state) {
            return Scaffold(
              body: buildContent(currentPage),
              bottomNavigationBar: Container(
                width: double.infinity,
                height: 89,
                padding: EdgeInsets.symmetric(horizontal: 60),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    BottomNavigationItem(icon: IconlyLight.category, index: 0),
                    BottomNavigationItem(icon: IconlyLight.chart, index: 1),
                    BottomNavigationItem(icon: IconlyLight.calendar, index: 2),
                    BottomNavigationItem(icon: IconlyLight.profile, index: 3),
                  ],
                ),
              ),
            );
          },
        );
      },
    );
  }
}
