import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';
import 'package:quranku/widgets/activity_item.dart';
import 'package:quranku/widgets/note_item.dart';

import '../theme.dart';

class StatisticPage extends StatelessWidget {
  const StatisticPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget header() {
      return Container(
        margin: EdgeInsets.only(bottom: 40, top: 60),
        child: Row(
          children: [
            Container(
              width: 48,
              height: 48,
              margin: EdgeInsets.only(right: 18),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage(
                    'assets/image_profile.png',
                  ),
                ),
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Alvioni. Tr',
                    style: blackTextStyle.copyWith(
                      fontSize: 18,
                      fontWeight: semibold,
                    ),
                  ),
                  SizedBox(
                    height: 2,
                  ),
                  Text(
                    'All of your progress',
                    style: greyTextStyle.copyWith(
                      fontSize: 12,
                    ),
                  ),
                ],
              ),
            ),
            Icon(
              IconlyLight.arrow_down_2,
              size: 18,
            ),
          ],
        ),
      );
    }

    Widget activity() {
      return Container(
        margin: EdgeInsets.only(bottom: 34),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'All Activity',
                  style: blackTextStyle.copyWith(
                    fontWeight: semibold,
                  ),
                ),
                Icon(
                  IconlyLight.plus,
                  size: 22,
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            ActivityItem(
                surah: 'Al Baqarah', date: 24, month: 'Dec', ayah: 286),
            ActivityItem(surah: 'As Sajadah', date: 23, month: 'Apr', ayah: 30),
          ],
        ),
      );
    }

    Widget notes() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Notes',
            style: blackTextStyle.copyWith(
              fontSize: 14,
              fontWeight: semibold,
            ),
          ),
          SizedBox(
            height: 30,
          ),
          Wrap(
            crossAxisAlignment: WrapCrossAlignment.center,
            spacing: 26,
            runSpacing: 24,
            children: [
              NoteItem(
                img: 'assets/image_note1.png',
                title: 'Tahsin Quran',
                date: '24 Dec 2021',
              ),
              NoteItem(
                img: 'assets/image_note2.png',
                title: 'Islam Fashion',
                date: '24 Dec 2021',
              ),
              NoteItem(
                img: 'assets/image_note3.png',
                title: 'Shalat Jamaah',
                date: '24 Dec 2021',
              ),
              NoteItem(
                img: 'assets/image_note4.png',
                title: 'Ka\'bah',
                date: '24 Dec 2021',
              ),
              NoteItem(
                img: 'assets/image_note5.png',
                title: 'Madinah',
                date: '24 Dec 2021',
              ),
              NoteItem(
                img: 'assets/image_note6.png',
                title: 'Mekkah',
                date: '24 Dec 2021',
              ),
            ],
          ),
        ],
      );
    }

    return ListView(
      padding: EdgeInsets.all(30),
      children: [
        header(),
        activity(),
        notes(),
      ],
    );
  }
}
