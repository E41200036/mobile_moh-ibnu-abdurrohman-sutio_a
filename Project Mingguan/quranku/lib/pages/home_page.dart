import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';

import '../theme.dart';
import '../widgets/category_item.dart';
import '../widgets/event_item.dart';
import '../widgets/menu_item.dart';
import '../widgets/read_item.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget header() {
      return Container(
        margin: EdgeInsets.only(bottom: 40),
        child: Row(
          children: [
            Container(
              width: 48,
              height: 48,
              margin: EdgeInsets.only(right: 18),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage(
                    'assets/image_profile.png',
                  ),
                ),
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Alvioni. Tr',
                    style: blackTextStyle.copyWith(
                      fontSize: 18,
                      fontWeight: semibold,
                    ),
                  ),
                  SizedBox(
                    height: 2,
                  ),
                  Text(
                    'Have you read Quran today ?',
                    style: greyTextStyle.copyWith(
                      fontSize: 12,
                    ),
                  ),
                ],
              ),
            ),
            Icon(
              IconlyLight.arrow_down_2,
              size: 18,
            ),
          ],
        ),
      );
    }

    Widget categories() {
      return Container(
        margin: EdgeInsets.only(bottom: 24),
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              CategoryItem(name: 'Daily', index: 0),
              CategoryItem(name: 'Weekly', index: 1),
              CategoryItem(name: 'Monthly', index: 2),
            ],
          ),
        ),
      );
    }

    Widget lastRead() {
      return Container(
        margin: EdgeInsets.only(bottom: 34),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ReadItem(
                surahName: 'QS: Al Kahfi',
                ayat: 112,
                arabic: 'Mekkah',
                date: '12 Desember 2019'),
            ReadItem(
                surahName: 'QS: Al Ma’idah',
                ayat: 120,
                arabic: 'Madinah',
                date: '20 Desember 2019'),
          ],
        ),
      );
    }

    Widget menu() {
      return Container(
        margin: EdgeInsets.only(bottom: 34),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Menu',
              style: blackTextStyle.copyWith(
                fontWeight: semibold,
              ),
            ),
            SizedBox(
              height: 24,
            ),
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              MenuItem(
                name: 'Zakat',
                icon: Icon(
                  IconlyLight.wallet,
                  color: kDarkGreen,
                ),
              ),
              MenuItem(
                name: 'Masjid',
                icon: Icon(
                  IconlyLight.home,
                  color: kDarkGreen,
                ),
              ),
              MenuItem(
                name: 'Hadisr',
                icon: Icon(
                  IconlyLight.bookmark,
                  color: kDarkGreen,
                ),
              ),
              MenuItem(
                name: 'Event',
                icon: Icon(
                  IconlyLight.calendar,
                  color: kDarkGreen,
                ),
              ),
              MenuItem(
                name: 'Motivasi',
                icon: Icon(
                  IconlyLight.star,
                  color: kDarkGreen,
                ),
              ),
            ])
          ],
        ),
      );
    }

    Widget newEvent() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'New Event',
            style: blackTextStyle.copyWith(fontWeight: semibold),
          ),
          SizedBox(
            height: 24,
          ),
          EventItem(
            img: 'assets/image_event1.png',
            title: 'Ngaji Bareng',
            content:
                "Program 'Smart Santri' yang digelar rutin Pemerintah Kabupaten Banyu..",
            date: '12 Desember 2022',
          ),
          EventItem(
            img: 'assets/image_event2.png',
            title: 'Jumlat Berkah',
            content:
                "Hadits tersebut menguraikan tentang bentuk-bentuk keimanan. Antaranya...",
            date: '22 April 2022',
          ),
        ],
      );
    }

    return ListView(
      padding: EdgeInsets.only(left: 30, right: 30, top: 60),
      children: [
        header(),
        categories(),
        lastRead(),
        menu(),
        newEvent(),
      ],
    );
  }
}
