import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color kPrimary = Color(0xff6C5ECF);
Color kSecondary = Color(0xff38ABBE);
Color kAlert = Color(0xffED6363);
Color kPrice = Color(0xff2C96F1);
Color kBackground1 = Color(0xff1F1D2B);
Color kBackground2 = Color(0xff2B2937);
Color kBackground3 = Color(0xff242231);
Color kPrimaryText = Color(0xffE1E1E1);
Color kSecondaryText = Color(0xff999999);
Color kSubtitleColor = Color(0xff504F5E);

TextStyle primaryTextStyle = GoogleFonts.poppins(color: kPrimaryText);
TextStyle secondaryTextStyle = GoogleFonts.poppins(color: kSecondaryText);
TextStyle priceTextStyle = GoogleFonts.poppins(color: kPrice);
TextStyle subtitleTextStyle = GoogleFonts.poppins(color: kSubtitleColor);
TextStyle purpleTextStyle = GoogleFonts.poppins(color: kPrimary);

FontWeight light = FontWeight.w300;
FontWeight regular = FontWeight.w400;
FontWeight medium = FontWeight.w500;
FontWeight semibold = FontWeight.w600;
FontWeight bold = FontWeight.w700;
