import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:nontonmovie/models/user_model.dart';
import 'package:nontonmovie/services/auth_service.dart';
import 'package:nontonmovie/services/user_service.dart';

part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  AuthCubit() : super(AuthInitial());

  void signUp(
      {required String fullname,
      required String email,
      required String password}) async {
    try {
      emit(AuthLoading());
      UserModel user = await AuthService()
          .signUp(fullname: fullname, email: email, password: password);
      emit(AuthSuccess(user: user));
    } catch (e) {
      emit(AuthFailure(message: e.toString()));
    }
  }

  void getCurrentUser(String id) async {
    try {
      AuthLoading();
      UserModel user = await UserService().getCurrentUser(id);
      emit(AuthSuccess(user: user));
    } catch (e) {
      emit(AuthFailure(message: e.toString()));
    }
  }
}
