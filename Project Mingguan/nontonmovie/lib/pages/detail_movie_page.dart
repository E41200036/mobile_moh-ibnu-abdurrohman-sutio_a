import 'package:flutter/material.dart';
import 'package:nontonmovie/models/movie_model.dart';
import 'package:nontonmovie/providers/movie_providers.dart';
import 'package:nontonmovie/theme.dart';
import 'package:provider/provider.dart';

class DetailMoviePage extends StatelessWidget {
  final MovieModel movie;

  DetailMoviePage(this.movie);

  @override
  Widget build(BuildContext context) {
    var movieProviders = Provider.of<MovieProviders>(context);
    Widget header() {
      return Container(
        width: double.infinity,
        height: 300,
        margin: EdgeInsets.only(bottom: 20),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(
              'https://image.tmdb.org/t/p/w780${movie.backdrop_path}',
            ),
            fit: BoxFit.cover,
          ),
        ),
      );
    }

    Widget title() {
      return Padding(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              movie.title,
              style: blackTextStyle.copyWith(
                fontSize: 20,
                fontWeight: bold,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              width: double.infinity,
              height: 0.5,
              color: Color.fromARGB(255, 224, 224, 224),
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Release Date',
                      style: blackTextStyle.copyWith(
                        fontSize: 14,
                        fontWeight: medium,
                      ),
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    // date format dd/mm/yyyy
                    Text(
                      movie.release_date.split('-')[2] +
                          '-' +
                          movie.release_date.split('-')[1] +
                          '-' +
                          movie.release_date.split('-')[0],
                      style: greyTextStyle.copyWith(fontSize: 14),
                    ),
                  ],
                ),
                SizedBox(
                  width: 20,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Vote Average',
                      style: blackTextStyle.copyWith(
                        fontSize: 14,
                        fontWeight: medium,
                      ),
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Text(
                      movie.vote_average.toString(),
                      style: greyTextStyle.copyWith(fontSize: 14),
                    ),
                  ],
                ),
                SizedBox(
                  width: 20,
                ),
                // watch time
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Watch Time',
                      style: blackTextStyle.copyWith(
                        fontSize: 14,
                        fontWeight: medium,
                      ),
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.access_time,
                          color: Colors.grey,
                          size: 14,
                        ),
                        SizedBox(
                          width: 2,
                        ),
                        Text(
                          '${movie.vote_count}',
                          style: greyTextStyle.copyWith(fontSize: 14),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              width: double.infinity,
              height: 0.5,
              color: Color.fromARGB(255, 224, 224, 224),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              'Overview',
              style: blackTextStyle.copyWith(
                fontSize: 16,
                fontWeight: bold,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              movie.overview,
              style: greyTextStyle.copyWith(fontSize: 14),
            ),

            // related movie
            SizedBox(
              height: 20,
            ),
            Container(
              width: double.infinity,
              height: 0.5,
              color: Color.fromARGB(255, 224, 224, 224),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              'Related Movie',
              style: blackTextStyle.copyWith(
                fontSize: 16,
                fontWeight: bold,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: SingleChildScrollView(
                clipBehavior: Clip.none,
                scrollDirection: Axis.horizontal,
                child: FutureBuilder<List<MovieModel>>(
                  future: movieProviders.getRelatedMovies(movie.id),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      // show only 4 movie
                      List<MovieModel> relatedMovies =
                          snapshot.data as List<MovieModel>;
                      return (relatedMovies.length > 4)
                          ? Container(
                              width: MediaQuery.of(context).size.width,
                              height: 202,
                              child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: relatedMovies.length,
                                itemBuilder: (context, index) {
                                  return Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        width: 150,
                                        height: 150,
                                        margin: EdgeInsets.only(right: 20),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          image: DecorationImage(
                                            image: NetworkImage(
                                              'https://image.tmdb.org/t/p/w780${snapshot.data?[index].poster_path}',
                                            ),
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      SizedBox(
                                        width: 150,
                                        child: Text(
                                          snapshot.data![index].title +
                                              ' (${snapshot.data![index].release_date.split('-')[0]})',
                                          style: blackTextStyle.copyWith(
                                            fontSize: 12,
                                          ),
                                          maxLines: 2,
                                          overflow: TextOverflow.clip,
                                        ),
                                      ),
                                    ],
                                  );
                                },
                              ),
                            )
                          : Container();
                    } else {
                      return Container();
                    }
                  },
                ),
              ),
            ), // related movie
            SizedBox(
              height: 10,
            ),
          ],
        ),
      );
    }

    return Scaffold(
      body: ListView(
        children: [
          header(),
          title(),
        ],
      ),
    );
  }
}
