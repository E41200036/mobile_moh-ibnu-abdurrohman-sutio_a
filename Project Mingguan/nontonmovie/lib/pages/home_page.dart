import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:nontonmovie/models/movie_model.dart';
import 'package:nontonmovie/pages/detail_movie_page.dart';
import 'package:nontonmovie/providers/movie_providers.dart';
import 'package:nontonmovie/theme.dart';
import 'package:nontonmovie/widgets/top_movie_item.dart';
import 'package:nontonmovie/widgets/upcoming_movie_item.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var movieProviders = Provider.of<MovieProviders>(context);

    Widget header() {
      return Container(
        margin: EdgeInsets.only(bottom: 30),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Image.asset(
              'assets/icon_sidebar.png',
              width: 18,
            ),
            Image.asset(
              'assets/avatar.png',
              width: 45,
            ),
          ],
        ),
      );
    }

    Widget upcomingMovie() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Coming Soon',
            style: blackTextStyle.copyWith(
              fontSize: 20,
              fontWeight: bold,
            ),
          ),
          SizedBox(
            height: 12,
          ),
          SingleChildScrollView(
            clipBehavior: Clip.none,
            scrollDirection: Axis.horizontal,
            child: FutureBuilder<List<MovieModel>>(
              future: movieProviders.getUpcomingMovie(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return Row(
                    children: snapshot.data!.map((movie) {
                      return UpcomingMovieItem(movie: movie);
                    }).toList(),
                  );
                } else {
                  return Container(
                    height: 200,
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  );
                }
              },
            ),
          ),
        ],
      );
    }

    Widget topMovie() {
      return Container(
        margin: EdgeInsets.only(top: 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Top Movie',
              style: blackTextStyle.copyWith(
                fontSize: 20,
                fontWeight: bold,
              ),
            ),
            SizedBox(
              height: 12,
            ),
            SingleChildScrollView(
              clipBehavior: Clip.none,
              scrollDirection: Axis.horizontal,
              child: FutureBuilder<List<MovieModel>>(
                  future: movieProviders.getPopularMovies(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return Row(
                        children: snapshot.data!.map((movie) {
                          return TopMovieItem(movie: movie);
                        }).toList(),
                      );
                    } else {
                      return Container(
                        height: 200,
                        child: Center(
                          child: CircularProgressIndicator(),
                        ),
                      );
                    }
                  }),
            )
          ],
        ),
      );
    }

    Widget listMovie() {
      return Container(
        margin: EdgeInsets.only(top: 30),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text(
            'List Movie',
            style: blackTextStyle.copyWith(
              fontSize: 20,
              fontWeight: bold,
            ),
          ),
          SizedBox(
            height: 12,
          ),
          // list movie with staggered grid view
          FutureBuilder<List<MovieModel>>(
            future: movieProviders.getNowPlayingMovies(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return StaggeredGrid.count(
                    crossAxisCount: 2,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 30,
                    children: [
                      for (var movie in snapshot.data!)
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        DetailMoviePage(movie)));
                          },
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                margin: EdgeInsets.only(right: 10),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(10),
                                  child: Image.network(
                                    'https://image.tmdb.org/t/p/w500/${movie.poster_path}',
                                    fit: BoxFit.cover,
                                    height: MediaQuery.of(context).size.height *
                                        0.3,
                                    width:
                                        MediaQuery.of(context).size.width / 2 -
                                            20,
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                movie.title +
                                    ' (${movie.release_date.split('-')[0]})',
                                maxLines: 2,
                                overflow: TextOverflow.clip,
                                style: blackTextStyle.copyWith(
                                  fontSize: 12,
                                  fontWeight: bold,
                                ),
                              ),
                            ],
                          ),
                        ),
                    ]);
              } else {
                return Container(
                  height: 200,
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                );
              }
            },
          ),
        ]),
      );
    }

    return Scaffold(
      body: SafeArea(
        child: ListView(
          padding: EdgeInsets.all(30),
          children: [
            header(),
            upcomingMovie(),
            topMovie(),
            listMovie(),
          ],
        ),
      ),
    );
  }
}
