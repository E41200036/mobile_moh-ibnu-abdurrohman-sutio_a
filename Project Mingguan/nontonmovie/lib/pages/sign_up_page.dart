import 'package:cool_alert/cool_alert.dart';
import 'package:email_validator/email_validator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart';
import 'package:nontonmovie/cubit/auth_cubit.dart';
import 'package:nontonmovie/models/user_model.dart';
import 'package:provider/provider.dart';

import '../theme.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  // @override
  // void initState() {
  //   Future.delayed(Duration.zero, () {
  //     User? user = FirebaseAuth.instance.currentUser;
  //     if (user != null) {
  //       print(user.email);
  //       context.read<AuthCubit>().getCurrentUser(user.uid);
  //       Navigator.pushNamed(context, '/home');
  //       ;
  //     } else {
  //       print('user null');
  //     }
  //     super.initState();
  //   });
  // }

  TextEditingController _fullnameController = TextEditingController(text: '');
  TextEditingController _emailAddressController =
      TextEditingController(text: '');
  TextEditingController _passwordController = TextEditingController(text: '');

  @override
  Widget build(BuildContext context) {
    void resetField() {
      _fullnameController.clear();
      _emailAddressController.clear();
      _passwordController.clear();
    }

    Widget buttonSignUp() {
      return BlocConsumer<AuthCubit, AuthState>(
        listener: (context, state) {
          if (state is AuthSuccess) {
            resetField();
            Navigator.pushReplacementNamed(context, '/home');
          } else if (state is AuthFailure) {
            CoolAlert.show(
              context: context,
              title: 'Error',
              text: state.message,
              type: CoolAlertType.error,
              autoCloseDuration: Duration(seconds: 3),
            );
            resetField();
          }
        },
        builder: (context, state) {
          if (state is AuthLoading) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            return Container(
              margin: EdgeInsets.only(top: 40),
              width: double.infinity,
              height: 45,
              decoration: BoxDecoration(
                color: kPurple,
                borderRadius: BorderRadius.circular(99),
              ),
              child: TextButton(
                onPressed: () {
                  if (_fullnameController.text.isEmpty ||
                      _emailAddressController.text.isEmpty ||
                      _passwordController.text.isEmpty) {
                    CoolAlert.show(
                      context: context,
                      title: 'Oops',
                      text: 'Please fill all field',
                      type: CoolAlertType.error,
                      autoCloseDuration: Duration(seconds: 2),
                    );
                  }
                  // check if email is valid
                  if (!EmailValidator.validate(_emailAddressController.text)) {
                    CoolAlert.show(
                      context: context,
                      title: 'Oops',
                      text: 'Please enter a valid email',
                      type: CoolAlertType.error,
                      autoCloseDuration: Duration(seconds: 2),
                    );
                  } else {
                    // check if password is valid
                    if (_passwordController.text.length < 6) {
                      CoolAlert.show(
                        context: context,
                        title: 'Oops',
                        text: 'Please enter a valid password',
                        type: CoolAlertType.error,
                        autoCloseDuration: Duration(seconds: 2),
                      );
                    }
                  }

                  if (EmailValidator.validate(_emailAddressController.text) &&
                      _passwordController.text.length >= 6 &&
                      _fullnameController.text.isNotEmpty) {
                    context.read<AuthCubit>().signUp(
                          fullname: _fullnameController.text,
                          email: _emailAddressController.text,
                          password: _passwordController.text,
                        );
                  }
                },
                child: Text(
                  'Sign Up',
                  style: whiteTextStyle.copyWith(
                    fontWeight: medium,
                  ),
                ),
              ),
            );
          }
        },
      );
    }

    Widget inputPassword() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 20,
          ),
          Text(
            'Password',
            style: greyTextStyle.copyWith(fontSize: 16),
          ),
          SizedBox(
            height: 8,
          ),
          TextFormField(
            obscureText: true,
            controller: _passwordController,
            onChanged: (value) {
              setState(() {});
            },
            cursorColor: kPurple,
            style: purpleTextStyle.copyWith(fontSize: 16),
            decoration: InputDecoration(
              contentPadding: EdgeInsets.only(left: 20, bottom: 12, top: 9),
              fillColor: kSoftGrey,
              filled: true,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(100),
                borderSide: BorderSide.none,
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(100),
                borderSide: BorderSide(
                    color: _passwordController.text.length > 0 ? kPurple : kRed,
                    width: 1),
              ),
            ),
          ),
        ],
      );
    }

    Widget inputEmail() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Email address',
            style: greyTextStyle.copyWith(fontSize: 16),
          ),
          SizedBox(
            height: 8,
          ),
          TextFormField(
            controller: _emailAddressController,
            onChanged: (value) {
              setState(() {});
            },
            cursorColor: kPurple,
            style: purpleTextStyle.copyWith(fontSize: 16),
            decoration: InputDecoration(
              contentPadding: EdgeInsets.only(left: 20, bottom: 12, top: 9),
              fillColor: kSoftGrey,
              filled: true,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(100),
                borderSide: BorderSide.none,
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(100),
                borderSide: BorderSide(
                    color: EmailValidator.validate(_emailAddressController.text)
                        ? kPurple
                        : Colors.red,
                    width: 1),
              ),
            ),
          ),
        ],
      );
    }

    Widget inputFullname() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Full Name',
            style: greyTextStyle.copyWith(fontSize: 16),
          ),
          SizedBox(
            height: 8,
          ),
          TextFormField(
            controller: _fullnameController,
            onChanged: (value) {
              setState(() {});
            },
            style: purpleTextStyle.copyWith(fontSize: 16),
            cursorColor: kPurple,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.only(left: 20, top: 9, bottom: 12),
              hintText: '',
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(100),
                borderSide: BorderSide.none,
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(100),
                borderSide: BorderSide(
                    color: _fullnameController.text.length > 0 ? kPurple : kRed,
                    width: 1),
              ),
              filled: true,
              fillColor: kSoftGrey,
            ),
          ),
          SizedBox(
            height: 20,
          ),
        ],
      );
    }

    Widget header() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 6,
          ),
          Text(
            'Sign Up',
            style: greyTextStyle.copyWith(fontSize: 16),
          ),
          SizedBox(
            height: 2,
          ),
          Text(
            'Create your account',
            style: blackTextStyle.copyWith(fontSize: 24, fontWeight: semibold),
          ),
          SizedBox(
            height: 50,
          )
        ],
      );
    }

    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.all(24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              header(),
              // inputImage(),
              inputFullname(),
              inputEmail(),
              inputPassword(),
              // inputGoal(),
              buttonSignUp(),
              // backLink(),
            ],
          ),
        ),
      ),
    );
  }

  void showError(String message) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(message),
      backgroundColor: Colors.red,
    ));
  }
}
