import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color kBlack = Color(0xff20242B);
Color kYellow = Color(0xffFFB800);
Color kGrey = Color(0xffAFAFAF);
Color kWhite = Color(0xffFFFFFF);
Color kPurple = Color(0xff4141A4);
Color kSoftGrey = Color(0xffF1F0F5);
Color kRed = Color(0xffE53935);

TextStyle blackTextStyle = GoogleFonts.poppins(color: kBlack);
TextStyle yellowTextStyle = GoogleFonts.poppins(color: kYellow);
TextStyle greyTextStyle = GoogleFonts.poppins(color: kGrey);
TextStyle whiteTextStyle = GoogleFonts.poppins(color: kWhite);
TextStyle purpleTextStyle = GoogleFonts.poppins(color: kPurple);

FontWeight semibold = FontWeight.w600;
FontWeight medium = FontWeight.w500;
FontWeight normal = FontWeight.normal;
FontWeight bold = FontWeight.bold;
