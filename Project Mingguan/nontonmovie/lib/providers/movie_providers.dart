import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:nontonmovie/models/movie_model.dart';

class MovieProviders with ChangeNotifier {
  Future<List<MovieModel>> getUpcomingMovie() async {
    try {
      var response = await http.get(Uri.parse(
          'https://api.themoviedb.org/3/movie/upcoming?api_key=640970a049607a3994b9abf76f809643&language=en-US&page=1'));

      print(response.statusCode);

      var ListMovie = json.decode(response.body)['results'] as List;
      print(ListMovie);
      return ListMovie.map((movie) => MovieModel.fromJson(movie)).toList();
    } catch (e) {
      print(e);
      return [];
    }
  }

  Future<List<MovieModel>> getPopularMovies() async {
    try {
      var response = await http.get(Uri.parse(
          'https://api.themoviedb.org/3/movie/popular?api_key=640970a049607a3994b9abf76f809643&language=en-US&page=1'));

      print(response.statusCode);

      var ListMovie = json.decode(response.body)['results'] as List;
      print(ListMovie);
      return ListMovie.map((movie) => MovieModel.fromJson(movie)).toList();
    } catch (e) {
      print(e);
      return [];
    }
  }

  Future<List<MovieModel>> getNowPlayingMovies() async {
    try {
      var response = await http.get(Uri.parse(
          'https://api.themoviedb.org/3/movie/now_playing?api_key=640970a049607a3994b9abf76f809643&language=en-US&page=1'));

      print(response.statusCode);

      var ListMovie = json.decode(response.body)['results'] as List;
      print(ListMovie);
      return ListMovie.map((movie) => MovieModel.fromJson(movie)).toList();
    } catch (e) {
      print(e);
      return [];
    }
  }

  Future<List<MovieModel>> getRelatedMovies(int id) async {
    try {
      var response = await http.get(Uri.parse(
          'https://api.themoviedb.org/3/movie/$id/similar?api_key=640970a049607a3994b9abf76f809643&language=en-US&page=1'));

      print(response.statusCode);

      var ListMovie = json.decode(response.body)['results'] as List;
      print(ListMovie);
      return ListMovie.map((movie) => MovieModel.fromJson(movie)).toList();
    } catch (e) {
      print(e);
      return [];
    }
  }
}
