import 'package:equatable/equatable.dart';

class UserModel extends Equatable {
  final String id;
  final String fullname;
  final String email;
  final String password;

  UserModel({
    required this.id,
    required this.fullname,
    required this.email,
    required this.password,
  });

  @override
  List<Object?> get props => [id, fullname, email, password];
}
