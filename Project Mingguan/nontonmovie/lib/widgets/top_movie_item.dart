import 'package:flutter/material.dart';
import 'package:nontonmovie/models/movie_model.dart';
import 'package:nontonmovie/pages/detail_movie_page.dart';
import 'package:nontonmovie/theme.dart';

class TopMovieItem extends StatelessWidget {
  final MovieModel movie;
  const TopMovieItem({Key? key, required this.movie}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => DetailMoviePage(movie)));
      },
      child: Container(
        margin: EdgeInsets.only(right: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: 120,
              height: 190,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                image: DecorationImage(
                  image: NetworkImage(
                    'https://image.tmdb.org/t/p/w185${movie.poster_path}',
                  ),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            // ellipsis text
            Container(
              width: 120,
              margin: EdgeInsets.only(top: 6),
              child: Text(
                movie.original_title,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: blackTextStyle.copyWith(
                  fontSize: 12,
                  fontWeight: bold,
                ),
              ),
            ),
            // rating
            Row(
              children: [
                Icon(
                  Icons.star,
                  color: Colors.yellowAccent,
                  size: 14,
                ),
                SizedBox(
                  width: 2,
                ),
                Text(
                  movie.vote_average.toString(),
                  style: greyTextStyle.copyWith(fontSize: 14),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
