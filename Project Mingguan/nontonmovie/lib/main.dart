import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nontonmovie/cubit/auth_cubit.dart';
import 'package:nontonmovie/pages/home_page.dart';
import 'package:nontonmovie/pages/sign_up_page.dart';
import 'package:nontonmovie/pages/splash_page.dart';
import 'package:nontonmovie/providers/movie_providers.dart';
import 'package:provider/provider.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => AuthCubit()),
      ],
      child: MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => MovieProviders()),
        ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          routes: {
            '/': (context) => SplashPage(),
            '/sign-up': (context) => SignUpPage(),
            '/home': (context) => HomePage(),
          },
        ),
      ),
    );
  }
}
