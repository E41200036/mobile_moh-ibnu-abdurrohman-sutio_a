import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:nontonmovie/models/user_model.dart';

class UserService {
  CollectionReference _collection =
      FirebaseFirestore.instance.collection('users');

  Future<void> createUser(UserModel user) async {
    try {
      _collection.doc(user.id).set({
        'id': user.id,
        'fullname': user.fullname,
        'email': user.email,
        'password': user.password,
      });
    } catch (e) {
      throw e;
    }
  }

  Future<UserModel> getCurrentUser(String id) async {
    try {
      DocumentSnapshot snapshot = await _collection.doc(id).get();
      return UserModel(
        id: snapshot['id'],
        fullname: snapshot['fullname'],
        email: snapshot['email'],
        password: snapshot['password'],
      );
    } catch (e) {
      throw e;
    }
  }
}
