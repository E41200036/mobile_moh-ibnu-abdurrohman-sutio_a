import 'package:firebase_auth/firebase_auth.dart';
import 'package:nontonmovie/models/user_model.dart';
import 'package:nontonmovie/services/user_service.dart';

class AuthService {
  FirebaseAuth _auth = FirebaseAuth.instance;

  Future<UserModel> signUp({
    required String fullname,
    required String email,
    required String password,
  }) async {
    try {
      UserCredential credential = await _auth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );

      UserModel user = UserModel(
        id: credential.user!.uid,
        fullname: fullname,
        email: email,
        password: password,
      );
      await UserService().createUser(user);
      return user;
    } catch (e) {
      throw e;
    }
  }
}
