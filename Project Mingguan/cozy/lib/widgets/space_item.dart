import 'package:cozy/models/space_model.dart';
import 'package:cozy/pages/detail_page.dart';
import 'package:cozy/theme.dart';
import 'package:flutter/material.dart';

class SpaceItem extends StatelessWidget {
  final SpaceModel space;
  SpaceItem(this.space);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => DetailPage(space)));
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 30),
        child: Row(
          children: [
            Container(
              width: 130,
              height: 110,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(18),
                image: DecorationImage(
                  image: NetworkImage(
                    space.image_url,
                  ),
                  fit: BoxFit.cover,
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Container(
                    width: 70,
                    height: 30,
                    decoration: BoxDecoration(
                      color: kPurple,
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(18),
                        bottomLeft: Radius.circular(18),
                      ),
                    ),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset(
                            'assets/icon_star.png',
                            width: 18,
                            height: 18,
                          ),
                          SizedBox(
                            width: 2,
                          ),
                          Text(
                            '${space.rating}/5',
                            style: whiteTextStyle.copyWith(
                                fontSize: 13, fontWeight: semibold),
                          ),
                        ]),
                  ),
                ],
              ),
            ),
            SizedBox(
              width: 20,
            ),
            Wrap(
              direction: Axis.vertical,
              children: [
                SizedBox(
                  width: 162,
                  child: Text(
                    space.name,
                    softWrap: true,
                    style: blackTextStyle.copyWith(
                      fontSize: 18,
                      fontWeight: semibold,
                    ),
                  ),
                ),
                SizedBox(
                  height: 2,
                ),
                Text.rich(
                  TextSpan(
                    children: [
                      TextSpan(
                        text: "\$" + space.price.toString(),
                        style: purpleTextStyle.copyWith(
                            fontSize: 16, fontWeight: semibold),
                      ),
                      TextSpan(
                        text: ' / month',
                        style: greyTextStyle.copyWith(
                            fontSize: 16, fontWeight: light),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 16),
                Text(
                  space.city + " " + space.country,
                  style: greyTextStyle.copyWith(fontWeight: light),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
