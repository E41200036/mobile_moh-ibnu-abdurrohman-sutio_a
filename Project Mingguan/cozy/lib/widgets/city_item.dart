import 'package:flutter/material.dart';

import '../theme.dart';

class CityItem extends StatefulWidget {
  final String cityImage, cityName;
  const CityItem({Key? key, required this.cityImage, required this.cityName})
      : super(key: key);

  @override
  State<CityItem> createState() => _CityItemState();
}

bool isFavorite = false;

class _CityItemState extends State<CityItem> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          isFavorite = !isFavorite;
        });
      },
      child: Container(
        width: 120,
        height: 150,
        margin: EdgeInsets.only(right: 20),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(18),
          color: Color(0xffF6F7F8),
        ),
        child: Column(children: [
          Stack(children: [
            Container(
              width: 120,
              height: 102,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.vertical(
                  top: Radius.circular(18),
                ),
                image: DecorationImage(
                  image: NetworkImage(widget.cityImage),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            isFavorite
                ? Align(
                    alignment: Alignment.topRight,
                    child: Container(
                      width: 50,
                      height: 30,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(18),
                          bottomLeft: Radius.circular(18),
                        ),
                        color: kPurple,
                      ),
                      child: Center(
                          child: Image.asset(
                        'assets/icon_star.png',
                        width: 16,
                        height: 16,
                      )),
                    ),
                  )
                : Container(),
          ]),
          SizedBox(
            height: 11,
          ),
          Text(
            widget.cityName,
            style: blackTextStyle.copyWith(fontSize: 16, fontWeight: semibold),
          ),
        ]),
      ),
    );
  }
}
