import 'package:cozy/theme.dart';
import 'package:flutter/material.dart';

class TGItem extends StatelessWidget {
  final String tgImage, tgName, tgLastUpdate;
  const TGItem(
      {Key? key,
      required this.tgImage,
      required this.tgName,
      required this.tgLastUpdate})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 20),
      child: Row(
        children: [
          Container(
            width: 80,
            height: 80,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(24),
              image: DecorationImage(
                image: AssetImage(tgImage),
                fit: BoxFit.cover,
              ),
            ),
          ),
          SizedBox(
            width: 16,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                tgName,
                style:
                    blackTextStyle.copyWith(fontSize: 18, fontWeight: semibold),
              ),
              SizedBox(
                height: 4,
              ),
              Text(
                'Updated $tgLastUpdate',
                style: greyTextStyle.copyWith(fontSize: 14, fontWeight: light),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
