import 'package:cozy/pages/calling_page.dart';
import 'package:cozy/pages/error_page.dart';
import 'package:cozy/pages/home_page.dart';
import 'package:cozy/pages/maps_page.dart';
import 'package:cozy/pages/splash_page.dart';
import 'package:cozy/providers/space_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => SpaceProvider()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        routes: {
          '/': (context) => SplashPage(),
          '/home': (context) => HomePage(),
          '/calling': (context) => CallingPage(),
          '/maps': (context) => MapsPage(),
          '/error': (context) => ErrorPage(),
        },
      ),
    );
  }
}
