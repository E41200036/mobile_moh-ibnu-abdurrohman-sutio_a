class SpaceModel {
  late String name, city, phone, country, image_url, address, map_url;
  late int id, price, rating;
  late List photos;
  late int number_of_kitchens, number_of_bedrooms, number_of_cupboards;

  SpaceModel(
      {required this.id,
      required this.name,
      required this.city,
      required this.country,
      required this.image_url,
      required this.address,
      required this.map_url,
      required this.price,
      required this.rating,
      required this.phone,
      required this.photos,
      required this.number_of_kitchens,
      required this.number_of_bedrooms,
      required this.number_of_cupboards});

  SpaceModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    city = json['city'];
    country = json['country'];
    image_url = json['image_url'];
    address = json['address'];
    map_url = json['map_url'];
    price = json['price'];
    rating = json['rating'];
    phone = json['phone'];
    photos = json['photos'];
    number_of_kitchens = json['number_of_kitchens'];
    number_of_bedrooms = json['number_of_bedrooms'];
    number_of_cupboards = json['number_of_cupboards'];
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'city': city,
      'country': country,
      'image_url': image_url,
      'address': address,
      'map_url': map_url,
      'price': price,
      'rating': rating,
      'phone': phone,
      'photos': photos,
      'number_of_kitchens': number_of_kitchens,
      'number_of_bedrooms': number_of_bedrooms,
      'number_of_cupboards': number_of_cupboards
    };
  }
}
