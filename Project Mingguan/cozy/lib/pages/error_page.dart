import 'package:cozy/theme.dart';
import 'package:flutter/material.dart';

class ErrorPage extends StatelessWidget {
  const ErrorPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(38),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                'assets/image_error.png',
                height: 86,
              ),
              SizedBox(
                height: 70,
              ),
              Text(
                'Where are you going?',
                style:
                    blackTextStyle.copyWith(fontSize: 24, fontWeight: semibold),
              ),
              Text(
                'Seems like the page that you were requested is already gone',
                style: greyTextStyle.copyWith(fontSize: 16, fontWeight: light),
              ),
              SizedBox(
                height: 50,
              ),
              Container(
                width: 210,
                height: 50,
                decoration: BoxDecoration(
                  color: kPurple,
                  borderRadius: BorderRadius.circular(17),
                ),
                child: TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text(
                      'Back to Home',
                      style: whiteTextStyle.copyWith(
                        fontSize: 18,
                        fontWeight: semibold,
                      ),
                    )),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
