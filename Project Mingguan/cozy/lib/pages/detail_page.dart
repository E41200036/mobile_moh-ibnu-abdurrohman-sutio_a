import 'dart:ffi';

import 'package:cozy/models/space_model.dart';
import 'package:cozy/theme.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class DetailPage extends StatefulWidget {
  final SpaceModel space;

  DetailPage(this.space);

  @override
  State<DetailPage> createState() => _DetailPageState();
}

bool isClicked = false;

class _DetailPageState extends State<DetailPage> {
  @override
  Widget build(BuildContext context) {
    _launchURL(String url) async {
      if (await canLaunch(url)) {
        await launch(url);
      } else {
        throw 'Could not launch $url';
      }
    }

    Future<void> _showDialog() {
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Konfirmasi'),
              content: Text('Apakah kamu yakin akan menelfon?'),
              actions: <Widget>[
                TextButton(
                  onPressed: () {},
                  child: Text('Cancel'),
                ),
                TextButton(
                    onPressed: () {
                      _launchURL('telp:${widget.space.phone}');
                    },
                    child: Text('Yes'))
              ],
            );
          });
    }

    Widget header() {
      return Container(
        margin: EdgeInsets.only(top: 30),
        child: Padding(
          padding: EdgeInsets.only(top: 30, left: 24, right: 24),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              GestureDetector(
                onTap: () => Navigator.pop(context),
                child: Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                    color: kWhite,
                    shape: BoxShape.circle,
                  ),
                  child: Center(
                    child: Icon(
                      Icons.arrow_back_outlined,
                      size: 20,
                      color: kBlack,
                    ),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  setState(() {
                    isClicked = !isClicked;
                  });
                },
                child: Container(
                  width: 40,
                  height: 40,
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: kWhite,
                    shape: BoxShape.circle,
                  ),
                  child: Image.asset(
                    isClicked
                        ? 'assets/icon_love_active.png'
                        : 'assets/icon_love_outline.png',
                    height: 20,
                    width: 20,
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    }

    Widget title() {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                widget.space.name,
                style: blackTextStyle.copyWith(
                  fontSize: 22,
                  fontWeight: semibold,
                ),
              ),
              SizedBox(
                height: 2,
              ),
              Text.rich(
                TextSpan(
                  children: [
                    TextSpan(
                      text: '\$' + widget.space.price.toString(),
                      style: purpleTextStyle.copyWith(
                        fontSize: 14,
                        fontWeight: semibold,
                      ),
                    ),
                    TextSpan(
                      text: ' / month',
                      style: greyTextStyle.copyWith(
                        fontSize: 16,
                        fontWeight: light,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          // rating star with looping
          Row(
            children: [
              for (int i = 0; i < widget.space.rating; i++)
                Image.asset(
                  'assets/icon_star.png',
                  width: 20,
                  height: 20,
                ),
              for (int i = widget.space.rating; i < 5; i++)
                Image.asset(
                  'assets/icon_star_inactive.png',
                  width: 20,
                  height: 20,
                ),
            ],
          ),
        ],
      );
    }

    Widget facilities_item(String name, String icon, int count) {
      return Container(
        margin: EdgeInsets.only(right: 35),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image.asset(
              icon,
              width: 32,
              height: 32,
            ),
            SizedBox(
              height: 8,
            ),
            Text.rich(TextSpan(
              children: [
                TextSpan(
                  text: count.toString(),
                  style: purpleTextStyle.copyWith(
                    fontSize: 14,
                    fontWeight: medium,
                  ),
                ),
                TextSpan(
                  text: ' $name',
                  style:
                      greyTextStyle.copyWith(fontSize: 14, fontWeight: light),
                )
              ],
            )),
          ],
        ),
      );
    }

    Widget facilities() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 30,
          ),
          Text(
            'Main Facilities',
            style: blackTextStyle.copyWith(fontSize: 16),
          ),
          SizedBox(
            height: 12,
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                facilities_item('kitchen', 'assets/icon_kitchen.png',
                    widget.space.number_of_kitchens),
                facilities_item('bedroom', 'assets/icon_bedroom.png',
                    widget.space.number_of_bedrooms),
                facilities_item('cupboard', 'assets/icon_cupboard.png',
                    widget.space.number_of_cupboards),
              ],
            ),
          ),
        ],
      );
    }

    Widget photo_items(String imageurl) {
      return Container(
        width: 110,
        margin: EdgeInsets.only(right: 18),
        height: 88,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          image:
              DecorationImage(image: NetworkImage(imageurl), fit: BoxFit.cover),
        ),
      );
    }

    Widget photos() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 30,
          ),
          Text(
            'Photos',
            style: blackTextStyle.copyWith(
              fontSize: 16,
            ),
          ),
          SizedBox(
            height: 12,
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: widget.space.photos.map((e) => photo_items(e)).toList(),
            ),
          ),
        ],
      );
    }

    Widget location() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 30),
          Text(
            'Location',
            style: blackTextStyle.copyWith(fontSize: 16),
          ),
          SizedBox(
            height: 6,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(widget.space.address, style: greyTextStyle),
                  SizedBox(
                    height: 2,
                  ),
                  Text(widget.space.city, style: greyTextStyle),
                ],
              ),
              SizedBox(
                width: 74,
              ),
              GestureDetector(
                onTap: () => _launchURL(widget.space.map_url),
                child: Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Color(0xffF6F7F8),
                  ),
                  child: Icon(
                    Icons.location_pin,
                    color: Color(0xff989BA1),
                    size: 20,
                  ),
                ),
              ),
            ],
          ),
        ],
      );
    }

    Widget buttonBookNow() {
      return Container(
        margin: EdgeInsets.only(top: 30),
        width: 327,
        height: 50,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(18),
          color: kPurple,
        ),
        child: TextButton(
            onPressed: () {
              _showDialog();
              // Navigator.pushNamedAndRemoveUntil(
              //     context, '/calling', (route) => false);
            },
            child: Text(
              'Book Now',
              style: whiteTextStyle.copyWith(
                fontSize: 18,
                fontWeight: semibold,
              ),
            )),
      );
    }

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                Image.network(
                  widget.space.image_url,
                  width: double.infinity,
                  height: 350,
                  fit: BoxFit.cover,
                ),
                header(),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    margin: EdgeInsets.only(top: 328),
                    width: double.infinity,
                    height: 400,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.vertical(
                        top: Radius.circular(24),
                      ),
                      color: kWhite,
                    ),
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: EdgeInsets.only(top: 30, left: 24, right: 24),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            title(),
                            facilities(),
                            photos(),
                            location(),
                            buttonBookNow(),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
