import 'package:cozy/models/space_model.dart';
import 'package:cozy/theme.dart';
import 'package:cozy/widgets/city_item.dart';
import 'package:cozy/widgets/space_item.dart';
import 'package:cozy/widgets/tg_item.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/space_provider.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var spaceProvider = Provider.of<SpaceProvider>(context);

    Widget header() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Explore Now',
            style: blackTextStyle.copyWith(fontSize: 24, fontWeight: semibold),
          ),
          SizedBox(
            height: 2,
          ),
          Text(
            'Mencari kosan yang cozy',
            style: greyTextStyle.copyWith(fontSize: 16, fontWeight: light),
          ),
        ],
      );
    }

    Widget popularCities() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 30,
          ),
          Text(
            'Popular Cities',
            style: blackTextStyle.copyWith(fontSize: 16),
          ),
          SizedBox(
            height: 16,
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: FutureBuilder<List<SpaceModel>>(
              future: spaceProvider.getAllSpace(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return Row(
                    children: snapshot.data!
                        .map((space) => CityItem(
                              cityImage: space.image_url,
                              cityName: space.city,
                            ))
                        .toList(),
                  );
                } else {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
              },
            ),
          )
        ],
      );
    }

    Widget recommendedSpace() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 30),
          Text(
            'Recommended Space',
            style: blackTextStyle.copyWith(fontSize: 16),
          ),
          SizedBox(height: 16),
          FutureBuilder<List<SpaceModel>>(
            future: spaceProvider.getAllSpace(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children:
                      snapshot.data!.map((space) => SpaceItem(space)).toList(),
                );
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            },
          ),
        ],
      );
    }

    Widget tipsAndGuidance() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Tips & Guides',
            style: blackTextStyle.copyWith(fontSize: 16),
          ),
          SizedBox(
            height: 16,
          ),
          GestureDetector(
            onTap: () {
              Navigator.pushNamed(context, '/error');
            },
            child: TGItem(
              tgImage: 'assets/icon_tg1.png',
              tgName: 'City Guidelines',
              tgLastUpdate: '20 April',
            ),
          ),
          TGItem(
            tgImage: 'assets/icon_tg2.png',
            tgName: 'Jakarta Fairship',
            tgLastUpdate: '11 Desc',
          ),
        ],
      );
    }

    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                header(),
                popularCities(),
                recommendedSpace(),
                tipsAndGuidance(),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: Container(
        width: double.infinity,
        height: 65,
        margin: EdgeInsets.only(left: 24, right: 24, bottom: 30),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(23),
          color: Color(0xffF6F7F8),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Image.asset(
              'assets/icon_home.png',
              width: 24,
              height: 24,
            ),
            Image.asset(
              'assets/icon_mail.png',
              width: 24,
              height: 24,
            ),
            Image.asset(
              'assets/icon_card.png',
              width: 24,
              height: 24,
            ),
            Image.asset(
              'assets/icon_love.png',
              width: 24,
              height: 24,
            ),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
