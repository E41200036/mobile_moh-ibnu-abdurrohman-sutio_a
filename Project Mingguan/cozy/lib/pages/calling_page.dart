import 'package:cozy/theme.dart';
import 'package:flutter/material.dart';

class CallingPage extends StatelessWidget {
  const CallingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 150,
              height: 150,
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  color: kPurple,
                  width: 1.5,
                ),
              ),
              child: Container(
                width: 130,
                height: 130,
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image: AssetImage('assets/image_user.png'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 65,
            ),
            Text(
              'Amanda Shayna',
              style:
                  blackTextStyle.copyWith(fontSize: 20, fontWeight: semibold),
            ),
            SizedBox(
              height: 6,
            ),
            Text(
              '12 : 30 minutes',
              style: greyTextStyle.copyWith(fontSize: 16, fontWeight: light),
            ),
            Container(
              width: 60,
              margin: EdgeInsets.only(top: 60),
              height: 60,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: kRed,
              ),
              child: TextButton(
                onPressed: () {
                  Navigator.pushNamed(context, '/home');
                },
                child: Icon(
                  Icons.close,
                  color: Colors.white,
                  size: 28,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
