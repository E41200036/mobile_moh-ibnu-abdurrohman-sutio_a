import 'dart:convert';

import 'package:cozy/models/space_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class SpaceProvider with ChangeNotifier {
  Future<List<SpaceModel>> getAllSpace() async {
    try {
      var response = await http
          .get(Uri.parse('https://bwa-cozy.herokuapp.com/recommended-spaces'));
      print(response.statusCode);
      print(response.body);
      if (response.statusCode == 200) {
        List<SpaceModel> spaces = [];
        List data = json.decode(response.body);
        data.forEach((element) {
          spaces.add(SpaceModel.fromJson(element));
        });
        return spaces;
      } else {
        return [];
      }
    } catch (e) {
      print(e);
      return [];
    }
  }
}
