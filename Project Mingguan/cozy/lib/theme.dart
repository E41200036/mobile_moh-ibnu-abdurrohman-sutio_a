import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color kWhite = Colors.white;
Color kBlack = Colors.black;
Color kGrey = Color(0xff82868E);
Color kPurple = Color(0xff5843BE);
Color kOrange = Color(0xffFF9376);
Color kDarkGrey = Color(0xff989BA1);
Color kRed = Color(0xffFF5B5B);

TextStyle whiteTextStyle = GoogleFonts.poppins(color: kWhite);

TextStyle greyTextStyle = GoogleFonts.poppins(color: kGrey);

TextStyle blackTextStyle = GoogleFonts.poppins(color: kBlack);
TextStyle purpleTextStyle = GoogleFonts.poppins(color: kPurple);

FontWeight light = FontWeight.w300;
FontWeight normal = FontWeight.w400;
FontWeight medium = FontWeight.w500;
FontWeight semibold = FontWeight.w600;
