import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class GetStarted1 extends StatelessWidget {
  const GetStarted1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Image.asset('assets/image_get_started1.png',
              width: double.infinity, fit: BoxFit.cover),
          Container(
            margin: EdgeInsets.only(top: 527),
            child: Center(
              child: Column(
                children: [
                  Text(
                    'Maximize Revenue',
                    style: GoogleFonts.poppins(
                      color: Colors.white,
                      fontSize: 24,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Text(
                    'Gain more profit from cryptocurrency\nwithout any risk involved',
                    textAlign: TextAlign.center,
                    style:
                        GoogleFonts.poppins(color: Colors.white, fontSize: 16),
                  ),
                  SizedBox(
                    height: 50,
                  ),
                  Container(
                    width: 80,
                    height: 80,
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Color.fromARGB(255, 136, 78, 229),
                    ),
                    child: Align(
                        child: Image.asset(
                      'assets/icon_login.png',
                      width: 30,
                      height: 30,
                    )),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
