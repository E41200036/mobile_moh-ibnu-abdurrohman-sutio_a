import 'package:flutter/material.dart';
import 'package:seven_days/mingguan/minggu%208/model/product_model.dart';
import 'package:get_storage/get_storage.dart';
import 'package:get/get.dart';

class DemoController extends GetxController {
  var cartItems = <ProductModel>[].obs;
  int get cartCount => cartItems.length;
  double get totalAmount => cartItems.fold(0, (a, b) => a + b.price);
  addToCart(ProductModel product) {
    cartItems.add(product);
  }

  final storage = GetStorage();
  bool get isDark => storage.read('isDark') ?? false;
  ThemeData get theme => isDark ? ThemeData.dark() : ThemeData.light();
  void changeTheme(bool val) {
    storage.write('isDark', val);
    update();
  }
}
