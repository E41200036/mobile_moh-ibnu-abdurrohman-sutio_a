import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:seven_days/mingguan/minggu%208/controller/demo_controller.dart';
import 'package:seven_days/mingguan/minggu%208/view/demo_page.dart';
import 'package:seven_days/mingguan/minggu%208/view/home_page.dart';

void main() async {
  await GetStorage.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final DemoController controller = Get.put(DemoController());
  MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SimpleBuilder(builder: (context) {
      return GetMaterialApp(
        title: 'Flutter Demo',
        theme: controller.theme,
        routes: {
          '/': (context) => HomePage(),
          '/cart': (context) => DemoPage(),
        },
      );
    });
  }
}



// import 'package:flutter/material.dart';
// import 'package:seven_days/mingguan/minggu%207/form.dart';

// void main() {
//   runApp(new MaterialApp(
//     // home: new Home(),
//     home: BelajarForm(),
//   ));
// }

// class Home extends StatefulWidget {
//   @override
//   _HomeState createState() => new _HomeState();
// }

// class _HomeState extends State<Home> {
//   List<String> agama = [
//     "Islam",
//     "Kristen Protestan",
//     "Kristen Katolik",
//     "Hindu",
//     "Budha",
//     "Konghucu"
//   ];

//   String religion = "Islam";

//   String gender = "";

//   TextEditingController controllerNama = new TextEditingController();
//   TextEditingController controllerPass = new TextEditingController();
//   TextEditingController controllerMoto = new TextEditingController();

//   void chooseGender(String value) {
//     setState(() {
//       gender = value;
//     });
//   }

//   void chooseReligion(String value) {
//     setState(() {
//       religion = value;
//     });
//   }

//   void kirimdata() {
//     AlertDialog alertDialog = new AlertDialog(
//       content: new Container(
//         height: 200.0,
//         child: new Column(
//           children: <Widget>[
//             new Text("Nama Lengkap : ${controllerNama.text}"),
//             new Text("Password : ${controllerPass.text}"),
//             new Text("Moto Hidup : ${controllerMoto.text}"),
//             new Text("Jenis Kelamin : ${gender}"),
//             new Text("Agama : ${religion}"),
//             // ignore: deprecated_member_use
//             new RaisedButton(
//               child: new Text("OK"),
//               onPressed: () => Navigator.pop(context),
//               color: Colors.teal,
//             )
//           ],
//         ),
//       ),
//     );
//     showDialog(context: context, builder: (_) => alertDialog);
//   }

//   @override
//   Widget build(BuildContext context) {
//     return new Scaffold(
//       appBar: new AppBar(
//         leading: new Icon(Icons.list),
//         title: new Text("Data diri"),
//         backgroundColor: Colors.teal,
//       ),
//       body: new ListView(
//         children: [
//           new Container(
//             padding: new EdgeInsets.all(10.0),
//             child: new Column(
//               children: <Widget>[
//                 new TextFormField(
//                   controller: controllerNama,
//                   decoration: new InputDecoration(
//                     hintText: "Nama Lengkap",
//                     labelText: "Nama Lengkap",
//                     border: new OutlineInputBorder(
//                       borderRadius: new BorderRadius.circular(20.0),
//                     ),
//                   ),
//                   validator: (value) {
//                     if (value!.isEmpty) {
//                       return 'Nama Tidak Boleh Kosong';
//                     }
//                     return null;
//                   },
//                 ),
//                 new Padding(
//                   padding: new EdgeInsets.only(top: 20.0),
//                 ),
//                 new TextFormField(
//                   controller: controllerPass,
//                   obscureText: true,
//                   decoration: new InputDecoration(
//                     hintText: "Password",
//                     labelText: "Password",
//                     border: new OutlineInputBorder(
//                       borderRadius: new BorderRadius.circular(20.0),
//                     ),
//                   ),
//                   validator: (value) {
//                     if (value!.isEmpty) {
//                       return 'Password Tidak Boleh Kosong';
//                     }
//                     return null;
//                   },
//                 ),
//                 new Padding(
//                   padding: new EdgeInsets.only(top: 20.0),
//                 ),
//                 new TextFormField(
//                   controller: controllerMoto,
//                   decoration: new InputDecoration(
//                     hintText: "Moto Hidup",
//                     labelText: "Moto Hidup",
//                     border: new OutlineInputBorder(
//                       borderRadius: new BorderRadius.circular(20.0),
//                     ),
//                   ),
//                   validator: (value) {
//                     if (value!.isEmpty) {
//                       return 'Motto Tidak Boleh Kosong';
//                     }
//                     return null;
//                   },
//                 ),
//                 new Padding(
//                   padding: new EdgeInsets.only(top: 20.0),
//                 ),
//                 new RadioListTile(
//                   value: "laki-laki",
//                   title: new Text("Laki-laki"),
//                   groupValue: gender,
//                   onChanged: (String? value) {
//                     chooseGender(value!);
//                   },
//                   activeColor: Colors.blue,
//                   subtitle: new Text("Pilih ini jika anda laki-laki"),
//                 ),
//                 new RadioListTile(
//                   value: "perempuan",
//                   title: new Text("Perempuan"),
//                   groupValue: gender,
//                   onChanged: (String? value) {
//                     chooseGender(value!);
//                   },
//                   activeColor: Colors.pink,
//                   subtitle: new Text("Pilih ini jika anda perempuan"),
//                 ),
//                 new Padding(
//                   padding: new EdgeInsets.only(top: 20.0),
//                 ),
//                 new Row(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: <Widget>[
//                     new Text(
//                       "Agama",
//                       style: new TextStyle(fontSize: 18.0, color: Colors.blue),
//                     ),
//                     new Padding(
//                       padding: new EdgeInsets.only(left: 15.0),
//                     ),
//                     DropdownButton(
//                       onChanged: (String? value) {
//                         chooseReligion(value!);
//                       },
//                       value: religion,
//                       items: agama.map((String value) {
//                         return new DropdownMenuItem(
//                           value: value,
//                           child: new Text(value),
//                         );
//                       }).toList(),
//                     )
//                   ],
//                 ),
//                 new RaisedButton(
//                     child: new Text("OK"),
//                     color: Colors.blue,
//                     onPressed: () {
//                       kirimdata();
//                     })
//               ],
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
