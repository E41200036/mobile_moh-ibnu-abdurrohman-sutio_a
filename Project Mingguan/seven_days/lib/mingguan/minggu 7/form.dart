import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: 'Belajar Form Flutter',
    home: BelajarForm(),
  ));
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  double nilaiSlider = 1;
  bool valueCheckBox = false;
  bool nilaiSwitch = true;
  final _formKey = GlobalKey<FormState>();

  TextEditingController namaLengkap = TextEditingController();
  TextEditingController password = TextEditingController();

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Flutter Form'),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                      controller: namaLengkap,
                      decoration: new InputDecoration(
                        hintText: 'Masukkan Nama Lengkap Anda',
                        labelText: 'Nama Lengkap',
                        icon: Icon(Icons.people),
                        border: OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5.0)),
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Nama Tidak Boleh Kosong';
                        }
                        return null;
                      }),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: password,
                    obscureText: true,
                    decoration: new InputDecoration(
                      labelText: "Password",
                      icon: Icon(Icons.security),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Password Tidak Boleh Kosong';
                      }
                      return null;
                    },
                  ),
                ),
                CheckboxListTile(
                  title: Text('Type Form Field'),
                  subtitle: Text('Checkbox, Radio, Switch'),
                  value: valueCheckBox,
                  activeColor: Colors.deepPurpleAccent,
                  onChanged: (value) {
                    setState(() {
                      valueCheckBox = value!;
                    });
                  },
                ),
                SwitchListTile(
                  title: Text('Type Submit Button'),
                  subtitle: Text('FlatButton, RaisedButton, OutlineButton'),
                  value: nilaiSwitch,
                  activeTrackColor: Color.fromARGB(255, 190, 190, 190),
                  onChanged: (value) {
                    setState(() {
                      nilaiSwitch = value;
                    });
                  },
                ),
                Slider(
                  value: nilaiSlider,
                  min: 0,
                  max: 100,
                  onChanged: (value) {
                    setState(() {
                      nilaiSlider = value;
                    });
                  },
                ),
                Container(
                  width: 200,
                  margin: EdgeInsets.only(top: 40),
                  height: 50,
                  decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: TextButton(
                    child: Text(
                      "Submit",
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        showDialog(
                            context: context,
                            builder: (_) => AlertDialog(
                                  title: Text('Hasil'),
                                  content: Text(
                                      'Nama Lengkap : ${namaLengkap.text}\nPassword : ${password.text}'),
                                ));
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
