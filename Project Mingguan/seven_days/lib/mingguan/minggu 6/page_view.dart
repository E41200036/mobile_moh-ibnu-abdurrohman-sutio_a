import 'package:flutter/material.dart';
import 'package:seven_days/mingguan/minggu%206/dropdown.dart';

class PageVieww extends StatelessWidget {
  const PageVieww({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    PageController controller = PageController();
    return Scaffold(
      body: ListView.builder(
        padding: EdgeInsets.symmetric(horizontal: 20),
        controller: controller,
        scrollDirection: Axis.horizontal,
        itemCount: 3,
        itemBuilder: (context, index) {
          return Align(
            alignment: Alignment.center,
            child: Container(
              width: 200,
              height: 78,
              margin: EdgeInsets.only(right: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [
                    Color(0xFFF8BBD0),
                    Color(0xFFF48FB1),
                  ],
                ),
              ),
              child: TextButton(
                child: Text(
                  index == 0
                      ? 'Login'
                      : index == 1
                          ? 'Register'
                          : 'Forgot Password',
                  style: TextStyle(color: Colors.white, fontSize: 18),
                ),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => DropDownn()));
                },
              ),
            ),
          );
        },
      ),
    );
  }
}
