import 'package:flutter/material.dart';

class Gradientt extends StatelessWidget {
  const Gradientt({Key? key}) : super(key: key);

  Widget buttonLogin() {
    return Container(
      width: double.infinity,
      height: 57,
      margin: EdgeInsets.only(top: 30),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Color(0xFFF8BBD0),
            Color(0xFFF48FB1),
          ],
        ),
      ),
      child: TextButton(
        onPressed: () {},
        child: Text(
          'Login',
          style: TextStyle(
            fontSize: 18,
            color: Colors.white,
          ),
        ),
      ),
    );
  }

  Widget emailInput() {
    return Container(
      width: double.infinity,
      height: 57,
      margin: EdgeInsets.only(top: 15),
      padding: EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        border: Border.all(width: 1, color: Color.fromARGB(145, 255, 182, 182)),
      ),
      child: Center(
        child: Expanded(
          child: TextFormField(
            decoration: InputDecoration.collapsed(
              hintText: 'Email',
              hintStyle: TextStyle(
                  fontSize: 18, color: Color.fromARGB(145, 255, 182, 182)),
            ),
          ),
        ),
      ),
    );
  }

  Widget passwordInput() {
    return Container(
      width: double.infinity,
      height: 57,
      margin: EdgeInsets.only(top: 15),
      padding: EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        border: Border.all(width: 1, color: Color.fromARGB(145, 255, 182, 182)),
      ),
      child: Center(
        child: Expanded(
          child: TextFormField(
            obscureText: true,
            decoration: InputDecoration.collapsed(
              hintText: 'Password',
              hintStyle: TextStyle(
                  fontSize: 18, color: Color.fromARGB(145, 255, 182, 182)),
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 30),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Silahkan Login',
                style: TextStyle(
                    fontSize: 24, color: Color.fromARGB(255, 255, 90, 186)),
              ),
              SizedBox(
                height: 30,
              ),
              emailInput(),
              passwordInput(),
              buttonLogin(),
            ],
          ),
        ),
      ),
    );
  }
}
