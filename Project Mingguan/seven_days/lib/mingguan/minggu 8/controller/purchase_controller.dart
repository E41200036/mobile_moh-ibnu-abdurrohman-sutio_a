import 'package:get/get.dart';
import 'package:seven_days/mingguan/minggu%208/model/product_model.dart';

class PurchaseController extends GetxController {
  var products = <ProductModel>[].obs;

  @override
  void onInit() {
    fetchProducts();
    super.onInit();
  }

  void fetchProducts() async {
    await Future.delayed(Duration(seconds: 2));
    var responseServer = [
      ProductModel(
        1,
        'Rinso',
        'https://assets.klikindomaret.com/products/20104003/20104003_2.jpg',
        'Mulai mesin cuci tipe top load atau bukaan atas, dan tipe front load atau bukaan depan. Untuk jenis top load, Rinso Matic bisa digunakan untuk mesin cuci bukaan atas jenis dua tabung ataupun satu tabung',
        60000,
      ),
      ProductModel(
        2,
        'Molto',
        'https://p-id.ipricegroup.com/uploaded_ac2c7dc5b44e205b10e2a6760a856919.jpg',
        'Mulai mesin cuci tipe top load atau bukaan atas, dan tipe front load atau bukaan depan. Untuk jenis top load, Rinso Matic bisa digunakan untuk mesin cuci bukaan atas jenis dua tabung ataupun satu tabung',
        50000,
      ),
      ProductModel(
        3,
        'Pensil 2B',
        'https://lzd-img-global.slatic.net/g/p/5e9b7416527beba7b2cdb9b9e504399b.jpg_720x720q80.jpg_.webp',
        'Mulai mesin cuci tipe top load atau bukaan atas, dan tipe front load atau bukaan depan. Untuk jenis top load, Rinso Matic bisa digunakan untuk mesin cuci bukaan atas jenis dua tabung ataupun satu tabung',
        40000,
      ),
    ];
    products.value = responseServer;
  }
}
