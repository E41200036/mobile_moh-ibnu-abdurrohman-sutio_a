class ProductModel {
  final int id;
  final String product_name;
  final String product_image;
  final String product_description;
  final double price;

  ProductModel(this.id, this.product_name, this.product_image,
      this.product_description, this.price);
}
