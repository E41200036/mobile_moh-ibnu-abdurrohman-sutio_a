import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:seven_days/mingguan/minggu%208/controller/demo_controller.dart';
import 'package:seven_days/mingguan/minggu%208/controller/purchase_controller.dart';

// ignore: must_be_immutable
class HomePage extends StatelessWidget {
  final PurchaseController purchaseController = Get.put(PurchaseController());
  DemoController demoController = Get.find();
  HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home Page'),
      ),
      bottomSheet: SafeArea(
          child: Card(
        elevation: 12,
        margin: EdgeInsets.zero,
        child: Container(
          height: 85,
          decoration: BoxDecoration(
            color: Colors.green,
            borderRadius: BorderRadius.circular(12),
          ),
          child: Padding(
              padding: EdgeInsets.all(8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Stack(children: [
                    Icon(
                      Icons.shopping_cart_checkout_outlined,
                      size: 40,
                      color: Colors.white,
                    ),
                    Positioned(
                      right: 5,
                      child: Container(
                        height: 20,
                        width: 20,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Center(
                          child: GetX<DemoController>(
                            builder: ((controller) => Text(
                                  '${controller.cartCount}',
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.black,
                                  ),
                                )),
                          ),
                        ),
                      ),
                    )
                  ]),
                  GetX<DemoController>(
                    builder: ((controller) => Text(
                          'Total: ${controller.totalAmount}',
                          style: TextStyle(
                            fontSize: 12,
                            color: Colors.white,
                          ),
                        )),
                  ),
                  IconButton(
                    onPressed: () => Get.toNamed('/cart'),
                    icon: Icon(
                      Icons.arrow_forward_ios_outlined,
                      color: Colors.white,
                      size: 20,
                    ),
                  ),
                ],
              )),
        ),
      )),
      body: Container(
        child: GetX<PurchaseController>(
          builder: (controller) {
            return ListView.builder(
              itemCount: controller.products.length,
              itemBuilder: (context, index) {
                return Padding(
                  padding: EdgeInsets.all(8),
                  child: Card(
                    elevation: 12,
                    margin: EdgeInsets.zero,
                    child: Container(
                      height: 100,
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Image.network(
                              controller.products[index].product_image,
                              height: 80,
                              width: 80,
                            ),
                            Text(
                              controller.products[index].product_name,
                              style: TextStyle(
                                fontSize: 20,
                                color: Colors.white,
                              ),
                            ),
                            IconButton(
                              onPressed: () => demoController
                                  .addToCart(controller.products[index]),
                              icon: Icon(
                                Icons.add_shopping_cart_outlined,
                                color: Colors.white,
                                size: 20,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
            );
          },
        ),
      ),
    );
  }
}
