import 'package:flutter/material.dart';
import 'package:seven_days/mingguan/minggu%205/widget/menu_item.dart';

class AboutPage extends StatelessWidget {
  final String title;
  const AboutPage({Key? key, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Container(
        width: MediaQuery.of(context).size.width - 60,
        child: Drawer(
          child: ListView(
            children: [
              Container(
                width: double.infinity,
                height: 190,
                padding: EdgeInsets.only(left: 20, top: 20, right: 20),
                decoration: BoxDecoration(
                  color: Colors.blue,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                            width: 70,
                            height: 70,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  image: AssetImage('assets/image_photo1.png'),
                                  fit: BoxFit.cover),
                            )),
                        Row(
                          children: [
                            Container(
                                width: 40,
                                height: 40,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                      image:
                                          AssetImage('assets/image_photo2.png'),
                                      fit: BoxFit.cover),
                                )),
                            SizedBox(
                              width: 10,
                            ),
                            Container(
                                width: 40,
                                height: 40,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                      image:
                                          AssetImage('assets/image_photo3.png'),
                                      fit: BoxFit.cover),
                                )),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      'Ibnu Ganteng',
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w700,
                          color: Colors.white),
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    Text(
                      'ibnu@gmail.com',
                      style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          color: Color.fromARGB(127, 214, 214, 214)),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    MenuItem(title: 'Home', icon: Icons.home),
                    MenuItem(title: 'Notification', icon: Icons.notifications),
                    MenuItem(title: 'Call', icon: Icons.call),
                    MenuItem(title: 'Sign out', icon: Icons.logout),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      appBar: AppBar(
        backgroundColor: Colors.blue,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.navigate_before,
            size: 30,
          ),
        ),
        actions: [
          Container(
              margin: EdgeInsets.only(right: 10), child: Icon(Icons.check_box)),
        ],
        title: Text(title),
        centerTitle: true,
      ),
      body: Center(
        child: Text(
          'About Page',
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }
}
