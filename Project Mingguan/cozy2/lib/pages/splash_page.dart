import 'package:cozy2/theme.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget header() {
      return Container(
        margin: EdgeInsets.only(top: 50),
        padding: EdgeInsets.symmetric(horizontal: 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: 50,
              height: 50,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                  image: AssetImage('assets/icon_logo.png'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Text(
              'Find Cozy House\nto Stay and Happy',
              style: blackTextStyle.copyWith(
                fontSize: 24,
                fontWeight: medium,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              'Stop membuang banyak waktu\npada tempat yang tidak habitabl',
              style: greyTextStyle.copyWith(
                fontSize: 16,
                fontWeight: light,
              ),
            ),
            SizedBox(height: 40),
            Container(
              width: 210,
              height: 50,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(17),
                color: kPurple,
              ),
              child: TextButton(
                onPressed: () {
                  Navigator.pushNamedAndRemoveUntil(
                      context, '/main', (route) => false);
                },
                child: Text(
                  'Explore Now',
                  style: whiteTextStyle.copyWith(
                    fontSize: 18,
                    fontWeight: medium,
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    }

    Widget background() {
      return Container(
        width: double.infinity,
        height: 443,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/image_splash.png'),
            fit: BoxFit.cover,
          ),
        ),
      );
    }

    return Scaffold(
      backgroundColor: kWhite,
      body: ListView(
        children: [
          header(),
          background(),
        ],
      ),
    );
  }
}
