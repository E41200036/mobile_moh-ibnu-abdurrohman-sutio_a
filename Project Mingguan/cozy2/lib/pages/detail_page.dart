import 'package:cozy2/theme.dart';
import 'package:cozy2/widget/photo_item.dart';
import 'package:flutter/material.dart';

class DetailPage extends StatelessWidget {
  final String imgUrl, placeName, city, country;
  final int price, rating;
  const DetailPage(
      {Key? key,
      required this.imgUrl,
      required this.placeName,
      required this.city,
      required this.country,
      required this.price,
      required this.rating})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhite,
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: 350,
            decoration: BoxDecoration(
              image:
                  DecorationImage(image: AssetImage(imgUrl), fit: BoxFit.cover),
            ),
            child: Align(
              alignment: Alignment.topCenter,
              child: Container(
                margin: EdgeInsets.only(top: 30, left: 24, right: 24),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Container(
                          width: 40,
                          height: 40,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle, color: kWhite),
                          child: Center(
                              child: Icon(Icons.chevron_left,
                                  color: kBlack, size: 20)),
                        ),
                      ),
                      Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: kWhite),
                        child: Center(
                            child: Icon(Icons.favorite_border,
                                color: kBlack, size: 20)),
                      ),
                    ]),
              ),
            ),
          ),
          Container(
            width: double.infinity,
            margin: EdgeInsets.only(top: 328),
            padding: EdgeInsets.symmetric(horizontal: 24),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.vertical(
                top: Radius.circular(30),
              ),
              color: kWhite,
            ),
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 30,
                  ),
                  Row(
                    children: [
                      Expanded(
                          child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Kuretakeso Hott',
                            style: blackTextStyle.copyWith(
                              fontSize: 22,
                              fontWeight: medium,
                            ),
                          ),
                          SizedBox(
                            height: 2,
                          ),
                          Text.rich(TextSpan(children: [
                            TextSpan(
                                text: '\$52',
                                style: purpleTextStyle.copyWith(
                                  fontSize: 16,
                                  fontWeight: medium,
                                )),
                            TextSpan(
                                text: ' / month',
                                style: greyTextStyle.copyWith(
                                  fontSize: 16,
                                )),
                          ]))
                        ],
                      )),
                      Row(
                        children: [
                          for (int i = 0; i < rating; i++)
                            Icon(
                              Icons.star,
                              color: kOrange,
                              size: 22,
                            ),
                          for (int i = 0; i < 5 - rating; i++)
                            Icon(
                              Icons.star,
                              color: kGrey,
                              size: 22,
                            ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Text(
                    'Main Facilities',
                    style: blackTextStyle.copyWith(fontSize: 16),
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Image.asset(
                            'assets/icon_kitchen.png',
                            width: 32,
                            height: 32,
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          Text.rich(TextSpan(children: [
                            TextSpan(
                              text: '2 ',
                              style: purpleTextStyle.copyWith(
                                fontSize: 14,
                                fontWeight: medium,
                              ),
                            ),
                            TextSpan(
                              text: 'Kitchen',
                              style: greyTextStyle.copyWith(
                                fontSize: 14,
                              ),
                            ),
                          ]))
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Image.asset(
                            'assets/icon_bedroom.png',
                            width: 32,
                            height: 32,
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          Text.rich(TextSpan(children: [
                            TextSpan(
                              text: '3 ',
                              style: purpleTextStyle.copyWith(
                                fontSize: 14,
                                fontWeight: medium,
                              ),
                            ),
                            TextSpan(
                              text: 'Beedroom',
                              style: greyTextStyle.copyWith(
                                fontSize: 14,
                              ),
                            ),
                          ]))
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Image.asset(
                            'assets/icon_cupboard.png',
                            width: 32,
                            height: 32,
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          Text.rich(TextSpan(children: [
                            TextSpan(
                              text: '3 ',
                              style: purpleTextStyle.copyWith(
                                fontSize: 14,
                                fontWeight: medium,
                              ),
                            ),
                            TextSpan(
                              text: 'Cupboard',
                              style: greyTextStyle.copyWith(
                                fontSize: 14,
                              ),
                            ),
                          ]))
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Text(
                    'Photos',
                    style: blackTextStyle.copyWith(fontSize: 16),
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: [
                        PhotoItem(img: 'assets/image_photo1.png'),
                        PhotoItem(img: 'assets/image_photo2.png'),
                        PhotoItem(img: 'assets/image_photo3.png'),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Location',
                              style: blackTextStyle.copyWith(fontSize: 16),
                            ),
                            SizedBox(
                              height: 6,
                            ),
                            Text(
                              'Jln. Kappan Sukses No. 20 Palembang',
                              style: greyTextStyle,
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: 40,
                        height: 40,
                        margin: EdgeInsets.only(bottom: 40),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: kSoftGrey,
                        ),
                        child: Center(
                          child: Icon(
                            Icons.navigation,
                            size: 18,
                            color: kGrey,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    width: double.infinity,
                    height: 50,
                    margin: EdgeInsets.only(bottom: 40),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(17),
                      color: kPurple,
                    ),
                    child: TextButton(
                      onPressed: () {},
                      child: Text(
                        'Book Now',
                        style: whiteTextStyle.copyWith(
                          fontSize: 18,
                          fontWeight: medium,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
