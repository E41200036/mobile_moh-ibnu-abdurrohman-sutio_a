import 'package:cozy2/widget/city_item.dart';
import 'package:cozy2/widget/space_item.dart';
import 'package:cozy2/widget/tips_and_guide_item.dart';
import 'package:flutter/material.dart';

import '../theme.dart';

class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget header() {
      return Container(
        margin: EdgeInsets.only(top: 20, bottom: 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Explore Now',
              style: blackTextStyle.copyWith(
                fontSize: 24,
                fontWeight: medium,
              ),
            ),
            SizedBox(
              height: 2,
            ),
            Text(
              'Mencari kosan yang cozy',
              style: greyTextStyle.copyWith(
                fontSize: 16,
                fontWeight: light,
              ),
            ),
          ],
        ),
      );
    }

    Widget popularCities() {
      return Container(
        margin: EdgeInsets.only(bottom: 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Popular Cities',
              style: blackTextStyle.copyWith(
                fontSize: 16,
                fontWeight: medium,
              ),
            ),
            SizedBox(
              height: 16,
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  CityItem(
                    img: 'assets/image_city1.png',
                    name: 'Jakarta',
                    isFavorite: false,
                  ),
                  CityItem(
                    img: 'assets/image_city2.png',
                    name: 'Bandung',
                    isFavorite: true,
                  ),
                  CityItem(
                    img: 'assets/image_city3.png',
                    name: 'Surabaya',
                    isFavorite: false,
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    }

    Widget recommendedSpace() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Recommended Space',
            style: blackTextStyle.copyWith(
              fontSize: 16,
              fontWeight: medium,
            ),
          ),
          SizedBox(
            height: 16,
          ),
          SpaceItem(
            img: 'assets/image_space2.png',
            title: 'Roemah Nenek',
            city: 'Seattle',
            country: 'Bogor',
            rating: 5,
            price: 11,
          ),
          SpaceItem(
            img: 'assets/image_space3.png',
            title: 'Darrling How',
            city: 'Jakarta',
            country: 'Indonesia',
            rating: 3,
            price: 40,
          ),
          SpaceItem(
            img: 'assets/image_space1.png',
            title: 'Kuretakeso Hott',
            city: 'Bandung',
            country: 'Germany',
            rating: 4,
            price: 52,
          ),
        ],
      );
    }

    Widget tipsAndGuidance() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Tips & Guidance',
            style: blackTextStyle.copyWith(
              fontSize: 16,
            ),
          ),
          SizedBox(
            height: 16,
          ),
          TipsAndGuidanceItem(
            icon: 'assets/icon_tg1.png',
            title: 'City Guidelines',
            date: '20 Apr',
          ),
          SizedBox(
            height: 20,
          ),
          TipsAndGuidanceItem(
            icon: 'assets/icon_tg2.png',
            title: 'Jakarta Fairship',
            date: '11 Dec',
          ),
        ],
      );
    }

    return Scaffold(
      backgroundColor: kWhite,
      body: ListView(padding: EdgeInsets.all(24), children: [
        header(),
        popularCities(),
        recommendedSpace(),
        tipsAndGuidance(),
      ]),
    );
  }
}
