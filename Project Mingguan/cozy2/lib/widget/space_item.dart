import 'package:cozy2/pages/detail_page.dart';
import 'package:flutter/material.dart';

import '../theme.dart';

class SpaceItem extends StatelessWidget {
  final String img, title, city, country;
  final int rating, price;
  const SpaceItem(
      {Key? key,
      required this.img,
      required this.title,
      required this.city,
      required this.country,
      required this.rating,
      required this.price})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => DetailPage(
                    imgUrl: img,
                    placeName: title,
                    city: city,
                    country: country,
                    price: price,
                    rating: rating)));
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 40),
        child: Row(
          children: [
            Container(
              width: 130,
              height: 110,
              margin: EdgeInsets.only(right: 20),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(18),
                image: DecorationImage(
                  image: AssetImage(img),
                ),
              ),
              child: Align(
                alignment: Alignment.topRight,
                child: Container(
                  width: 70,
                  height: 30,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(30),
                        topRight: Radius.circular(18)),
                    color: kPurple,
                  ),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/icon_star.png',
                          width: 18,
                          height: 18,
                        ),
                        SizedBox(
                          width: 2,
                        ),
                        Text(
                          '$rating/5',
                          style: whiteTextStyle.copyWith(
                            fontSize: 13,
                            fontWeight: medium,
                          ),
                        ),
                      ]),
                ),
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    overflow: TextOverflow.ellipsis,
                    style: blackTextStyle.copyWith(
                      fontSize: 18,
                      fontWeight: medium,
                    ),
                  ),
                  SizedBox(
                    height: 2,
                  ),
                  Text.rich(TextSpan(children: [
                    TextSpan(
                      text: '\$$price',
                      style: purpleTextStyle.copyWith(
                        fontSize: 16,
                        fontWeight: medium,
                      ),
                    ),
                    TextSpan(
                      text: ' / month',
                      style: greyTextStyle.copyWith(
                        fontSize: 16,
                        fontWeight: light,
                      ),
                    ),
                  ])),
                  SizedBox(
                    height: 16,
                  ),
                  Text(
                    '$city, $country',
                    style: greyTextStyle.copyWith(
                      fontWeight: light,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
