import 'package:flutter/material.dart';

import '../theme.dart';

class CityItem extends StatelessWidget {
  final String img, name;
  final bool isFavorite;
  const CityItem(
      {Key? key,
      required this.img,
      required this.name,
      required this.isFavorite})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 120,
      height: 150,
      margin: EdgeInsets.only(right: 20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(18),
        color: kSoftGrey,
      ),
      child: Column(
        children: [
          Container(
            width: double.infinity,
            height: 102,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(18), topLeft: Radius.circular(18)),
              image: DecorationImage(
                image: AssetImage(
                  img,
                ),
                fit: BoxFit.cover,
              ),
            ),
            child: isFavorite
                ? Align(
                    alignment: Alignment.topRight,
                    child: Container(
                      width: 50,
                      height: 30,
                      padding: EdgeInsets.all(4),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(30),
                          topRight: Radius.circular(20),
                        ),
                        color: kPurple,
                      ),
                      child: Container(
                        width: 20,
                        height: 20,
                        margin: EdgeInsets.only(left: 8),
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage('assets/icon_star.png'),
                          ),
                        ),
                      ),
                    ),
                  )
                : Container(),
          ),
          SizedBox(
            height: 11,
          ),
          Text(
            name,
            style: blackTextStyle.copyWith(
              fontSize: 16,
              fontWeight: medium,
            ),
          ),
        ],
      ),
    );
  }
}
