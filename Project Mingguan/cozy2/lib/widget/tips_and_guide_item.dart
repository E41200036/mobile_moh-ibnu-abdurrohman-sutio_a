import 'package:flutter/material.dart';

import '../theme.dart';

class TipsAndGuidanceItem extends StatelessWidget {
  final String icon, title;
  final String date;
  const TipsAndGuidanceItem(
      {Key? key, required this.icon, required this.title, required this.date})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          width: 80,
          height: 80,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(24),
            image: DecorationImage(
              image: AssetImage(icon),
              fit: BoxFit.cover,
            ),
          ),
        ),
        SizedBox(
          width: 16,
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                title,
                style: blackTextStyle.copyWith(
                  fontSize: 18,
                  fontWeight: medium,
                ),
              ),
              SizedBox(
                height: 4,
              ),
              Text(
                'Updated $date',
                style: greyTextStyle.copyWith(
                  fontSize: 14,
                  fontWeight: light,
                ),
              ),
            ],
          ),
        ),
        Icon(
          Icons.chevron_right,
          color: kGrey,
          size: 26,
        ),
      ],
    );
  }
}
