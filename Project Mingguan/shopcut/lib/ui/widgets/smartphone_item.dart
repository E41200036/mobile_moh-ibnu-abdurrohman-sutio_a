import 'package:flutter/material.dart';
import 'package:shopcut/theme.dart';

class SmartphoneItem extends StatefulWidget {
  final String imgUrl, name;
  final bool isPromo;
  final double price, previousPrice;
  const SmartphoneItem(
      {Key? key,
      required this.imgUrl,
      required this.name,
      required this.isPromo,
      required this.price,
      required this.previousPrice})
      : super(key: key);

  @override
  State<SmartphoneItem> createState() => _SmartphoneItemState();
}

bool isPressed = false;
int itemCount = 0;

class _SmartphoneItemState extends State<SmartphoneItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 15),
      child: Row(
        children: [
          Stack(
            children: [
              Container(
                width: 80,
                height: 78,
                margin: EdgeInsets.only(left: 2, right: 20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                      image: AssetImage(widget.imgUrl), fit: BoxFit.cover),
                ),
              ),
              if (widget.isPromo)
                Container(
                  margin: EdgeInsets.only(top: 10),
                  width: 31,
                  height: 12,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: kRed,
                  ),
                  child: Center(
                    child: Text(
                      'promo',
                      style: whiteTextStyle.copyWith(
                        fontSize: 7,
                        fontWeight: bold,
                      ),
                    ),
                  ),
                ),
            ],
          ),
          SizedBox(
            width: 15,
          ),
          Container(
            // make border
            width: MediaQuery.of(context).size.width - 177,
            height: 78,
            // decoration: BoxDecoration(
            //   border: Border.all(color: kBrand),
            // ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  widget.name,
                  style: blackTextStyle.copyWith(fontSize: 12),
                ),
                SizedBox(
                  height: 28,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: '\$ ${widget.price} ',
                            style: blackTextStyle.copyWith(
                                color: Color(0xff26603F),
                                fontSize: 14,
                                fontWeight: FontWeight.bold),
                          ),
                          if (widget.previousPrice != null && widget.isPromo)
                            TextSpan(
                              text: '\$ ${widget.previousPrice}',
                              style: greyTextStyle.copyWith(
                                  fontSize: 12,
                                  decoration: TextDecoration.lineThrough),
                            ),
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: () => setState(() {
                        isPressed = !isPressed;
                      }),
                      child: !isPressed
                          ? Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Icon(
                                  Icons.remove_circle_outline,
                                  color: kBrand,
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  itemCount.toString(),
                                  style: blackTextStyle.copyWith(
                                    fontSize: 12,
                                    fontWeight: bold,
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Icon(
                                  Icons.add_circle_rounded,
                                  color: kBrand,
                                ),
                              ],
                            )
                          : Container(
                              width: 46,
                              height: 24,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4),
                                border: Border.all(width: 1, color: kBrand),
                                color: Colors.transparent,
                              ),
                              child: Center(
                                child: Text(
                                  'add',
                                  style: brandTextStyle.copyWith(fontSize: 10),
                                ),
                              ),
                            ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
