import 'package:flutter/material.dart';
import 'package:shopcut/theme.dart';

class PopularItem extends StatefulWidget {
  final String imgUrl, title;
  final double price;

  const PopularItem(
      {Key? key,
      required this.imgUrl,
      required this.title,
      required this.price})
      : super(key: key);

  @override
  State<PopularItem> createState() => _PopularItemState();
}

int itemCount = 0;
bool isPressed = false;

class _PopularItemState extends State<PopularItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: (MediaQuery.of(context).size.width - 90) / 2,
      // height: 215,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: double.infinity,
            height: 142,
            margin: EdgeInsets.only(bottom: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(7),
              image: DecorationImage(
                  image: AssetImage(widget.imgUrl), fit: BoxFit.cover),
            ),
          ),
          Text(
            widget.title,
            style: blackTextStyle.copyWith(fontSize: 12),
          ),
          SizedBox(
            height: 29,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                '\$${widget.price}',
                style: blackTextStyle.copyWith(
                  color: Color(0xff83A95C),
                  fontWeight: bold,
                ),
              ),
              GestureDetector(
                onTap: () => setState(() {
                  isPressed = !isPressed;
                }),
                child: !isPressed
                    ? Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Icon(
                            Icons.remove_circle_outline,
                            color: kBrand,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            itemCount.toString(),
                            style: blackTextStyle.copyWith(
                              fontSize: 12,
                              fontWeight: bold,
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Icon(
                            Icons.add_circle_rounded,
                            color: kBrand,
                          ),
                        ],
                      )
                    : Container(
                        width: 46,
                        height: 24,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                          border: Border.all(width: 1, color: kBrand),
                          color: Colors.transparent,
                        ),
                        child: Center(
                          child: Text(
                            'add',
                            style: brandTextStyle.copyWith(fontSize: 10),
                          ),
                        ),
                      ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
