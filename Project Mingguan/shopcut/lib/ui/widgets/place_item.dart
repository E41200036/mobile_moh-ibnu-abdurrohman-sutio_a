import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';
import 'package:shopcut/theme.dart';
import 'package:shopcut/ui/pages/detail_page.dart';

class PlaceItem extends StatelessWidget {
  final String imageUrl, name, category;
  final int openTime, closeTime;
  final double rating, distance;
  final isPromo;
  const PlaceItem(
      {Key? key,
      required this.imageUrl,
      required this.name,
      required this.category,
      required this.openTime,
      required this.closeTime,
      required this.rating,
      required this.distance,
      this.isPromo})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(context, MaterialPageRoute(builder: (_) {
        return DetailPage(
            imageUrl: imageUrl,
            name: name,
            category: category,
            openTime: openTime,
            closeTime: closeTime,
            rating: rating,
            distance: distance);
      })),
      child: Container(
        width: double.infinity,
        margin: EdgeInsets.only(bottom: 15),
        child: Row(
          children: [
            Stack(
              children: [
                Container(
                  width: 80,
                  height: 78,
                  margin: EdgeInsets.only(left: 2, right: 20),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    image: DecorationImage(
                        image: AssetImage(imageUrl), fit: BoxFit.cover),
                  ),
                ),
                if (isPromo)
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    width: 31,
                    height: 12,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      color: kRed,
                    ),
                    child: Center(
                      child: Text(
                        'promo',
                        style: whiteTextStyle.copyWith(
                          fontSize: 7,
                          fontWeight: bold,
                        ),
                      ),
                    ),
                  ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      name,
                      style: blackTextStyle.copyWith(
                        fontWeight: medium,
                      ),
                    ),
                    if (isPromo)
                      Container(
                        margin: EdgeInsets.only(left: 4),
                        child: Image.asset(
                          'assets/icon_discount.png',
                          width: 15,
                          height: 15,
                        ),
                      ),
                  ],
                ),
                SizedBox(
                  height: 4,
                ),
                Row(
                  children: [
                    Row(
                      children: [
                        Icon(
                          IconlyBold.star,
                          color: kYellow,
                          size: 14,
                        ),
                        SizedBox(
                          width: 2,
                        ),
                        Text(
                          rating.toString(),
                          style: blackTextStyle.copyWith(fontSize: 10),
                        ),
                      ],
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      '·',
                      style: greyTextStyle.copyWith(fontSize: 10),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      category,
                      style: greyTextStyle.copyWith(fontSize: 10),
                    ),
                  ],
                ),
                SizedBox(
                  height: 12,
                ),
                DottedLine(
                  dashColor: kRed,
                  dashGapRadius: 10,
                  direction: Axis.horizontal,
                  dashLength: double.infinity,
                  lineLength: 6,
                  dashGapLength: 8,
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Icon(
                      IconlyBold.location,
                      color: kBrand,
                      size: 14,
                    ),
                    SizedBox(
                      width: 3,
                    ),
                    Text(
                      '< ' + distance.toString(),
                      style: blackTextStyle.copyWith(fontSize: 10),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      'Closes at ${closeTime.toString()}',
                      style:
                          greyTextStyle.copyWith(fontSize: 8, fontWeight: bold),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
