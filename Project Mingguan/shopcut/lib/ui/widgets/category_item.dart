import 'package:flutter/material.dart';
import 'package:shopcut/theme.dart';

class CategoryItem extends StatelessWidget {
  final String imageUrl, name;
  const CategoryItem({Key? key, required this.imageUrl, required this.name})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: (MediaQuery.of(context).size.width - 140) / 3,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: 47,
            height: 47,
            padding: EdgeInsets.all(13),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(width: 1, color: kSoftBrand),
            ),
            child: Image.asset(
              imageUrl,
              width: 22,
              height: 22,
            ),
          ),
          SizedBox(
            height: 11,
          ),
          Text(
            name,
            style: blackTextStyle,
          ),
        ],
      ),
    );
  }
}
