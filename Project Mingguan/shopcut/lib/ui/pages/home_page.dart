import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';

import '../../theme.dart';
import '../widgets/category_item.dart';
import '../widgets/place_item.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget userInfo() {
      return Container(
        margin: EdgeInsets.only(bottom: 22),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Container(
                  width: 34,
                  height: 34,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(7),
                    color: kSoftBrand,
                  ),
                  child: Icon(
                    IconlyBold.location,
                    size: 14,
                    color: kBrand,
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Adi Sucipto No 347, Manahan',
                      style: blackTextStyle.copyWith(
                        fontSize: 12,
                        fontWeight: bold,
                      ),
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Text(
                      'Change location',
                      style: greyTextStyle.copyWith(fontSize: 10),
                    ),
                  ],
                )
              ],
            ),
            Icon(
              IconlyLight.arrow_right_2,
              size: 16,
            ),
          ],
        ),
      );
    }

    Widget categories() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text.rich(TextSpan(children: [
            TextSpan(
              text: 'Our ',
              style: blackTextStyle.copyWith(fontSize: 18),
            ),
            TextSpan(
              text: 'Category',
              style: blackTextStyle.copyWith(fontSize: 18, fontWeight: bold),
            ),
          ])),
          Text(
            '7 categories available',
            style: greyTextStyle.copyWith(fontSize: 12),
          ),
          SizedBox(
            height: 20,
          ),
          Center(
            child: Wrap(
              crossAxisAlignment: WrapCrossAlignment.center,
              spacing: 30,
              runSpacing: 40,
              children: [
                CategoryItem(
                    imageUrl: 'assets/icon_restaurant.png',
                    name: 'Restaurants'),
                CategoryItem(
                    imageUrl: 'assets/icon_supermarket.png',
                    name: 'Supermarket'),
                CategoryItem(
                    imageUrl: 'assets/icon_baby.png', name: 'Baby & Kids'),
                CategoryItem(
                    imageUrl: 'assets/icon_electronic.png',
                    name: 'Electronics'),
                CategoryItem(imageUrl: 'assets/icon_book.png', name: 'Books'),
                CategoryItem(imageUrl: 'assets/icon_other.png', name: 'Other'),
              ],
            ),
          ),
        ],
      );
    }

    Widget searchField() {
      return Container(
        width: double.infinity,
        height: 50,
        margin: EdgeInsets.only(top: 30, bottom: 20),
        padding: EdgeInsets.symmetric(horizontal: 21),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Color(0xffF6F6F6),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: TextFormField(
                style: greyTextStyle.copyWith(fontSize: 12),
                decoration: InputDecoration.collapsed(
                  hintText: 'Search Electronics Store',
                  hintStyle: greyTextStyle.copyWith(fontSize: 12),
                ),
              ),
            ),
            Icon(
              IconlyLight.search,
              color: kGrey,
            ),
          ],
        ),
      );
    }

    Widget places() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          PlaceItem(
            imageUrl: 'assets/image_place1.png',
            name: 'Mbak Yayuk - Grand Mall',
            category: 'Laptop, Electronic Products',
            openTime: 7,
            closeTime: 22,
            rating: 4.8,
            distance: 0.2,
            isPromo: true,
          ),
          PlaceItem(
            imageUrl: 'assets/image_place2.png',
            name: 'Gramedia - Solo Square',
            category: 'Gym, Electronics',
            openTime: 7,
            closeTime: 22,
            rating: 4.8,
            distance: 1.0,
            isPromo: false,
          ),
          PlaceItem(
            imageUrl: 'assets/image_place3.png',
            name: 'iBox - The Park',
            category: 'iPhone, iMac, Macbook',
            openTime: 7,
            closeTime: 22,
            rating: 4.8,
            distance: 1.8,
            isPromo: false,
          ),
          PlaceItem(
            imageUrl: 'assets/image_place4.png',
            name: 'Candi Elektronik - Grand Mall',
            category: 'Smart TV, Air Conditioner',
            openTime: 7,
            closeTime: 22,
            rating: 4.8,
            distance: 1.8,
            isPromo: true,
          ),
          PlaceItem(
            imageUrl: 'assets/image_place5.png',
            name: 'Mi Store - Solo Paragon',
            category: 'Mi Products',
            openTime: 7,
            closeTime: 22,
            rating: 4.8,
            distance: 1.8,
            isPromo: true,
          ),
          PlaceItem(
            imageUrl: 'assets/image_place6.png',
            name: 'Ace Hardware - Solo Paragon',
            category: 'Home Living, Electronics',
            openTime: 7,
            closeTime: 22,
            rating: 4.8,
            distance: 1.8,
            isPromo: true,
          ),
        ],
      );
    }

    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Container(
              width: double.infinity,
              height: 250,
              decoration: BoxDecoration(
                color: kBrand,
              ),
              child: Stack(
                children: [
                  Align(
                    alignment: Alignment.bottomRight,
                    child: Image.asset(
                      'assets/image_background.png',
                      width: 307,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 50),
                    padding: EdgeInsets.only(left: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Hi, Queen',
                          style: whiteTextStyle.copyWith(
                              fontSize: 18, fontWeight: light),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Text(
                          'What shop do\nyou need?',
                          style: whiteTextStyle.copyWith(
                              fontSize: 24, fontWeight: bold),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height,
              width: double.infinity,
              child: DraggableScrollableSheet(
                initialChildSize: 0.740,
                minChildSize: 0.740,
                maxChildSize: 1,
                builder: (context, scrollController) {
                  return Container(
                    padding: EdgeInsets.all(20),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20),
                      ),
                    ),
                    child: SingleChildScrollView(
                      controller: scrollController,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          userInfo(),
                          categories(),
                          searchField(),
                          places(),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
