import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';
import 'package:shopcut/theme.dart';
import 'package:shopcut/ui/widgets/popular_item.dart';
import 'package:shopcut/ui/widgets/smartphone_item.dart';

class DetailPage extends StatelessWidget {
  final String imageUrl, name, category;
  final int openTime, closeTime;
  final double rating, distance;

  const DetailPage(
      {Key? key,
      required this.imageUrl,
      required this.name,
      required this.category,
      required this.openTime,
      required this.closeTime,
      required this.rating,
      required this.distance})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget header() {
      return Container(
        margin: EdgeInsets.only(bottom: 20),
        child: Stack(children: [
          Container(
            width: double.infinity,
            height: 389,
            child: Stack(children: [
              Container(
                width: double.infinity,
                height: 350,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.vertical(
                    bottom: Radius.circular(20),
                  ),
                  image: DecorationImage(
                    image: AssetImage(imageUrl),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Container(
                    width: double.infinity,
                    height: 87,
                    padding: EdgeInsets.all(20),
                    decoration: BoxDecoration(
                      color: kWhite,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                          color: Color(0xffACB4BC).withOpacity(.15),
                          offset: Offset(0, 4),
                          blurRadius: 30,
                        ),
                      ],
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          children: [
                            Row(
                              children: [
                                Text(
                                  name,
                                  style: blackTextStyle.copyWith(
                                      fontSize: 16, fontWeight: bold),
                                ),
                                SizedBox(
                                  width: 4,
                                ),
                                Image.asset(
                                  'assets/icon_discount.png',
                                  width: 15,
                                  height: 15,
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              category,
                              style: greyTextStyle.copyWith(fontSize: 12),
                            ),
                          ],
                        ),
                        Container(
                          width: 40,
                          height: 40,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(7),
                            color: kSoftBrand,
                          ),
                          child: Icon(
                            IconlyBold.location,
                            color: kBrand,
                            size: 16,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ]),
          ),
        ]),
      );
    }

    Widget badge_item(Icon icon, String name, String value) {
      return Container(
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.only(right: 10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: kWhite,
        ),
        child: Row(children: [
          Container(
            width: 34,
            height: 34,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(7),
              color: kYellow.withOpacity(.10),
            ),
            child: Center(child: icon),
          ),
          SizedBox(
            width: 10,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                name,
                style: greyTextStyle.copyWith(
                  fontWeight: medium,
                  fontSize: 10,
                ),
              ),
              SizedBox(
                height: 2,
              ),
              Text(
                value,
                style: blackTextStyle.copyWith(fontSize: 12, fontWeight: bold),
              ),
            ],
          )
        ]),
      );
    }

    Widget badge() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(children: [
              badge_item(
                Icon(
                  IconlyBold.star,
                  color: Color(0xffE9C496),
                  size: 16,
                ),
                'Ratings',
                rating.toString(),
              ),
              badge_item(
                Icon(
                  IconlyBold.time_circle,
                  color: Color(0xffE9C496),
                  size: 16,
                ),
                'Opening Hours',
                '$openTime AM - $closeTime PM',
              ),
              badge_item(
                Icon(
                  IconlyBold.location,
                  color: Color(0xffE9C496),
                  size: 16,
                ),
                'Distance',
                '< $distance km',
              ),
            ]),
          ),
          SizedBox(
            height: 20,
          ),
          DottedLine(
            dashColor: kGrey.withOpacity(.60),
            dashLength: 8,
            lineLength: double.infinity,
            dashRadius: 30,
          ),
          SizedBox(
            height: 20,
          ),
        ],
      );
    }

    Widget popularProduct() {
      return Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Top Popular',
              style: blackTextStyle.copyWith(fontSize: 16, fontWeight: bold),
            ),
            SizedBox(
              height: 15,
            ),
            Wrap(
              crossAxisAlignment: WrapCrossAlignment.center,
              spacing: 30,
              runSpacing: 30,
              children: [
                PopularItem(
                  imgUrl: 'assets/image_item1.png',
                  title: 'Macbook Terbaru Murah',
                  price: 89.99,
                ),
                PopularItem(
                  imgUrl: 'assets/image_item2.png',
                  title: 'Playsatation 5 KW Super',
                  price: 99,
                ),
                PopularItem(
                  imgUrl: 'assets/image_item3.png',
                  title: 'Vaporizer Terbaik 2021',
                  price: 99,
                ),
                PopularItem(
                  imgUrl: 'assets/image_item4.png',
                  title: 'Smartphone Singosarenan',
                  price: 100,
                ),
              ],
            ),
            SizedBox(
              height: 30,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 47),
              child: DottedLine(
                dashColor: kGrey.withOpacity(.60),
                dashLength: 8,
                lineLength: double.infinity,
                dashRadius: 30,
              ),
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      );
    }

    Widget smartphone() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Smartphone',
            style: blackTextStyle.copyWith(fontSize: 16, fontWeight: bold),
          ),
          SizedBox(
            height: 15,
          ),
          SmartphoneItem(
            imgUrl: 'assets/image_sm1.png',
            name: 'iPhone 12 Pro Max Unyu',
            isPromo: true,
            price: 3.50,
            previousPrice: 5.00,
          ),
          SmartphoneItem(
            imgUrl: 'assets/image_sm2.png',
            name: 'Xiaomi Mi 11 Ultra Resmi',
            isPromo: false,
            price: 5.50,
            previousPrice: 5.00,
          ),
          SmartphoneItem(
            imgUrl: 'assets/image_sm3.png',
            name: 'iPhone X 64GB - Garansi 1 Tahun',
            isPromo: false,
            price: 5.50,
            previousPrice: 5.00,
          ),
        ],
      );
    }

    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: Container(
        width: double.infinity,
        margin: EdgeInsets.symmetric(horizontal: 30),
        height: 59,
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(14),
          color: kBrand,
        ),
        child:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Row(
            children: [
              Text(
                'Checkout',
                style: whiteTextStyle.copyWith(fontWeight: bold),
              ),
              SizedBox(
                width: 5,
              ),
              Container(
                width: 50,
                height: 20,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4),
                  color: Color(0xffA96D87),
                ),
                child: Center(
                    child: Text(
                  '3 items',
                  style:
                      whiteTextStyle.copyWith(fontSize: 10, fontWeight: bold),
                )),
              )
            ],
          ),
          Text(
            '\$14,50',
            style: whiteTextStyle.copyWith(
              fontSize: 16,
              fontWeight: bold,
            ),
          ),
        ]),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            header(),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  badge(),
                  popularProduct(),
                  smartphone(),
                  SizedBox(
                    height: 80,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
