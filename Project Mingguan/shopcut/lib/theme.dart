import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color kBrand = Color(0xff944E6C);
Color kSoftBrand = Color(0xff944E6C).withOpacity(.10);
Color kWhite = Colors.white;
Color kBlack = Color(0xff2B2B2B);
Color kGrey = Color(0xffBDBDBD);
Color kRed = Color(0xffFF2929);
Color kYellow = Color(0xffF9CA24);

TextStyle brandTextStyle = GoogleFonts.poppins(
  color: kBrand,
);
TextStyle whiteTextStyle = GoogleFonts.poppins(
  color: kWhite,
);
TextStyle blackTextStyle = GoogleFonts.poppins(
  color: kBlack,
);
TextStyle greyTextStyle = GoogleFonts.poppins(
  color: kGrey,
);
TextStyle redTextStyle = GoogleFonts.poppins(
  color: kRed,
);

FontWeight normal = FontWeight.normal;
FontWeight bold = FontWeight.bold;
FontWeight medium = FontWeight.w500;
FontWeight light = FontWeight.w300;
FontWeight semibold = FontWeight.w600;
